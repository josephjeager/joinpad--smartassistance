﻿using System;
using System.Collections.Generic;
using Byn.Net;
using SmartAssistanceSDK.Models;

namespace SmartAssistanceSDK.EnvironmentConfiguration
{
    internal class DevUbuntuConfiguration : SAEnviromentConfig
    {
        internal DevUbuntuConfiguration()
        {
            this.baseUrl = "devubuntu.smartassistance.net";
            this.secureConnection = true;
            this.iceServers = new List<SAIceServer>();
            iceServers.Add(new SAIceServer("stun:stun1.l.google.com:19302"));
            iceServers.Add(new SAIceServer("stun:stun2.l.google.com:19302"));
            iceServers.Add(new SAIceServer("stun:stun3.l.google.com:19302"));
            iceServers.Add(new SAIceServer("stun:stun4.l.google.com:19302"));
            iceServers.Add(new SAIceServer("turn:smartassistance@18.194.107.130:3478", "smartassistance", "vb4mfsfgoq3k9sbv9uaaocv6b6do9n8gdrpbfn5wezncc3d83z8"));
        }
    }
}
