﻿using System;
using System.Collections.Generic;
using SmartAssistanceSDK.Models;

namespace SmartAssistanceSDK.EnvironmentConfiguration
{
    public abstract class SAEnviromentConfig
    {
        protected string baseUrl;
        protected bool secureConnection;
        protected List<SAIceServer> iceServers;
        internal string BaseUrl
        {
            get { return this.baseUrl; }
        }
        private string httpProtocol{
            get{ return this.secureConnection? "https://": "http://";}
        }

        private string wsProtocol
        {
            get { return this.secureConnection ? "wss://" : "ws://"; }
        }

        internal string LoginUrl{
            get { return this.httpProtocol + this.baseUrl + "/token"; }
        }
        internal string LogoutUrl
        {
            get { return this.httpProtocol + this.baseUrl + "/user/logout"; }
        }
        internal string UserSignalerUrl{
            get { return this.httpProtocol + this.baseUrl + "/user-signaler/"; }
        }
        internal string CallSignalerUrl
        {
            get { return this.wsProtocol + this.baseUrl + "/call-signaler"; }
        }
        internal List<SAIceServer> IceServers{
            get { return this.iceServers; }
        }
    }
}
