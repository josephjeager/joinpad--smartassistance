﻿using System;
using System.Collections.Generic;

namespace SmartAssistanceSDK.Models
{
    public class SARooms
    {
        public string name { get; set; }
        public string description { get; set; }
        public string path { get; set; }
        public List<SAUser> users { get; set; }
        public SARooms()
        {
        }
    }
}
