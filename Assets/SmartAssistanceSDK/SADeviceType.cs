﻿using System;
namespace SmartAssistanceSDK
{
    public enum SADeviceType
    {
        Web = 1,
        Smartphone = 2,
        Tablet = 3,
        Hololens = 4
    }
}
