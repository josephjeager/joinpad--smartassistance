﻿using System;
using SmartAssistanceSDK.Models;

namespace SmartAssistanceSDK.HttpResponse
{
    internal class LoginResponse
    {
        public string username { get; set; }
        public SAUser user { get; set; }
        public string token { get; set; }

        public LoginResponse(){}
    }
}
