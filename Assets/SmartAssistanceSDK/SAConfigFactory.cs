﻿using SmartAssistanceSDK.EnvironmentConfiguration;

namespace SmartAssistanceSDK
{
    internal static class SAConfigFactory
    {
        internal static SAEnviromentConfig GetConfiguration(SAEnvironmentType env){
            SAEnviromentConfig config;
            switch(env){
                case SAEnvironmentType.Alstom:
                    config = new AlstomConfiguration();
                    break;
                case SAEnvironmentType.China:
                    config = new ChinaConfiguration();
                    break;
                case SAEnvironmentType.Joinpad:
                    config = new JoinpadConfiguration();
                    break;
                case SAEnvironmentType.DevUbuntu:
                    config = new DevUbuntuConfiguration();
                    break;  
                case SAEnvironmentType.Debug:
                    config = new DebugConfiguration();
                    break;
                case SAEnvironmentType.Test:
                    config = new TestConfiguration();
                    break;
                default: config = new TestConfiguration();
                    break;           
            }
            return config;
        }
    }
}
