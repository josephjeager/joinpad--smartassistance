﻿using System;
namespace SmartAssistanceSDK
{
    public enum SAUserStatus
    {
        Available,
        Busy,
        Away,
        InCall
    }
}
