﻿using System;
namespace SmartAssistanceSDK.Delegates
{
    public enum SASignalerEventType
    {
        LoginSuccess,
        LoginError,
        LogoutSuccess,
        LogoutError,
        Error,
        SignalerConnected,
        SignalerDisconnected,
        UserConnected,
        UserDisconnected,
        UserListUpdated,
        CallRequestReceived,
        CallAccepted,
        CallRejected,
        CallStopRequestReceived,
        CallStop,
        MeUpdated,
        SocketReconnectionFailed
    }
}
