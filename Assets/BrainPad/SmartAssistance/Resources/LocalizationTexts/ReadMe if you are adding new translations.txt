Chinese charactes font atlas becomes huge if all chinese characters are included 
thus only actually used characters are generated.

If a new chinese text is added it is necessary to regenerate the font atlas from  
"Windows->TextMeshPro->Font atlas creator" including all the previously used characters 
as well as newly added ones

The fonts you have to generate an atlas from are:
- NotoSansSC-Bold
- NotoSansSC-Light
- NotoSansSC-Regular

The following list contains all the chinese (and ASCII) characters currently used, 
make sure to update this file if you add new characters (using notepad++)

 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~ …□、。一上下不与专业丢个中为主了于人仍他以件位住何作你佳使信停先克入关册决准出击列初删到制加务动助化协单即发取口另只叫号名后向否听启员周呼咨器回围图在场声备复太失好如始姓完定家密将尝工已帮常并应开弱录待很得态总恢息您意成我或户所手打扬技拒拖择持指按掌接提握援搜操支收放效整断新方旋无时明是显智暂最服术束来果查标案检横止正步氏法注活流消滑激点然照状现理用由电画登的目相码确示程窗笔等管系索线终经结绘络绝统继续绿网置而联能色获菜行表被要视角解计认讯记设试话询误说请调败质超转边达返进远连退送逆选通道部配重量针钮错键闭限除需音频颜风首麦（），：？