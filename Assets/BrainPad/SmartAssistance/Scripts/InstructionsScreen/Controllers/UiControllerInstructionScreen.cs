﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.InstructionsScreen.Controllers
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    public class UiControllerInstructionScreen : MonoBehaviour
    {
        public GameObject AnimatedContent;

        public Toggle DisplayInstructionsOnStartupCheck;

        private void OnEnable()
        {
            ((RectTransform) AnimatedContent.transform).anchoredPosition = new Vector2(0, 0);
            AnimatedContent.GetComponent<Animator>().enabled = false;
        }

        private void Start()
        {
            bool hideInstructionsOnStartup = PlayerPrefs.GetInt("HideInstructionsOnStartup") == 1;
            DisplayInstructionsOnStartupCheck.isOn = !hideInstructionsOnStartup;

            DisplayInstructionsOnStartupCheck.onValueChanged.AddListener(delegate { SaveDisplayInstructionsOnStartupPreference(); });
        }

        public void OpenWithAnimation()
        {
            AnimatedContent.GetComponent<Animator>().enabled = true;
            AnimatedContent.GetComponent<Animator>().Play("InstructionsIn", 0, 0f);
        }

        public void CloseWithAnimation()
        {
            AnimatedContent.GetComponent<Animator>().enabled = true;
            AnimatedContent.GetComponent<Animator>().Play("InstructionsOut", 0, 0f);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                CloseWithAnimation();
            }
        }

        public void SaveDisplayInstructionsOnStartupPreference()
        {
            PlayerPrefs.SetInt("HideInstructionsOnStartup", !DisplayInstructionsOnStartupCheck.isOn ? 1 : 0);
        }

        public void CloseInstructions()
        {
            CloseWithAnimation();
        }

        public void TutorialStartACall()
        {
            SmartAssistanceManager.Instance.TutorialStartACallScreen();
        }

        public void TutorialArWidgets()
        {
            SmartAssistanceManager.Instance.TutorialArWidgetsScreen();
        }

        public void TutorialDrawWidgets()
        {
            SmartAssistanceManager.Instance.TutorialDrawWidgetsScreen();
        }

        public void TutorialRemoveWidgets()
        {
            SmartAssistanceManager.Instance.TutorialRemoveWidgetsScreen();
        }
    }
}