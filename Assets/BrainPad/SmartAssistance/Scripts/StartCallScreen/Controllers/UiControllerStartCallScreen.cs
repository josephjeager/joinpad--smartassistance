﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.Core.Http;
using Byn.Media;
using GameToolkit.Localization;
using SmartAssistanceSDK;
using SmartAssistanceSDK.Delegates;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.StartCallScreen.Controllers
{
    [SuppressMessage("ReSharper", "SwitchStatementMissingSomeCases")]
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "InvertIf")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    public class UiControllerStartCallScreen : MonoBehaviour
    {
        public TextMeshProUGUI NameLabel;
        public TextMeshProUGUI LastNameLabel;
        public TextMeshProUGUI RoleLabel;
        public TextMeshProUGUI LocationLabel;
        public TextMeshProUGUI NoProfilePicCapitalLetter;
        public Image ProfilePic;
        public int Timeout = 20;

        [Header("Labels")] public LocalizedText OnField;
        public LocalizedText Support;
        
        [Header("Notifications")] public LocalizedText CallEndedDueToTimeout;
        public LocalizedText CallRejectedByOnFieldUser;
        public LocalizedText CallRejectedBySupport;
        public LocalizedText CallEndedDueToOnFieldUserDisconnection;
        public LocalizedText CallEndedDueToSupportDisconnection;
        public LocalizedText CallEndedDueToServerDisconnection;
        
        private IEnumerator _timeoutCoroutine;
        private IEnumerator _callErrorCoroutine;

        private float _startTime;

        private void OnEnable()
        {
            _startTime = Time.time;
            NameLabel.text = SmartAssistanceManager.Instance.RemoteUser.name;
            LastNameLabel.text = SmartAssistanceManager.Instance.RemoteUser.lastname;
            RoleLabel.text = SmartAssistanceManager.Instance.RemoteUser.role == SAUserRole.UserOnField ? OnField.Value : Support.Value;
            LocationLabel.text = SmartAssistanceManager.Instance.RemoteUser.location;

            if (SmartAssistanceManager.Instance.RemoteUser.name.Length > 0)
            {
                NoProfilePicCapitalLetter.text = SmartAssistanceManager.Instance.RemoteUser.name[0].ToString().ToUpper();
            }

            ProfilePicturesLoader.Instance.LoadProfilePic(SmartAssistanceManager.Instance.RemoteUser, ProfilePic.mainTexture.width, ProfilePic.mainTexture.height, ((Texture2D) ProfilePic.mainTexture).format, sprite =>
            {
                if (sprite != null)
                {
                    ProfilePic.sprite = sprite;
                    ProfilePic.gameObject.SetActive(true);
                }
            });

            SmartAssistanceManager.Instance.SignalerCallEvent += OnSignalerCallEvent;
            SmartAssistanceManager.Instance.WebRtcCallEvent += OnWebRtcCallEvent;
            StartCoroutine(_timeoutCoroutine = TimeoutRoutine());
        }

        private void OnDisable()
        {
            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.SignalerCallEvent -= OnSignalerCallEvent;
                SmartAssistanceManager.Instance.WebRtcCallEvent -= OnWebRtcCallEvent;
            }

            StopCoroutines();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                StopCallRequest();
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
            {
                if (_startTime + Timeout > Time.time)
                {
                    StopCoroutines();
                    StartCoroutine(_callErrorCoroutine = CallFailedRoutine(CallEndedDueToTimeout.Value));
                }
            }
        }

        public void StopCallRequest()
        {
            StopCoroutines();
            SmartAssistanceManager.Instance.StopCallRequest();
            SmartAssistanceManager.Instance.ContactsScreen();
        }

        private void OnSignalerCallEvent(object sender, SASignalerEventArgs args)
        {
            switch (args.EventType)
            {
                case SASignalerEventType.UserDisconnected:
                    if (args.User.username.Equals(SmartAssistanceManager.Instance.RemoteUser.username))
                    {
                        StopCoroutines();
                        LocalizedText notification = SmartAssistanceManager.Instance.RemoteUser.role == SAUserRole.UserOnField ? CallEndedDueToOnFieldUserDisconnection : CallEndedDueToSupportDisconnection;
                        StartCoroutine(_callErrorCoroutine = CallFailedRoutine(notification.Value));
                        SmartAssistanceManager.Instance.OnRemoteUserDisconnectedWhileCalling();
                    }

                    break;
                case SASignalerEventType.SignalerDisconnected:
                    StopCoroutines();
                    StartCoroutine(_callErrorCoroutine = CallFailedRoutine(CallEndedDueToServerDisconnection.Value));
                    SmartAssistanceManager.Instance.OnSignalerDisconnectedWhileCalling();
                    break;
                case SASignalerEventType.CallRejected:
                    StopCoroutines();
                    LocalizedText notification2 =  SmartAssistanceManager.Instance.RemoteUser.role == SAUserRole.UserOnField ? CallRejectedByOnFieldUser : CallRejectedBySupport;
                    StartCoroutine(_callErrorCoroutine = CallFailedRoutine(notification2.Value));
                    SmartAssistanceManager.Instance.OnCallRejected();
                    break;
                case SASignalerEventType.CallAccepted:
                    SmartAssistanceManager.Instance.OnCallAccepted();
                    break;
            }
        }

        private void OnWebRtcCallEvent(object sender, ICall iCall, CallEventArgs args)
        {
            switch (args.Type)
            {
                case CallEventType.CallAccepted:
                    SmartAssistanceManager.Instance.CallScreen();
                    break;
            }
        }

        private void StopCoroutines()
        {
            if (_timeoutCoroutine != null)
            {
                StopCoroutine(_timeoutCoroutine);
            }

            if (_callErrorCoroutine != null)
            {
                StopCoroutine(_callErrorCoroutine);
            }
        }

        private IEnumerator TimeoutRoutine()
        {
            yield return new WaitForSeconds(Timeout);

            if (_callErrorCoroutine != null)
            {
                StopCoroutine(_callErrorCoroutine);
            }

            StartCoroutine(_callErrorCoroutine = CallFailedRoutine(CallEndedDueToTimeout.Value));
        }

        private IEnumerator CallFailedRoutine(string message)
        {
            SmartAssistanceManager.Instance.StopCallRequest();
            SmartAssistanceManager.Instance.DispatchNotification(message);

            yield return new WaitForSeconds(3);

            SmartAssistanceManager.Instance.ContactsScreen();
        }
    }
}