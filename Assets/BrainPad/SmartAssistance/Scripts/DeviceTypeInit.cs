﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BrainPad.SmartAssistance.Scripts
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
    public class DeviceTypeInit : MonoBehaviour
    {
        public float TabletDeviceDiagonalInches = 6.5f;

        private void Start()
        {
            if (DeviceDiagonalSizeInInches() > TabletDeviceDiagonalInches)
            {
                // Tablets
                SceneManager.LoadScene("BrainPad/SmartAssistance/SmartAssistanceTablet", LoadSceneMode.Single);
            }
            else
            {
                SceneManager.LoadScene("BrainPad/SmartAssistance/SmartAssistance", LoadSceneMode.Single);
            }
        }

        private static float DeviceDiagonalSizeInInches()
        {
            float screenWidth = Screen.width / Screen.dpi;
            float screenHeight = Screen.height / Screen.dpi;
            float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));
            return diagonalInches;
        }
    }
}