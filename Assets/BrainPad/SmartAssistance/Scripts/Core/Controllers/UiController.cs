﻿using System.Collections;
using BrainPad.SmartAssistance.Scripts.InstructionsScreen.Controllers;
using SmartAssistanceSDK;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.Core.Controllers
{
    public class UiController : MonoBehaviour
    {
        public GameObject CallScreen;
        public GameObject InstructionsScreen;
        public GameObject IncomingCallScreen;
        public GameObject StartCallScreen;
        public GameObject ConfirmLogoutScreen;
        public GameObject ContactsScreen;
        public GameObject LoginScreen;
        public GameObject TutorialStartACallScreen;
        public GameObject TutorialArWidgetsScreen;
        public GameObject TutorialDrawWidgetsScreen;
        public GameObject TutorialRemoveWidgetsScreen;
        public GameObject DisconnectedFromServerScreen;

        public void OpenLoginScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            Clear();
            LoginScreen.SetActive(true);
        }

        public void OpenContactsScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;

            Clear();
            ContactsScreen.SetActive(true);
        }

        public void OpenCallScreen()
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            StartCoroutine(OpenCallScreenRoutine());
        }

        private IEnumerator OpenCallScreenRoutine()
        {
            Clear();
            yield return new WaitForEndOfFrame();
            CallScreen.SetActive(true);
        }

        public void OpenStartCallScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            Clear();
            StartCallScreen.SetActive(true);
        }

        public void OpenIncomingCallScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            Clear();
            IncomingCallScreen.SetActive(true);
        }

        public void OpenInstructionsScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            Clear();
            ContactsScreen.SetActive(true);
            InstructionsScreen.SetActive(true);
        }

        public void OpenInstructionsScreenWithAnimation()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            InstructionsScreen.SetActive(true);
            InstructionsScreen.GetComponent<UiControllerInstructionScreen>().OpenWithAnimation();
        }

        public void OpenTutorialStartACallScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            Clear();
            TutorialStartACallScreen.SetActive(true);
        }

        public void OpenTutorialArWidgetsScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            Clear();
            TutorialArWidgetsScreen.SetActive(true);
        }

        public void OpenTutorialDrawWidgetsScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            Clear();
            TutorialDrawWidgetsScreen.SetActive(true);
        }

        public void OpenTutorialRemoveWidgetsScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            Clear();
            TutorialRemoveWidgetsScreen.SetActive(true);
        }

        public void OpenDisconnectedFromServerScreen()
        {
            Screen.orientation = SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone ? ScreenOrientation.Portrait : ScreenOrientation.Landscape;
            Clear();
            DisconnectedFromServerScreen.SetActive(true);
        }

        public void ShowLogoutScreen()
        {
            ConfirmLogoutScreen.SetActive(true);
        }

        public void HideLogoutScreen()
        {
            ConfirmLogoutScreen.SetActive(false);
        }

        private void Clear()
        {
            CallScreen.SetActive(false);
            if (SmartAssistanceManager.Instance.DeviceType == SADeviceType.Smartphone)
            {
                InstructionsScreen.SetActive(false);
            }

            IncomingCallScreen.SetActive(false);
            StartCallScreen.SetActive(false);
            ConfirmLogoutScreen.SetActive(false);
            ContactsScreen.SetActive(false);
            LoginScreen.SetActive(false);
            TutorialStartACallScreen.SetActive(false);
            TutorialArWidgetsScreen.SetActive(false);
            TutorialDrawWidgetsScreen.SetActive(false);
            TutorialRemoveWidgetsScreen.SetActive(false);
            DisconnectedFromServerScreen.SetActive(false);
        }
    }
}