﻿namespace BrainPad.SmartAssistance.Scripts.Core.Delegates
{
    public delegate void OnNotificationDispatchAllowed(object sender);
}