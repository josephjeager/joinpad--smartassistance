﻿using BrainPad.SmartAssistance.Scripts.Core.Http;

namespace BrainPad.SmartAssistance.Scripts.Core.Delegates
{
    public delegate void ConnectionCheckedEvent(object sender, ConnectionChecker.Status status, float speedMbps, float fitness);
}