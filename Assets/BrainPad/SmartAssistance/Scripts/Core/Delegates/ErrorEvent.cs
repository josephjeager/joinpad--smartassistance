﻿namespace BrainPad.SmartAssistance.Scripts.Core.Delegates
{
    public delegate void ErrorEvent(object sender, string message);
}