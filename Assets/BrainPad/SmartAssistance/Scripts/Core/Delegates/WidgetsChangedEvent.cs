﻿using System.Collections.Generic;
using BrainPad.SmartAssistance.Scripts.Models;

namespace BrainPad.SmartAssistance.Scripts.Core.Delegates
{
    public delegate void WidgetsChangedEvent(object sender, List<Widget> widgets);
}