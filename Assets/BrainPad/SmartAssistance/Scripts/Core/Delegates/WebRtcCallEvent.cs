﻿using Byn.Media;

namespace BrainPad.SmartAssistance.Scripts.Core.Delegates
{
    public delegate void WebRtcCallEvent(object sender, ICall call, CallEventArgs args);
}