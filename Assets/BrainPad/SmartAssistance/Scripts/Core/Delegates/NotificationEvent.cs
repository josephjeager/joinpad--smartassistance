﻿namespace BrainPad.SmartAssistance.Scripts.Core.Delegates
{
    public delegate void NotificationEvent(object sender, string notification);
}