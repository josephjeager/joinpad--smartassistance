﻿using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.Core.Http;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.Core.Behaviors
{
    [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
    public class SignalBarBehavior : MonoBehaviour
    {
        public GameObject Offline;
        public GameObject Background;
        public GameObject Fill;

        public Color32 Good = new Color32(56, 172, 73, 255);
        public Color32 Medium = new Color32(255, 146, 30, 255);
        public Color32 Poor = new Color32(168, 31, 31, 255);

        private void OnEnable()
        {
            OnConnectionChecked(this, ConnectionChecker.Instance.CurrentStatus, ConnectionChecker.Instance.CurrentSpeedMbps, ConnectionChecker.Instance.CurrentFitness);
            ConnectionChecker.Instance.ConnectionCheckedEvent += OnConnectionChecked;
        }

        private void OnDisable()
        {
            if (SmartAssistanceManager.Instance != null)
            {
                ConnectionChecker.Instance.ConnectionCheckedEvent -= OnConnectionChecked;
            }
        }

        private void OnConnectionChecked(object sender, ConnectionChecker.Status status, float speedMbps, float fitness)
        {
            if (status == ConnectionChecker.Status.Offline)
            {
                Offline.SetActive(true);
                Background.SetActive(false);
                Fill.SetActive(false);
            }
            else
            {
                Offline.SetActive(false);
                Background.SetActive(true);
                Fill.SetActive(true);
                Fill.GetComponent<Image>().color = GetColor(fitness);
                Fill.GetComponent<Image>().fillAmount = fitness;
            }
        }

        private Color32 GetColor(float fitness)
        {
            Color32 color32;
            if (fitness > 0.5)
            {
                color32 = Color32.Lerp(Medium, Good, (fitness - 0.5f) * 2);
            }
            else
            {
                color32 = Color32.Lerp(Poor, Medium, fitness * 2);
            }

            return color32;
        }
    }
}