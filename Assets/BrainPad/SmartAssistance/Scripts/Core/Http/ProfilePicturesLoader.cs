﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using SmartAssistanceSDK.Models;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.Core.Http
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    public class ProfilePicturesLoader : MonoBehaviour
    {
        public static ProfilePicturesLoader Instance;

        private readonly IDictionary<string, Sprite> _cachedProfilePics = new Dictionary<string, Sprite>();

        private IEnumerator _loadProfilePicCoroutine;

        private void Awake()
        {
            if (Instance != null) return;
            Instance = this;
        }

        private void OnDisable()
        {
            if (_loadProfilePicCoroutine != null)
            {
                StopCoroutine(_loadProfilePicCoroutine);
            }
        }

        public void LoadProfilePic(SAUser user, int textureWidth, int textureHeight, TextureFormat textureFormat, Action<Sprite> callback)
        {
            if (_cachedProfilePics.ContainsKey(user.id))
            {
                callback(_cachedProfilePics[user.id]);
            }
            else
            {
                if (string.IsNullOrEmpty(user.profile_picture))
                {
                    callback(null);
                }
                else
                {
                    StartCoroutine(_loadProfilePicCoroutine = DownloadProfilePic(user, textureWidth, textureHeight, textureFormat, callback));
                }
            }
        }

        private IEnumerator DownloadProfilePic(SAUser user, int textureWidth, int textureHeight, TextureFormat textureFormat, Action<Sprite> callback)
        {
            WWW www = new WWW(user.profile_picture);
            yield return www;
            
            if (string.IsNullOrEmpty(www.error))
            {
                try
                {
                    Texture2D downloadedTexture = new Texture2D(textureWidth, textureHeight, textureFormat, false);
                    www.LoadImageIntoTexture(downloadedTexture);
                    Sprite sprite = Sprite.Create(downloadedTexture, new Rect(0.0f, 0.0f, downloadedTexture.width, downloadedTexture.height), new Vector2(0.5f, 0.5f), 100);
                    _cachedProfilePics[user.id] = sprite;
                    callback(sprite);
                }
                catch
                {
                    callback(null);
                }
            }
            else
            {
                callback(null);   
            }
         }
    }
}