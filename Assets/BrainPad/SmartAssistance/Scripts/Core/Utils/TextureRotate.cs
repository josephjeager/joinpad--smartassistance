﻿using System;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.Core.Utils
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    public class TextureRotate
    {
        public static Texture2D Rotate(Texture2D originalTexture, bool clockwise)
        {
            Color32[] original = originalTexture.GetPixels32();
            Color32[] rotated = new Color32[original.Length];
            int w = originalTexture.width;
            int h = originalTexture.height;

            for (int j = 0; j < h; ++j)
            {
                for (int i = 0; i < w; ++i)
                {
                    int iRotated = (i + 1) * h - j - 1;
                    int iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                    rotated[iRotated] = original[iOriginal];
                }
            }

            Texture2D rotatedTexture = new Texture2D(h, w);
            rotatedTexture.SetPixels32(rotated);
            rotatedTexture.Apply();
            return rotatedTexture;
        }

        public static Texture2D Rotate180(Texture2D original)
        {
            Color[] pix = original.GetPixels(0, 0, original.width, original.height);
            Array.Reverse(pix, 0, pix.Length);
            Texture2D rotatedTexture = new Texture2D(original.width, original.height);
            rotatedTexture.SetPixels(pix);
            rotatedTexture.Apply();
            return rotatedTexture;
        }
    }
}