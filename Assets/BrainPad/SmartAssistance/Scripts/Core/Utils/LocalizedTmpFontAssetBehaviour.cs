﻿using System.Diagnostics.CodeAnalysis;
using GameToolkit.Localization;
using TMPro;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.Core.Utils
{
    [SuppressMessage("ReSharper", "InvertIf")]
    [SuppressMessage("ReSharper", "SwitchStatementMissingSomeCases")]
    public class LocalizedTmpFontAssetBehaviour : LocalizedAssetBehaviour
    {
        private TMP_FontAsset _robotoLight;
        private TMP_FontAsset _robotoRegular;
        private TMP_FontAsset _robotoBold;
        private TMP_FontAsset _notoMonoLight;
        private TMP_FontAsset _notoMonoRegular;
        private TMP_FontAsset _notoMonoBold;

        private FontType _fontType = FontType.Undefined;

        private TextMeshProUGUI _component;

        private enum FontType
        {
            Undefined,
            Light,
            Regular,
            Bold
        }

        protected override void UpdateComponentValue()
        {
            if (!_component)
            {
                _component = GetComponent<TextMeshProUGUI>();        
                
                _robotoLight = Resources.Load<TMP_FontAsset>("Fonts/Roboto-Light SDF");
                _robotoRegular = Resources.Load<TMP_FontAsset>("Fonts/Roboto-Regular SDF");
                _robotoBold = Resources.Load<TMP_FontAsset>("Fonts/Roboto-Bold SDF");
                _notoMonoLight = Resources.Load<TMP_FontAsset>("Fonts/NotoSansSC-Light SDF");
                _notoMonoRegular = Resources.Load<TMP_FontAsset>("Fonts/NotoSansSC-Regular SDF");
                _notoMonoBold = Resources.Load<TMP_FontAsset>("Fonts/NotoSansSC-Bold SDF");
            }


#if UNITY_EDITOR
            // Disable on editor when not playing.
            if (Application.isPlaying)
            {
#endif
                // Get initial font type
                if (_fontType == FontType.Undefined)
                {
                    if (_component.font.name == _robotoBold.name || _component.font.name == _notoMonoBold.name)
                    {
                        _fontType = FontType.Bold;
                    }
                    else if (_component.font.name == _robotoLight.name || _component.font.name == _notoMonoLight.name)
                    {
                        _fontType = FontType.Light;
                    }
                    else
                    {
                        _fontType = FontType.Regular;
                    }
                }

                if (Localization.Instance.CurrentLanguage == SystemLanguage.Chinese)
                {
                    switch (_fontType)
                    {
                        case FontType.Bold:
                            _component.font = _notoMonoBold;
                            break;
                        case FontType.Light:
                            _component.font = _notoMonoLight;
                            break;
                        default:
                            _component.font = _notoMonoRegular;
                            break;
                    }
                }
                else
                {
                    switch (_fontType)
                    {
                        case FontType.Bold:
                            _component.font = _robotoBold;
                            break;
                        case FontType.Light:
                            _component.font = _robotoLight;
                            break;
                        default:
                            _component.font = _robotoRegular;
                            break;
                    }
                }
#if UNITY_EDITOR
            }
#endif
        }
    }
}