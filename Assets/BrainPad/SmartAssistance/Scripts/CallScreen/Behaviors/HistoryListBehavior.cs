﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.CallScreen.Controllers;
using BrainPad.SmartAssistance.Scripts.Models;
using BrainPad.SmartAssistance.Scripts.Models.Messages;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class HistoryListBehavior : MonoBehaviour
    {
        public ArController ArController;

        public GameObject ItemsContainer;
        public GameObject HistoryWidgetItemPrefab;

        public int Count
        {
            get { return _widgets.Count; }
        }

        private List<Widget> _widgets;
        private int _visibleWidgets;

        private void OnEnable()
        {
            RefreshUi();
        }

        public void SetWidgets(List<Widget> widgets)
        {
            _widgets = widgets;
            RefreshUi();
        }

        private void RefreshUi()
        {
            foreach (Transform child in ItemsContainer.transform)
            {
                Destroy(child.gameObject);
            }

            if (_widgets != null)
            {
                foreach (Widget widget in _widgets)
                {
                    GameObject historyWidgetItem = Instantiate(HistoryWidgetItemPrefab, ItemsContainer.transform);
                    HistoryWidgetItemBehavior historyWidgetItemBehavior = historyWidgetItem.GetComponent<HistoryWidgetItemBehavior>();
                    historyWidgetItemBehavior.SetWidget(widget);
                    historyWidgetItemBehavior.SetHistoryListBehavior(this);
                }
            }
        }

        public void HistoryItemSelected(HistoryWidgetItemBehavior historyWidgetItemBehavior)
        {
            ArController.SelectWidgetLocal(historyWidgetItemBehavior.GetWidget());

            WidgetSelect widgetSelect = new WidgetSelect(historyWidgetItemBehavior.GetWidget().Id);
            SmartAssistanceManager.Instance.Send(widgetSelect.GetMessage());
        }

        public void HistoryItemDeselected(HistoryWidgetItemBehavior historyWidgetItemBehavior)
        {
            ArController.DeselectWidgetLocal(historyWidgetItemBehavior.GetWidget());

            WidgetDeselect widgetDeselect = new WidgetDeselect(historyWidgetItemBehavior.GetWidget().Id);
            SmartAssistanceManager.Instance.Send(widgetDeselect.GetMessage());
        }

        public void DeleteWidget(Widget widget)
        {
            ArController.RemoveTracker(widget.Id);
        }
    }
}