﻿using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.Core.Delegates;
using TMPro;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class NotificationBehavior : MonoBehaviour
    {
        public event OnNotificationDispatchAllowed OnNotificationDispatchAllowed;

        public void ShowNotification(string message)
        {
            GetComponent<TextMeshProUGUI>().text = message;
            GetComponent<Animator>().enabled = true;
            GetComponent<Animator>().Play("NotificationOut", 0, 0f);
        }

        public void OnNewNotificationAllowed()
        {
            if (OnNotificationDispatchAllowed != null && gameObject.activeInHierarchy)
            {
                OnNotificationDispatchAllowed(this);
            }
        }

        public void OnAnimationEnd()
        {
            Destroy(gameObject);
        }
    }
}