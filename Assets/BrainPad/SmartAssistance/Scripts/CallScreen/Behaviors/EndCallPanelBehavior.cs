﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using GameToolkit.Localization;
using TMPro;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class EndCallPanelBehavior : MonoBehaviour
    {
        public TextMeshProUGUI CountdownText;
        public int CountdownSeconds;

        [Header("Notifications")] public LocalizedText BackToContactsIn;
        
        private IEnumerator _countdownCoroutine;

        private void OnEnable()
        {
            StartCoroutine(_countdownCoroutine = CountdownRoutine());
        }

        private void OnDisable()
        {
            if (_countdownCoroutine != null)
            {
                StopCoroutine(_countdownCoroutine);
            }
        }

        public void RemoteUserDisconnectedConfirm()
        {
            if (_countdownCoroutine != null)
            {
                StopCoroutine(_countdownCoroutine);
            }

            SmartAssistanceManager.Instance.ContactsScreen();
        }

        private IEnumerator CountdownRoutine()
        {
            int countDown = CountdownSeconds;
            while (countDown > 0)
            {
                CountdownText.text = BackToContactsIn.Value+ countDown;
                countDown--;
                yield return new WaitForSeconds(1);
            }

            SmartAssistanceManager.Instance.ContactsScreen();
        }
    }
}