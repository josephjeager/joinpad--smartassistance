﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class NotificationAreaBehavior : MonoBehaviour
    {
        private List<string> _notificationQueue;
        private bool _notificationDispatchAllowed;

        private void OnEnable()
        {
            _notificationQueue = new List<string>();
            _notificationDispatchAllowed = true;
            SmartAssistanceManager.Instance.NotificationEvent += OnNotificationEvent;
        }

        private void OnDisable()
        {
            _notificationQueue.Clear();
            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.NotificationEvent -= OnNotificationEvent;
            }
        }

        private void OnNotificationEvent(object sender, string message)
        {
            EnqueueNotification(message);
        }

        private void EnqueueNotification(string message)
        {
            _notificationQueue.Add(message);
            if (_notificationDispatchAllowed)
            {
                DequeueAndShowNotification();
            }
        }

        private void DequeueAndShowNotification()
        {
            if (_notificationQueue.Count > 0)
            {
                _notificationDispatchAllowed = false;
                GameObject notification = Instantiate(Resources.Load<GameObject>("Prefabs/Notification"), transform);
                notification.GetComponent<NotificationBehavior>().OnNotificationDispatchAllowed += OnNotificationDispatchAllowed;
                notification.GetComponent<NotificationBehavior>().ShowNotification(_notificationQueue[0]);
                _notificationQueue.RemoveAt(0);
            }
        }

        private void OnNotificationDispatchAllowed(object sender)
        {
            _notificationDispatchAllowed = true;
            DequeueAndShowNotification();
        }
    }
}