﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.CallScreen.Controllers;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors
{
    [SuppressMessage("ReSharper", "InvertIf")]
    public class CameraFeedBehavior : MonoBehaviour, IPointerDownHandler
    {
        public UiControllerCallScreen UiController;
        public ArController ArController;

        public float WidgetAddDelay = 0.5f;
        private bool _addEnable;

        private void OnEnable()
        {
            _addEnable = true;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (UiController.UiStatus == "ColorPicker" || !_addEnable) return;

            StartCoroutine(LockRoutine());

            if (!string.IsNullOrEmpty(UiController.SelectedWidgetType) && UiController.SelectedWidgetType != ArController.WidgetPaint) // Pen widget is handled by WidgetPaint
            {
                if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    ArController.AddWidgetStream(UiController.SelectedWidgetType, UiController.SelectedWidget, UiController.SelectedColor, Input.GetTouch(0).position, 0, 0);
                }
                else if (Input.GetMouseButtonDown(0))
                {
                    ArController.AddWidgetStream(UiController.SelectedWidgetType, UiController.SelectedWidget, UiController.SelectedColor, eventData.position, 0, 0);
                }
            }
        }

        private IEnumerator LockRoutine()
        {
            _addEnable = false;
            yield return new WaitForSeconds(WidgetAddDelay);
            _addEnable = true;
        }
    }
}