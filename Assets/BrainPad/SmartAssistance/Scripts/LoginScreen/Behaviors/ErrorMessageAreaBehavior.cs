﻿using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.LoginScreen.Behaviors
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    public class ErrorMessageAreaBehavior : MonoBehaviour
    {
        private void OnEnable()
        {
            SmartAssistanceManager.Instance.ErrorEvent += OnErrorEvent;
        }

        private void OnDisable()
        {
            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.ErrorEvent -= OnErrorEvent;
            }
        }

        private void OnErrorEvent(object sender, string message)
        {
            Clear();
            GameObject notification = Instantiate(Resources.Load<GameObject>("Prefabs/ErrorMessage"), transform);
            notification.GetComponent<ErrorBehavior>().ShowNotification(message);
        }

        private void Clear()
        {
            foreach (Transform childTransform in transform)
            {
                Destroy(childTransform.gameObject);
            }
        }
    }
}