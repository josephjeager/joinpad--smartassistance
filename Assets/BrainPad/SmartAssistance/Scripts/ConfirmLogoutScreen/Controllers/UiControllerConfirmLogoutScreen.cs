﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using GameToolkit.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.ConfirmLogoutScreen.Controllers
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class UiControllerConfirmLogoutScreen : MonoBehaviour
    {
        public Button LogoutButton;
        public Button CancelButton;

        [Header("Errors")] public LocalizedText TimeoutErrorCheckYourConnection;
        
        private IEnumerator _resetLogoutRoutine;

        private bool _waitingForLogout;

        private void OnEnable()
        {
            _waitingForLogout = false;
            LogoutButton.interactable = true;
            CancelButton.interactable = true;

            SmartAssistanceManager.Instance.ErrorEvent += OnErrorEvent;
        }

        private void OnDisable()
        {
            if (_resetLogoutRoutine != null)
            {
                StopCoroutine(_resetLogoutRoutine);
            }

            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.ErrorEvent -= OnErrorEvent;
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !_waitingForLogout)
            {
                Cancel();
            }
        }

        public void Confirm()
        {
            SmartAssistanceManager.Instance.IsLoggingIn = false;
            LogoutButton.interactable = false;
            CancelButton.interactable = false;
            SmartAssistanceManager.Instance.Logout();
            StartCoroutine(_resetLogoutRoutine = ResetLoginRoutine());
        }

        public void Cancel()
        {
            if (_resetLogoutRoutine != null)
            {
                StopCoroutine(_resetLogoutRoutine);
            }

            SmartAssistanceManager.Instance.HideLogout();
        }

        private IEnumerator ResetLoginRoutine()
        {
            _waitingForLogout = true;

            yield return new WaitForSeconds(10);

            SmartAssistanceManager.Instance.DispatchError(TimeoutErrorCheckYourConnection.Value);

            LogoutButton.interactable = true;
            CancelButton.interactable = true;

            _waitingForLogout = false;
        }

        private void OnErrorEvent(object sender, string message)
        {
            LogoutButton.interactable = true;
            CancelButton.interactable = true;
        }
    }
}