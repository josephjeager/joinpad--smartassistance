﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace BrainPad.SmartAssistance.Scripts.Models.Messages
{
    [Serializable]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    public class MicEnabled
    {
        public const string PrefixTag = "MicEnabled:";
        private const string Separator = "###___###";

        public readonly bool Enable;

        public MicEnabled(string message)
        {
            string messageCommand = message.Substring(PrefixTag.Length, message.Length - PrefixTag.Length);
            string[] messageSplit = Regex.Split(messageCommand, Separator);
            if (messageSplit.Length == 1)
            {
                Enable = Convert.ToInt32(messageSplit[0]) != 0;
            }
        }

        public MicEnabled(bool enable)
        {
            Enable = enable;
        }

        public string GetMessage()
        {
            return PrefixTag + (Enable ? 1 : 0);
        }
    }
}