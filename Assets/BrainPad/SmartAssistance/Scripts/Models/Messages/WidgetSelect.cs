﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace BrainPad.SmartAssistance.Scripts.Models.Messages
{
    [Serializable]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    public class WidgetSelect
    {
        public const string PrefixTag = "WidgetSelect:";
        private const string Separator = "###___###";

        public readonly int WidgetId;

        public WidgetSelect(string message)
        {
            string messageCommand = message.Substring(PrefixTag.Length, message.Length - PrefixTag.Length);
            string[] messageSplit = Regex.Split(messageCommand, Separator);
            if (messageSplit.Length == 1)
            {
                WidgetId = Convert.ToInt32(messageSplit[0]);
            }
        }

        public WidgetSelect(int widgetId)
        {
            WidgetId = widgetId;
        }

        public string GetMessage()
        {
            return PrefixTag + WidgetId;
        }
    }
}