﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using BrainPad.SmartAssistance.Scripts.CallScreen.Controllers;
using UnityEngine;
using WebSocketSharpUnityMod;

namespace BrainPad.SmartAssistance.Scripts.Models.Messages
{
    [Serializable]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class TrackerRequest
    {
        public readonly float Top;
        public readonly float Left;
        public readonly float Bottom;
        public readonly float Right;
        public readonly float FramePointX;
        public readonly float FramePointY;
        public readonly string WidgetType;
        public readonly Color WidgetColor;
        public readonly string Owner;
        public readonly string WidgetMessage;
        public readonly Texture2D Texture2D;
        public readonly float UnitySpaceWidth;
        public readonly float UnitySpaceHeight;

        public const string PrefixTag = "TrackerRequest:";
        private const string Separator = "###___###";

        public TrackerRequest(string message)
        {
            string messageCommand = message.Substring(PrefixTag.Length, message.Length - PrefixTag.Length);
            string[] messageSplit = Regex.Split(messageCommand, Separator);
            if (messageSplit.Length == 15)
            {
                Top = Convert.ToSingle(messageSplit[0]);
                Left = Convert.ToSingle(messageSplit[1]);
                Bottom = Convert.ToSingle(messageSplit[2]);
                Right = Convert.ToSingle(messageSplit[3]);
                FramePointX = Convert.ToSingle(messageSplit[4]);
                FramePointY = Convert.ToSingle(messageSplit[5]);
                WidgetType = messageSplit[6];
                ColorUtility.TryParseHtmlString("#" + messageSplit[7], out WidgetColor);
                Owner = messageSplit[8];
                WidgetMessage = messageSplit[9];
                if (!messageSplit[10].IsNullOrEmpty())
                {
                    Texture2D = new Texture2D(Convert.ToInt32(messageSplit[11]), Convert.ToInt32(messageSplit[12]))
                    {
                        filterMode = FilterMode.Point,
                        wrapMode = TextureWrapMode.Clamp
                    };
                    Texture2D.LoadImage(Convert.FromBase64String(messageSplit[10]));
                }

                UnitySpaceWidth = Convert.ToSingle(messageSplit[13]);
                UnitySpaceHeight = Convert.ToSingle(messageSplit[14]);
            }
        }

        public TrackerRequest(float top, float left, float bottom, float right, float framePointX, float framePointY, string widgetType, Color widgetColor, string owner, string widgetMessage, Texture2D texture2D, float unitySpaceWidth, float unitySpaceHeight)
        {
            Top = top;
            Left = left;
            Bottom = bottom;
            Right = right;
            FramePointX = framePointX;
            FramePointY = framePointY;
            WidgetType = widgetType;
            WidgetColor = widgetColor;
            Owner = owner;
            WidgetMessage = widgetMessage;
            Texture2D = texture2D;
            UnitySpaceWidth = unitySpaceWidth;
            UnitySpaceHeight = unitySpaceHeight;
        }

        public string GetMessage()
        {
            string message = PrefixTag +
                             Top +
                             Separator +
                             Left +
                             Separator +
                             Bottom +
                             Separator +
                             Right +
                             Separator +
                             FramePointX +
                             Separator +
                             FramePointY +
                             Separator +
                             WidgetType +
                             Separator +
                             ColorUtility.ToHtmlStringRGBA(WidgetColor) +
                             Separator +
                             Owner +
                             Separator +
                             WidgetMessage +
                             Separator;

            if (WidgetType == ArController.WidgetPaint)
            {
                message += Convert.ToBase64String(Texture2D.EncodeToPNG()) +
                           Separator +
                           Texture2D.width +
                           Separator +
                           Texture2D.height +
                           Separator;
            }
            else
            {
                message += "" + Separator + "" + Separator + "" + Separator;
            }

            message += UnitySpaceWidth +
                       Separator +
                       UnitySpaceHeight;
            return message;
        }
    }
}