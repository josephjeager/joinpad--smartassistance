﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace BrainPad.SmartAssistance.Scripts.Models.Messages
{
    [Serializable]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class Tracking
    {
        public const string PrefixTag = "Tracking:";
        private const string Separator = "###___###";

        public readonly int WidgetId;
        public readonly float X;
        public readonly float Y;

        public Tracking(string message)
        {
            string messageCommand = message.Substring(PrefixTag.Length, message.Length - PrefixTag.Length);
            string[] messageSplit = Regex.Split(messageCommand, Separator);
            if (messageSplit.Length == 3)
            {
                WidgetId = Convert.ToInt32(messageSplit[0]);
                X = Convert.ToSingle(messageSplit[1]);
                Y = Convert.ToSingle(messageSplit[2]);
            }
        }

        public Tracking(int widgetId, float x, float y)
        {
            WidgetId = widgetId;
            X = x;
            Y = y;
        }

        public string GetMessage()
        {
            return PrefixTag + WidgetId + Separator + X + Separator + Y;
        }
    }
}