﻿using System;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.Models
{
    [Serializable]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local")]
    [SuppressMessage("ReSharper", "NotAccessedField.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    public class Widget
    {
        public int Id; // Id of the recognized object 
        public string Type; // Literal widget type 
        public string Owner; // Device Unique ID
        public GameObject Instance;
        public bool SelectedLocal;
        public bool SelectedRemote;

        private GameObject _highlightLocal;
        private GameObject _highlightRemote;

        // Used to smooth the movement by Lerp
        public Vector3 StartPosition;
        public Vector3 TargetPosition;
        public float LastUpdate;
        public float CurrentLerpTime;
        public float LerpTime;

        public Widget(int id, string type, string owner, GameObject instance)
        {
            Id = id;
            Type = type;
            Instance = instance;
            Owner = owner;
            _highlightLocal = Instance.transform.GetChild(0).gameObject;
            _highlightRemote = Instance.transform.GetChild(1).gameObject;
        }

        public void SelectLocal()
        {
            _highlightLocal.SetActive(true);
            SelectedLocal = true;
        }

        public void DeselectLocal()
        {
            _highlightLocal.SetActive(false);
            SelectedLocal = false;
        }

        public void SelectRemote()
        {
            _highlightRemote.SetActive(true);
            SelectedRemote = true;
        }

        public void DeselectRemote()
        {
            _highlightRemote.SetActive(false);
            SelectedRemote = false;
        }

        // Resets the Lerp, needed by LerpUpdate()
        public void LerpInitialPosition(Vector3 position)
        {
            CurrentLerpTime = 0;
            LerpTime = 0;
            LastUpdate = Time.time;

            Instance.transform.localPosition = position;
            TargetPosition = new Vector3(Instance.transform.localPosition.x, Instance.transform.localPosition.y, Instance.transform.localPosition.z);
        }

        // Updates the Lerp, needed by LerpUpdate()
        public void LerpNewPosition(Vector3 position)
        {
            CurrentLerpTime = 0;
            LerpTime = Time.time - LastUpdate;
            if (LerpTime > 0.5)
            {
                LerpTime = 0.5f;
            }

            LastUpdate = Time.time;

            StartPosition = new Vector3(Instance.transform.localPosition.x, Instance.transform.localPosition.y, Instance.transform.localPosition.z);
            TargetPosition = position;
        }

        public void LerpUpdate()
        {
            if (!(LerpTime > 0) || !Instance.activeInHierarchy) return;

            CurrentLerpTime += Time.deltaTime;
            if (CurrentLerpTime > LerpTime)
            {
                CurrentLerpTime = LerpTime;
            }

            float lerpTime = CurrentLerpTime / LerpTime;

            // Ease In Interpolation
            // lerpTime = 1f - Mathf.Cos(lerpTime * Mathf.PI * 0.5f);

            Instance.transform.localPosition = Vector3.Lerp(StartPosition, TargetPosition, lerpTime);
        }
    }
}