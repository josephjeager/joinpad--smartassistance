﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using GameToolkit.Localization;
using TMPro;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.DisconnectedFromServerScreen.Behaviors
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class DisconnectedFromServerPanelBehavior : MonoBehaviour
    {
        public TextMeshProUGUI CountdownText;
        public int CountdownSeconds;
        
        [Header("Notifications")] public LocalizedText BackToLoginIn;
        
        private IEnumerator _countdownCoroutine;

        private void OnEnable()
        {
            StartCoroutine(_countdownCoroutine = CountdownRoutine());
        }

        private void OnDisable()
        {
            if (_countdownCoroutine != null)
            {
                StopCoroutine(_countdownCoroutine);
            }
        }

        public void GoToLoginConfirm()
        {
            if (_countdownCoroutine != null)
            {
                StopCoroutine(_countdownCoroutine);
            }

            SmartAssistanceManager.Instance.LoginScreen();
        }

        private IEnumerator CountdownRoutine()
        {
            int countDown = CountdownSeconds;
            while (countDown > 0)
            {
                CountdownText.text = BackToLoginIn.Value + countDown;
                countDown--;
                yield return new WaitForSeconds(1);
            }

            SmartAssistanceManager.Instance.LoginScreen();
        }
    }
}