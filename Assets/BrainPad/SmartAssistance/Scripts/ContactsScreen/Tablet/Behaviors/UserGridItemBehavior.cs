﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.Core.Http;
using SmartAssistanceSDK;
using SmartAssistanceSDK.Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.ContactsScreen.Tablet.Behaviors
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class UserGridItemBehavior : MonoBehaviour
    {
        public TextMeshProUGUI NoProfilePicCapitalLetter;
        public Image ProfilePic;
        public TextMeshProUGUI Name;
        public TextMeshProUGUI Lastname;
        public Image StatusBar;
        public Image CallIcon;
        public GameObject UserIsBusy;
        public TextMeshProUGUI UserIsBusyMessage;

        public Color32 ColorOnline = new Color32(56, 172, 73, 255);
        public Color32 ColorAway = new Color32(237, 34, 36, 255);
        public Color32 ColorBusy = new Color32(237, 34, 36, 255);
        public Color32 ColorInCall = new Color32(255, 146, 30, 255);

        private SAUser _user;
        private UserGridBehavior _userGridBehavior;

        private IEnumerator _hideUserIsBusyCoroutine;

        private void OnDisable()
        {
            if (_hideUserIsBusyCoroutine != null)
            {
                StopCoroutine(_hideUserIsBusyCoroutine);
            }
        }

        public void SetUser(SAUser user)
        {
            _user = user;
            Name.text = _user.name;
            Lastname.text = _user.lastname;
            if (_user.name.Length > 0)
            {
                NoProfilePicCapitalLetter.text = _user.name[0].ToString().ToUpper();
            }

            switch (_user.status)
            {
                case SAUserStatus.Available:
                    CallIcon.color = ColorOnline;
                    StatusBar.color = ColorOnline;
                    break;
                case SAUserStatus.Away:
                    CallIcon.color = ColorAway;
                    StatusBar.color = ColorAway;
                    break;
                case SAUserStatus.Busy:
                    CallIcon.color = ColorBusy;
                    StatusBar.color = ColorBusy;
                    break;
                case SAUserStatus.InCall:
                    CallIcon.color = ColorInCall;
                    StatusBar.color = ColorInCall;
                    break;
                default:
                    CallIcon.color = ColorAway;
                    StatusBar.color = ColorAway;
                    break;
            }

            ProfilePicturesLoader.Instance.LoadProfilePic(_user, ProfilePic.mainTexture.width, ProfilePic.mainTexture.height, ((Texture2D) ProfilePic.mainTexture).format, sprite =>
            {
                if (sprite != null)
                {
                    ProfilePic.sprite = sprite;
                    ProfilePic.gameObject.SetActive(true);
                }
            });
        }

        public void SetUserGridBehavior(UserGridBehavior userGridBehavior)
        {
            _userGridBehavior = userGridBehavior;
        }

        public void OnClick()
        {
            if (_userGridBehavior != null)
            {
                if (_user.status == SAUserStatus.Available)
                {
                    SmartAssistanceManager.Instance.CallUser(_user);
                }
                else
                {
                    if (_user.status == SAUserStatus.Busy)
                    {
                        UserIsBusyMessage.text = "User is Busy,\nretry later.";
                    }
                    else if (_user.status == SAUserStatus.InCall)
                    {
                        UserIsBusyMessage.text = "User is In Call,\nretry later.";
                    }
                    else if (_user.status == SAUserStatus.Away)
                    {
                        UserIsBusyMessage.text = "User is Away,\nretry later.";
                    }

                    UserIsBusy.SetActive(true);
                    StartCoroutine(_hideUserIsBusyCoroutine = HideUserIsBusyRoutine());
                }
            }
        }

        private IEnumerator HideUserIsBusyRoutine()
        {
            yield return new WaitForSeconds(1.5f);
            UserIsBusy.SetActive(false);
        }
    }
}