﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.ContactsScreen.Smartphone.Behaviors
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "InvertIf")]
    [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
    public class SwipeUpForHelpBehavior : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public int PixelsSwipeToTriggerAction;
        public float ResetAnimationTime = 0.5f;

        private Vector3 _initPosition;
        private Color32 _initColor;
        private readonly Color32 _highlightColor = new Color32(139, 161, 205, 50);
        private Color32 _lerpStartColor;
        private Vector3 _lerpStartPosition;
        private Vector3 _lerpTargetPosition;
        private float _currentLerpTime;
        private bool _isLerping;
        private bool _dragEnable;

        private IEnumerator _disableDragCoroutine;

        private void Start()
        {
            _initPosition = transform.localPosition;
            _initColor = transform.GetChild(1).GetComponent<Image>().color;
        }

        private void OnEnable()
        {
            _dragEnable = true;
        }

        private void OnDisable()
        {
            if (_disableDragCoroutine != null)
            {
                StopCoroutine(_disableDragCoroutine);
            }
        }

        private void Update()
        {
            if (_isLerping)
            {
                _currentLerpTime += Time.deltaTime;
                if (_currentLerpTime > ResetAnimationTime)
                {
                    _currentLerpTime = ResetAnimationTime;
                }

                float lerpTime = _currentLerpTime / ResetAnimationTime;
                lerpTime = lerpTime * lerpTime * lerpTime * (lerpTime * (6f * lerpTime - 15f) + 10f);
                transform.localPosition = Vector3.Lerp(_lerpStartPosition, _lerpTargetPosition, lerpTime);

                Color32 lerpColor = Color32.Lerp(_lerpStartColor, _initColor, lerpTime);
                transform.GetChild(0).GetComponent<TMP_Text>().color = lerpColor;
                transform.GetChild(1).GetComponent<Image>().color = lerpColor;
                if (lerpTime >= 1)
                {
                    _isLerping = false;
                }
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _isLerping = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_dragEnable)
            {
                if (transform.localPosition.y + eventData.delta.y < _initPosition.y)
                {
                    transform.localPosition = new Vector2(transform.localPosition.x, _initPosition.y);
                }
                else
                {
                    transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y + eventData.delta.y);
                }

                float percentageDragged = -1 * (_initPosition.y - transform.localPosition.y) / PixelsSwipeToTriggerAction;
                float lerpPercentage = percentageDragged * percentageDragged * percentageDragged * (percentageDragged * (6f * percentageDragged - 15f) + 10f);
                Color32 lerpColor = Color32.Lerp(_initColor, _highlightColor, lerpPercentage);
                transform.GetChild(0).GetComponent<TMP_Text>().color = lerpColor;
                transform.GetChild(1).GetComponent<Image>().color = lerpColor;
                if (percentageDragged >= 1)
                {
                    _isLerping = false;

                    transform.GetChild(0).GetComponent<TMP_Text>().color = new Color(0, 0, 0, 0);
                    transform.GetChild(1).GetComponent<Image>().color = new Color(0, 0, 0, 0);

                    StartCoroutine(_disableDragCoroutine = OnOpenInstructionsRoutine());
                    SmartAssistanceManager.Instance.InstructionsScreenWithAnimation();
                }
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (_dragEnable)
            {
                LerpStart(_initPosition);
            }
        }

        private void LerpStart(Vector3 position)
        {
            _isLerping = true;
            _currentLerpTime = 0;
            _lerpStartPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
            _lerpTargetPosition = position;
            _lerpStartColor = transform.GetChild(0).GetComponent<TMP_Text>().color;
        }

        private IEnumerator OnOpenInstructionsRoutine()
        {
            _dragEnable = false;

            yield return new WaitForSeconds(1);

            transform.localPosition = _initPosition;
            transform.GetChild(0).GetComponent<TMP_Text>().color = _initColor;
            transform.GetChild(1).GetComponent<Image>().color = _initColor;

            _dragEnable = true;
        }
    }
}