﻿using Org.WebRtc;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using WebRtcWrapper.Model;
using WebRtcWrapper.Utilities;

namespace WebRtcWrapper
{
    public interface IWebRtcControl
    {
        void Initialize();
        void SendPeerDataChannelMessage(string msg);
        void SendPeerMessageData(string msg);
        void ConnectToServer(string peerName);
        void ConnectToServer(string host, string port, string peerName);
        void ConnectToPeer();
        void DisconnectFromPeer();
        void DisconnectFromServer();
        ObservableCollection<Peer> Peers { get; set; }
        bool IsConnected { get; set; }
        bool IsMicrophoneEnabled { get; set; }
        bool IsCameraEnabled { get; set; }
        bool IsConnecting { get; set; }
        bool IsDisconnecting { get; set; }
        RTCPeerConnectionHealthStats PeerConnectionHealthStats { get; set; }
        void UpdatePeerConnHealthStatsVisibilityHelper();
        bool IsConnectedToPeer { get; set; }
        void UpdateLoopbackVideoVisibilityHelper();
        bool VideoLoopbackEnabled { get; set; }
        bool PeerConnectionHealthStatsEnabled { get; set; }
        bool ShowLoopbackVideo { get; set; }
        bool ShowPeerConnectionHealthStats { get; set; }
        bool IsReadyToConnect { get; set; }
        bool IsReadyToDisconnect { get; set; }
        ObservableCollection<IceServer> IceServers { get; set; }
        IceServer NewIceServer { get; set; }
        ObservableCollection<CodecInfo> AudioCodecs { get; set; }
        ObservableCollection<CodecInfo> VideoCodecs { get; set; }
        CodecInfo SelectedAudioCodec { get; set; }
        CodecInfo SelectedVideoCodec { get; set; }
        ValidableNonEmptyString Ip { get; set; }
        ValidableIntegerString Port { get; set; }
        bool HasServer { get; set; }
        ValidableNonEmptyString NtpServer { get; set; }
        bool NtpSyncEnabled { get; set; }
        Boolean NtpSyncInProgress { get; set; }
        Peer SelectedPeer { get; set; }
        ObservableCollection<MediaDevice> Cameras { get; set; }
        ObservableCollection<MediaDevice> Microphones { get; set; }
        ObservableCollection<MediaDevice> AudioPlayoutDevices { get; set; }
        MediaDevice SelectedCamera { get; set; }
        ObservableCollection<String> AllCapRes { get; set; }
        String SelectedCapResItem { get; set; }
        ObservableCollection<CaptureCapability> AllCapFps { get; set; }
        CaptureCapability SelectedCapFpsItem { get; set; }
        MediaDevice SelectedMicrophone { get; set; }
        MediaDevice SelectedAudioPlayoutDevice { get; set; }
        event Action OnInitialized;
        event Action<int, string> OnPeerConnected;
        event Action<int> OnPeerDisconnected;
        event Action OnPeerConnectionClosed;
        event Action<int, string> OnPeerMessageDataReceived;
        event Action<string> OnStatusMessageUpdate;
        event Action<int, IDataChannelMessage> OnPeerDataChannelReceived;
        event Func<int, string, Task<bool>> OnPeerSdpOfferReceived;
        event Action<int, string> OnPeerSdpAnswerReceived;
        void MuteMicrophone();
        void UnmuteMicrophone();
    }
}
