﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Globalization;
using UnityEngine.UI;
using System.Net;


#if WINDOWS_UWP
using Windows.Networking.Sockets;
using System.Threading.Tasks;
using Windows.Storage.Streams;
#endif

using UnityEngine;

public class WebSocket : MonoBehaviour
{

    public GameObject Square;
    public GameObject Arrow;
    public GameObject Circle;
    public GameObject DigitBtn;
    public GameObject Widgets;
    public GameObject widget;

    public int OldObject;

    JsonWSMessage JsonMessage;

    const int ON_OFF = 0;
    const int COORDINATES = 1;
    const int ARROW_ACTIVE = 4;
    const int SQUARE_ACTIVE = 6;
    const int CIRCLE_ACTIVE = 5;
    const int MY_COORDINATES = 3;

    const int SET_WHITE = 7;
    const int SET_RED = 8;
    const int SET_YELLOW = 9;
    const int SET_GREEN = 10;
    const int SET_BLUE = 11;

    const int UP = 15;
    const int DOWN = 16;
    const int LEFT = 17;
    const int RIGHT = 18;
    const int ZOOM_IN = 19;
    const int ZOOM_OUT = 20;
    const int INCREASE_Z = 21;
    const int DECREASE_Z = 22;

    // private float ArFovWidth = 915f;
    // private float ArFovHeight = 515f;
    // private float ArFovOffsetX = 210f;
    // private float ArFovOffsetY = 85f;

    //private float ArFovWidth = 1280f;
    //private float ArFovHeight = 720f;

    private float ArFovWidth = 915f;
    private float ArFovHeight = 515f;
    private float ArFovOffsetX = 210f;
    private float ArFovOffsetY = 85f;

#if WINDOWS_UWP
    private MessageWebSocket MessageSocket;
    private MessageWebSocket MessageSocketFFmpeg;
    private DataWriter MessageWriter;
#endif

    public static WebSocket Instance { private set; get; }
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {

        Ping p = new Ping("google.com");

        Widgets = GameObject.Find("Widgets");
        widget = GameObject.Find("widget");
        // Button b = DigitBtn.GetComponent<Button>();
        //  SpatialMapping.Instance.MappingEnabled = true;
        GameObject.Find("Cube").SetActive(false);
        Square = GameObject.Find("quadrato");
        Arrow = GameObject.Find("freccia");
        Circle = GameObject.Find("cerchio");

        Square.SetActive(false);
        Arrow.SetActive(false);
        Circle.SetActive(false);

        // Square.GetComponentInChildren<Renderer>().material.color = new Color(0,0,0,0);
        // Arrow.GetComponentInChildren<Renderer>().material.color = new Color(0,0,0,0);
        // Circle.GetComponentInChildren<Renderer>().material.color = new Color(0,0,0,0);

        //        Arrow.SetActive(false);

#if WINDOWS_UWP

       // RunFFmpeg();
        ConnectAsync();

#endif

    }

    // Update is called once per frame
    void Update()
    {
        if (JsonMessage != null)
        {
            int message = Int32.Parse(JsonMessage.message);
            switch (message)
            {
                case (COORDINATES):
                    float x = float.Parse(JsonMessage.x, CultureInfo.InvariantCulture.NumberFormat);
                    float y = float.Parse(JsonMessage.y, CultureInfo.InvariantCulture.NumberFormat);
                    SetCoordinates(x, y);
                    break;
                case (ON_OFF):
                    SetActiveDeactive();
                    break;
                case (MY_COORDINATES):
                    GetObjCoordinates();
                    break;
                case (SQUARE_ACTIVE):
                    ActiveObject(SQUARE_ACTIVE);
                    break;
                case (CIRCLE_ACTIVE):
                    ActiveObject(CIRCLE_ACTIVE);
                    break;
                case (ARROW_ACTIVE):
                    ActiveObject(ARROW_ACTIVE);
                    break;
                case (SET_WHITE):
                    SetColor(new Color32(255,255,255,1));
                    break;
                case (SET_RED):
                    SetColor(new Color32(233,64,64,1));
                    break;
                case (SET_YELLOW):
                    SetColor(new Color32(252,238,33,1));
                    break;
                case (SET_GREEN):
                    SetColor(new Color32(122,201,67,1));
                    break;
                case (SET_BLUE):
                    SetColor(new Color32(63,169,245,1));
                    break;
                case (UP):
                    SetCoordinates(UP);
                    break;
                case (DOWN):
                    SetCoordinates(DOWN);
                    break;
                case (LEFT):
                    SetCoordinates(LEFT);
                    break;
                case (RIGHT):
                    SetCoordinates(RIGHT);
                    break;
                case (ZOOM_IN):
                    SetCoordinates(ZOOM_IN);
                    break;
                case (ZOOM_OUT):
                    SetCoordinates(ZOOM_OUT);
                    break;
                case (INCREASE_Z):
                    SetCoordinates(INCREASE_Z);
                    break;
                case (DECREASE_Z):
                    SetCoordinates(DECREASE_Z);
                    break;

            }
            JsonMessage = null;
        }
    }

    private void SetCoordinates(int action)
    {
        float x, y, z;
        float lx, ly, lz;
        float step = 0.05f;

        x = widget.transform.localPosition.x;
        y = widget.transform.localPosition.y;
        z = widget.transform.localPosition.z;

        lx = widget.transform.localScale.x;
        ly = widget.transform.localScale.y;
        lz = widget.transform.localScale.z;

        switch (action)
        {
            case (UP):
                widget.transform.localPosition = new Vector3(x, y + step, z);
                break;
            case (DOWN):
                widget.transform.localPosition = new Vector3(x, y - step, z);
                break;
            case (LEFT):
                widget.transform.localPosition = new Vector3(x + step, y, z);
                break;
            case (RIGHT):
                widget.transform.localPosition = new Vector3(x - step, y, z);
                break;
            case (ZOOM_IN):
                widget.transform.localScale += new Vector3(step, step, step);
                break;
            case (ZOOM_OUT):
                widget.transform.localScale -= new Vector3(step, step, step);
                break;
            case (INCREASE_Z):
                widget.transform.localPosition = new Vector3(x , y, z+step);
                break;
            case (DECREASE_Z):
                widget.transform.localPosition = new Vector3(x, y, z-step);
                break;
        }
    }

    private void SetColor(Color color)
    {
        if (Arrow.activeInHierarchy)
            GameObject.Find("freccia").GetComponent<Renderer>().material.color = color;
        else if (Square.activeInHierarchy)
            GameObject.Find("quadrato").GetComponent<Renderer>().material.color = color;
        else if (Circle.activeInHierarchy)
            GameObject.Find("cerchio").GetComponent<Renderer>().material.color = color;
    }

    private void ActiveObject(int objActive)
    {
        switch (objActive)
        {
            case ARROW_ACTIVE:
                if (!Arrow.activeInHierarchy)
                {
                    if (Square.activeInHierarchy)
                    {
                        Arrow.transform.position = Square.transform.position;
                        Square.SetActive(false);
                        Arrow.SetActive(true);
                        //Arrow.GetComponentInChildren<Renderer>().material.color = Square.GetComponentInChildren<Renderer>().material.color;
                        //Square.GetComponentInChildren<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    else if(Circle.activeInHierarchy)
                    {
                        Arrow.transform.position = Circle.transform.position;
                        Circle.SetActive(false);
                        Arrow.SetActive(true);
                        //Arrow.GetComponentInChildren<Renderer>().material.color = Circle.GetComponentInChildren<Renderer>().material.color;
                        //Circle.GetComponentInChildren<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    else { 
                        //Arrow.GetComponentInChildren<Renderer>().material.color = new Color(255,255,255);
                        Arrow.SetActive(true);
                    }
                }
                break;

            case SQUARE_ACTIVE:
                if (!Square.activeInHierarchy)
                {
                    if (Arrow.activeInHierarchy)
                    {
                        Square.transform.position = Arrow.transform.position;
                        Arrow.SetActive(false);
                        Square.SetActive(true);
                        //Square.GetComponentInChildren<Renderer>().material.color = Arrow.GetComponentInChildren<Renderer>().material.color;
                        //Arrow.GetComponentInChildren<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    else if (Circle.activeInHierarchy)
                    {
                        Square.transform.position = Circle.transform.position;
                        Circle.SetActive(false);
                        Square.SetActive(true);
                        //Square.GetComponentInChildren<Renderer>().material.color = Circle.GetComponentInChildren<Renderer>().material.color;
                        //Circle.GetComponentInChildren<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    else
                    {
                        //Square.GetComponentInChildren<Renderer>().material.color = new Color(255, 255, 255);
                        Square.SetActive(true);
                    }
                }
                break;

            case CIRCLE_ACTIVE:
                if (!Circle.activeInHierarchy)
                {
                    if (Arrow.activeInHierarchy)
                    {
                        Circle.transform.position = Arrow.transform.position;
                        Arrow.SetActive(false);
                        Circle.SetActive(true);
                        //Circle.GetComponentInChildren<Renderer>().material.color = Arrow.GetComponentInChildren<Renderer>().material.color;
                        //Arrow.GetComponentInChildren<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    else if (Square.activeInHierarchy)
                    {
                        Circle.transform.position = Square.transform.position;
                        Square.SetActive(false);
                        Circle.SetActive(true);
                        //Circle.GetComponentInChildren<Renderer>().material.color = Square.GetComponentInChildren<Renderer>().material.color;
                        //Square.GetComponentInChildren<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    else
                    {
                        //Square.GetComponentInChildren<Renderer>().material.color = new Color(255, 255, 255);
                        Circle.SetActive(true);
                    }
                }
                break;
        }
    }

    private float NormalizeCoordX(float x)
    {
        return ArFovWidth * (x / 100)  + ArFovOffsetX;
    }

    private float NormalizeCoordY(float y)
    {
        return ArFovHeight * (y / 100) + ArFovOffsetY;
    }

    private void GetObjCoordinates()
    {
#if WINDOWS_UWP
        SendAsync();
#endif
    }

    private void SetActiveDeactive()
    {
        GameObject currentGameObject = null;

        if (Square.activeInHierarchy) {
            currentGameObject = Square;
            OldObject = SQUARE_ACTIVE;
        }
        else if (Arrow.activeInHierarchy)
        {
            currentGameObject = Arrow;
            OldObject = ARROW_ACTIVE;
        }
        else if (Circle.activeInHierarchy)
        {
            currentGameObject = Circle;
            OldObject = CIRCLE_ACTIVE;
        }

        if (currentGameObject != null)
        {
            if (currentGameObject.activeInHierarchy)
            {
                currentGameObject.SetActive(false);
            }
            else
            {
                currentGameObject.SetActive(true);
            }
        }
        else
        {
            ActiveObject(OldObject);
        }
    }

    public void SetCoordinatesTest()
    {
        SetCoordinates(1280, 720);
    }


    private void SetCoordinates(float x, float y)
    {
        x = NormalizeCoordX(x);
        y = NormalizeCoordY(y);

        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(x, y, 0));

        if (Physics.Raycast(ray, out hitInfo, 100, SpatialMapping.PhysicsRaycastMask))
        {
            Widgets.transform.position = hitInfo.point;
            Widgets.transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward, Camera.main.transform.up);
        }
    }

    /*
    private void SetCoordinates(float x, float y)
    {

        x = NormalizeCoordX(x);
        y = NormalizeCoordY(y);

        //x = 915f;
        //y = 515f;

        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(x, y, 0));

        if (Physics.Raycast(ray, out hitInfo, 100, SpatialMapping.PhysicsRaycastMask))
        {
            if (Square.activeInHierarchy)
            {
                Square.transform.position = hitInfo.point;
                Square.transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward, Camera.main.transform.up);
#if WINDOWS_UWP
                SendAsync();
#endif
            }
            else if (Arrow.activeInHierarchy)
            {
                Arrow.transform.position = hitInfo.point;
                Arrow.transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward, Camera.main.transform.up);

#if WINDOWS_UWP
                SendAsync();
#endif
            }
            else if (Circle.activeInHierarchy)
            {
                Circle.transform.position = hitInfo.point;
                Circle.transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward, Camera.main.transform.up);

#if WINDOWS_UWP
                SendAsync();
#endif
            }
        }
    }*/

#if WINDOWS_UWP
    private async Task RunFFmpeg()
    {
       MessageSocketFFmpeg = new MessageWebSocket();
       MessageSocketFFmpeg.Control.MessageType = SocketMessageType.Utf8;
      
    
       //MessageSocket.MessageReceived += MessageReceived;
       try{
            Uri ffmpeg = new Uri("ws://192.168.43.145:8084");
            await MessageSocketFFmpeg.ConnectAsync(ffmpeg);
            MessageSocketFFmpeg.Dispose();
            //MessageWriter = new DataWriter(MessageSocket.OutputStream);
        }
        catch (Exception ex) // For debugging
        {
            // Error happened during connect operation.
            MessageSocketFFmpeg.Dispose();
            MessageSocketFFmpeg = null;
            return;
        }
    }

    private async Task ConnectAsync()
    {

        MessageSocket = new MessageWebSocket();
        MessageSocket.Control.MessageType = SocketMessageType.Utf8;
        MessageSocket.MessageReceived += MessageReceived;
        //   MessageSocket.Closed += OnClosed;

        try
        {
            Uri myUri = new Uri("ws://cvapi8850.cloudapp.net:8085");
            //Uri myUri = new Uri("ws://192.168.1.8:8085");
            await MessageSocket.ConnectAsync(myUri);
            MessageWriter = new DataWriter(MessageSocket.OutputStream);
        }
        catch (Exception ex) // For debugging
        {
            // Error happened during connect operation.
            MessageSocket.Dispose();
            MessageSocket = null;
            return;
        }

        // The default DataWriter encoding is Utf8.
        //MessageWriter = new DataWriter(MessageSocket.OutputStream);
        //   rootPage.NotifyUser("Connected", NotifyType.StatusMessage);
    }

    private void MessageReceived(MessageWebSocket sender, MessageWebSocketMessageReceivedEventArgs args)
    {
        DataReader reader = args.GetDataReader();
        reader.UnicodeEncoding = UnicodeEncoding.Utf8;
        string read = reader.ReadString(reader.UnconsumedBufferLength);

        // jsonWSMessage = JsonConvert.DeserializeObject<JsonWSMessage>(read);
        read.Trim();
        string[] sp = read.Split(',');
        string message = sp[0].Replace("{message:", "").Replace("}", "");
        JsonMessage = new JsonWSMessage();
        JsonMessage.message = message;

        if (sp.Length > 1)
        {
            string x = sp[1].ToString().Replace("x:", "");
            string y = sp[2].ToString().Replace("y:", "").Replace("}", ""); ;
            //string z = sp[3].Replace("z:", "").Replace("}", "");
            JsonMessage.x = x;
            JsonMessage.y = y;
            //jsonWSMessage.z = z;
        }

       // n = 1;
    }

    async Task SendAsync()
    {
        string message = "";
        if (Square.activeInHierarchy)
        {
            message = Square.transform.position.ToString();
        }
        else if (Arrow.activeInHierarchy)
        {
            message = Arrow.transform.position.ToString();
        }
        if (String.IsNullOrEmpty(message))
        {
            return;
        }
        // Buffer any data we want to send.
        MessageWriter.WriteString(message);
        try
        {
            // Send the data as one complete message.
            await MessageWriter.StoreAsync();
        }
        catch (Exception ex)
        {
            return;
        }

    }

    private string jsonParser(string s)
    {

        return s;
    }

#endif

    public class JsonWSMessage
    {
        public string message { get; set; }
        public string x { get; set; }
        public string y { get; set; }
        // public string z { get; set; }
    }
}
