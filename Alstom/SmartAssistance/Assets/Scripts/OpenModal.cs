﻿using UnityEngine;
using HoloToolkit.Unity.InputModule;

public class OpenModal : MonoBehaviour, IInputClickHandler
{
    float lerpTime = 0.5f;
    float currentLerpTime;
    Vector3 startPosition;
    Vector3 endPosition;
    Vector3 startDimension;
    Vector3 endDimensionOpening;
    float positionDelta = 0.2f;

    public string imgStatus;

    private Renderer video;
    private MovieTexture videoTexture;

    private void Start()
    {
        imgStatus = "start";
        startPosition = gameObject.transform.position;
        endPosition = startPosition + transform.up * positionDelta;
        startDimension = gameObject.transform.localScale;
        video = gameObject.GetComponent<Renderer>();
        if (transform.name == "ImageVert") { endDimensionOpening = new Vector3(0.027f, 1f, 0.048f); }
        else { endDimensionOpening = new Vector3(0.048f, 1f, 0.027f); }
        if (transform.name == "Video") { videoTexture = (MovieTexture)video.material.mainTexture; }
    }

    private void Update()
    {
        if (imgStatus == "open")
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime) { currentLerpTime = lerpTime; }
            float perc = currentLerpTime / lerpTime;
            transform.position = Vector3.Lerp(startPosition, endPosition, perc);
            transform.localScale = Vector3.Lerp(startDimension, endDimensionOpening, perc);
        }

        if (imgStatus == "close")
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime) { currentLerpTime = lerpTime; }
            float perc = currentLerpTime / lerpTime;
            transform.position = Vector3.Lerp(endPosition, startPosition, perc);
            transform.localScale = Vector3.Lerp(transform.localScale, startDimension, perc);
        }
    }

    public void OnInputClicked(InputEventData eventData)
    {
        currentLerpTime = 0f;
        switch (imgStatus)
        {
            case "start":
                imgStatus = "open";
                if (transform.name == "Video") { videoTexture.Play(); }
                break;
            case "open":
                imgStatus = "close";
                if (transform.name == "Video") { videoTexture.Stop(); }
                break;
            case "close":
                imgStatus = "open";
                if (transform.name == "Video") { videoTexture.Play(); }
                break;
        }

    }
}