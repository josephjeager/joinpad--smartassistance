﻿using UnityEngine;
using UnityEngine.SceneManagement;
using HoloToolkit.Unity.InputModule;
using System.Collections.Generic;

public class OpenStep : MonoBehaviour, IInputClickHandler
{
    public static string currentOpenedStep = "";
    public string stepReference = "";
    List<GameObject> menuBtns;

    private void Start()
    {
        menuBtns = new List<GameObject>()
        {
            GameObject.Find("MenuBtn0"),
            GameObject.Find("MenuBtn1"),
            GameObject.Find("MenuBtn2"),
            GameObject.Find("MenuBtn3"),
            GameObject.Find("MenuBtn4")
        };
    }

    public void OnInputClicked(InputEventData eventData)
    {
        if (currentOpenedStep != "")
        {
            SceneManager.UnloadSceneAsync(currentOpenedStep);
        }
        if (GameObject.Find("InstructionLabel") != null)
        {
            GameObject.Find("InstructionLabel").SetActive(false);
        }

        for (int i = 0; i < 5; i++)
        {
            menuBtns[i].GetComponentInChildren<TextMesh>().fontStyle = FontStyle.Normal;
            menuBtns[i].GetComponentInChildren<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        }
        gameObject.GetComponentInChildren<TextMesh>().fontStyle = FontStyle.Bold;
        gameObject.GetComponentInChildren<SpriteRenderer>().color = new Color(1, 1, 1, 1);


        SceneManager.LoadScene(stepReference, LoadSceneMode.Additive);
        currentOpenedStep = stepReference;
    }
}
