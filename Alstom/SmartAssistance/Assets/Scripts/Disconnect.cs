﻿using System;
using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#if WINDOWS_UWP
using Windows.Networking.Sockets;
using System.Threading.Tasks;
using Windows.Storage.Streams;
#endif

public class Disconnect : MonoBehaviour, IInputClickHandler{

    public GameObject DigitBtn;
#if WINDOWS_UWP
    private MessageWebSocket MessageSocket;
    private MessageWebSocket MessageSocketFFmpeg;
    private DataWriter MessageWriter;
#endif
    void Start () {
        DigitBtn = GameObject.Find("DigitBtn");       
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnInputClicked(InputEventData eventData)
    {
#if WINDOWS_UWP
        DisconnectFFmpeg();
#endif
        SceneManager.LoadScene("Call");
    }

#if WINDOWS_UWP
    private async Task DisconnectFFmpeg()
    {
        MessageSocketFFmpeg = new MessageWebSocket();
        MessageSocketFFmpeg.Control.MessageType = SocketMessageType.Utf8;
        try
        {
            Uri ffmpeg = new Uri("ws://192.168.43.145:8085");
            await MessageSocketFFmpeg.ConnectAsync(ffmpeg);
        }
        catch (Exception ex) // For debugging
        {
            // Error happened during connect operation.
            MessageSocketFFmpeg.Dispose();
            MessageSocketFFmpeg = null;
            return;
        }
    }

#endif
}
