﻿using UnityEngine;
using UnityEngine.SceneManagement;
using HoloToolkit.Unity.InputModule;

public class GoTo3D : MonoBehaviour, IInputClickHandler
{
    private void Start()
    {

    }

    public void OnInputClicked(InputEventData eventData)
    {
        SceneManager.LoadScene("3DModel");
    }
}
