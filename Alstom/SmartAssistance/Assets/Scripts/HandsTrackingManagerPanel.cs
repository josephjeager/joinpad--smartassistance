﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

namespace HoloToolkit.Unity
{
    public class HandsTrackingManagerPanel : Singleton<HandsTrackingManager>
    {
        public GameObject TargetObject;
        private bool _tracking;

        private Vector3 _startPosition;
        private Vector3 _lastPosition;

        //  private GestureRecognizer _recognizer;
        private HashSet<uint> _trackedHands = new HashSet<uint>();
        
        void Awake()
        {
            InteractionManager.SourcePressed += InteractionManager_SourcePressed;
            InteractionManager.SourceReleased += InteractionManager_SourceReleased;
            InteractionManager.SourceUpdated += InteractionManager_SourceUpdated;
        }

        private void InteractionManager_SourceUpdated(InteractionSourceState state)
        {
            uint id = state.source.id;

            if (IsTracking())
            {
                if (state.source.kind == InteractionSourceKind.Hand)
                {
                    Vector3 pos;
                    if (state.properties.location.TryGetPosition(out pos))
                    {
                        _lastPosition = pos;
                        float deltaX = _lastPosition.x - _startPosition.x;
                        float deltaY = _lastPosition.y - _startPosition.y;
                        float deltaZ = _lastPosition.z - _startPosition.z;

                        float deltaAzimuth = Mathf.Atan(deltaX);
                        float deltaInclination = Mathf.Atan(deltaY);

                        OnTrackingUpdate(state, _lastPosition, deltaX, deltaY, deltaZ, deltaAzimuth, deltaInclination);
                    }
                }
            }
        }

        private void InteractionManager_SourcePressed(InteractionSourceState state)
        {
            if (state.source.kind != InteractionSourceKind.Hand)
            {
                return;
            }
            _trackedHands.Add(state.source.id);
            Vector3 pos;
            if (state.properties.location.TryGetPosition(out pos))
            {
                if (!IsTracking())
                {
                    _startPosition = pos;
                    _lastPosition = pos;
                    OnTrackingStart(state, _startPosition);                
                }
            }
        }

        private void InteractionManager_SourceReleased(InteractionSourceState state)
        {
            if (state.source.kind != InteractionSourceKind.Hand)
            {
                return;
            }

            if (_trackedHands.Contains(state.source.id))
            {
                _trackedHands.Remove(state.source.id);
            }

            _tracking = false;
            OnTrackingStop(state, _lastPosition);
        }

        public bool IsTracking()
        {
            return _tracking;
        }

        public void OnTrackingStart(InteractionSourceState state, Vector3 position)
        {
            _tracking = true;
        }

        public void OnTrackingUpdate(InteractionSourceState state, Vector3 position, float deltaX, float deltaY, float deltaZ, float deltaAzimut, float deltaInclination)
        {
            if (AdjustToggle.isAdjusting)
            {
                TargetObject.transform.position = TargetObject.transform.position + transform.right * deltaX * 0.1f + transform.up * deltaY * 0.1f + transform.forward * deltaZ * 0.1f;
            }
        }

        public void OnTrackingStop(InteractionSourceState state, Vector3 lastPosition)
        {
            // Do nothing..
        }

        void OnDestroy()
        {
            InteractionManager.SourcePressed -= InteractionManager_SourcePressed;
            InteractionManager.SourceReleased -= InteractionManager_SourceReleased;
            InteractionManager.SourceUpdated -= InteractionManager_SourceUpdated;
        }
    }
}