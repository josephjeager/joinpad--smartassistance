﻿using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class GazeGestureManager : MonoBehaviour
{
    public static GazeGestureManager Instance { get; private set; }

    // Represents the hologram that is currently being gazed at.
    //public GameObject FocusedObject { get; private set; }

    GestureRecognizer recognizer;
    
    // Use this for initialization
    void Start()
    {
        Instance = this;

        // Set up a GestureRecognizer to detect Select gestures.
        recognizer = new GestureRecognizer();
        recognizer.TappedEvent += (source, tapCount, ray) =>
        {
            //WebSocket.Instance.SetCoordinatesTest();
            
            if (SpatialMapping.Instance.DrawVisualMeshes)
            {
                SpatialMapping.Instance.DrawVisualMeshes = false;
            }
            else
            {
                SpatialMapping.Instance.DrawVisualMeshes = true;
            }
        };
        recognizer.StartCapturingGestures();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}