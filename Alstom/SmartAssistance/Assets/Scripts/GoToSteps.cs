﻿using UnityEngine;
using UnityEngine.SceneManagement;
using HoloToolkit.Unity.InputModule;

public class GoToSteps : MonoBehaviour, IInputClickHandler
{
    private void Start()
    {

    }

    public void OnInputClicked(InputEventData eventData)
    {
        SceneManager.LoadScene("Steps");
    }
}
