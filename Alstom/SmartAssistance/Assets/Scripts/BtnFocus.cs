﻿using UnityEngine;
using HoloToolkit.Unity.InputModule;
using System;

public class BtnFocus : MonoBehaviour, IFocusable
{
    private Material[] defaultMaterials;
    private float intensity = 30;

    private void Start()
    {
        defaultMaterials = gameObject.GetComponentInChildren<Renderer>().materials;
    }

    public void OnFocusEnter()
    {
        for (int i = 0; i < defaultMaterials.Length; i++)
        {
            float r = defaultMaterials[i].color.r + intensity / 100;
            float g = defaultMaterials[i].color.g + intensity / 100;
            float b = defaultMaterials[i].color.b + intensity / 100;
            defaultMaterials[i].color = new Color(r,g,b);
        }
    }

    public void OnFocusExit()
    {
        for (int i = 0; i < defaultMaterials.Length; i++)
        {
            float r = defaultMaterials[i].color.r - intensity / 100;
            float g = defaultMaterials[i].color.g - intensity / 100;
            float b = defaultMaterials[i].color.b - intensity / 100;
            defaultMaterials[i].color = new Color(r, g, b);
        }
    }
}
