﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class AdjustToggle : MonoBehaviour, IInputClickHandler
{
    static public bool isAdjusting;
    private Material[] btnColors;

    void Start()
    {
        isAdjusting = false;
        btnColors = gameObject.GetComponentInChildren<Renderer>().materials;
    }

    void Update()
    {

    }

    public void OnInputClicked(InputEventData eventData)
    {
        if (isAdjusting)
        {
            for (int i = 0; i < btnColors.Length; i++)
            {
                btnColors[i].color = new Color(0, 0, 0);
            }
            isAdjusting = false;
        }
        else
        {
            for (int i = 0; i < btnColors.Length; i++)
            {
                btnColors[i].color = new Color(0.6f, 0.6f, 0.6f);
            }
            isAdjusting = true;
        }
    }
}

