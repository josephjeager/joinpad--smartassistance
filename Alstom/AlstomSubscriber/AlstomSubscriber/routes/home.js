﻿var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    if (typeof loggedIn != 'undefined' && loggedIn)
        res.render('home', { title: 'Express' });
    else
        res.redirect('/');
});

router.get("/waitForCall", function (req, res) {
    var cont = 0;
    var interval = setInterval(function () {
        cont += 1;
        if (cont == 3) {
            clearInterval(interval);
            res.send("test");
        }
    }, 1000);
});

module.exports = router;