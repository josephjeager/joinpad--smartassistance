﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.UI;

public class ContactsNavigationScript : MonoBehaviour, INavigationHandler
{

    public ControlBusScript ControlBus;

    public void OnNavigationCanceled(NavigationEventData eventData)
    {
        ControlBus.HideNavigationCursor();
    }

    public void OnNavigationCompleted(NavigationEventData eventData)
    {
        ControlBus.HideNavigationCursor();
    }

    public void OnNavigationStarted(NavigationEventData eventData)
    {
        ControlBus.ShowNavigationCursor();
    }

    public void OnNavigationUpdated(NavigationEventData eventData)
    {
        if (eventData.NormalizedOffset.y >= 0 && eventData.NormalizedOffset.y <= 1)
            this.gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = 1 - eventData.NormalizedOffset.y;
    }

}
