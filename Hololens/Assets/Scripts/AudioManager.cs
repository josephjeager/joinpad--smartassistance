﻿using UnityEngine;

public enum Audio
{
    WELCOME,
    WELCOME_FIRST_ACCESS,
    CONNECTED,
    CALL_TERMINATED,
    INCOMING_CALL,
    OUTGOING_CALL
}

public class AudioManager : IAudioManager {

    /* Private Properties */

    private readonly GameObject _audioContainer;
    private GameObject AudioContainer
    {
        get
        {
            return _audioContainer;
        }
    }

    #region CHILDREN GAME OBJECTS

    private GameObject _incomingCallAudio;
    private GameObject IncomingCallAudio
    {
        get
        {
            return _incomingCallAudio ?? (_incomingCallAudio =
                       AudioContainer.transform.Find("IncomingCallAudio").gameObject);
        }
    }

    private GameObject _outgoingCallAudio;
    private GameObject OutgoingCallAudio
    {
        get
        {
            return _outgoingCallAudio ?? (_outgoingCallAudio =
                       AudioContainer.transform.Find("OutgoingCallAudio").gameObject);
        }
    }

    private GameObject _welcomeAudio;
    private GameObject WelcomeAudio
    {
        get
        {
            return _welcomeAudio ?? (_welcomeAudio = AudioContainer.transform.Find("WelcomeAudio").gameObject);
        }
    }

    private GameObject _welcomeFirstAccessAudio;
    private GameObject WelcomeFirstAccessAudio
    {
        get
        {
            return _welcomeFirstAccessAudio ?? (_welcomeFirstAccessAudio =
                       AudioContainer.transform.Find("WelcomeFirstAccessAudio").gameObject);
        }
    }

    private GameObject _callTerminatedAudio;
    private GameObject CallTerminatedAudio
    {
        get
        {
            return _callTerminatedAudio ?? (_callTerminatedAudio =
                       AudioContainer.transform.Find("CallTerminatedAudio").gameObject);
        }
    }

    private GameObject _connectedAudio;
    private GameObject ConnectedAudio
    {
        get
        {
            return _connectedAudio ?? (_connectedAudio = AudioContainer.transform.Find("ConnectedAudio").gameObject);
        }
    }

    /* Constructor */

    public AudioManager(GameObject audioContainer)
    {
        _audioContainer = audioContainer;
    }

    #endregion

    /* Public Methods */

    public void PlayAudio(Audio audio)
    {
        switch (audio)
        {
            case Audio.WELCOME:
                StopAudio(Audio.WELCOME_FIRST_ACCESS);
                StopAudio(Audio.CONNECTED);
                StopAudio(Audio.CALL_TERMINATED);
                StopAudio(Audio.INCOMING_CALL);
                StopAudio(Audio.OUTGOING_CALL);
                WelcomeAudio.GetComponent<AudioSource>().Play();
                break;
            case Audio.WELCOME_FIRST_ACCESS:
                StopAudio(Audio.WELCOME);
                StopAudio(Audio.CONNECTED);
                StopAudio(Audio.CALL_TERMINATED);
                StopAudio(Audio.INCOMING_CALL);
                StopAudio(Audio.OUTGOING_CALL);
                WelcomeFirstAccessAudio.GetComponent<AudioSource>().Play();
                break;
            case Audio.CONNECTED:
                StopAudio(Audio.WELCOME_FIRST_ACCESS);
                StopAudio(Audio.WELCOME);
                StopAudio(Audio.CALL_TERMINATED);
                StopAudio(Audio.INCOMING_CALL);
                StopAudio(Audio.OUTGOING_CALL);
                ConnectedAudio.GetComponent<AudioSource>().Play();
                break;
            case Audio.CALL_TERMINATED:
                StopAudio(Audio.WELCOME_FIRST_ACCESS);
                StopAudio(Audio.CONNECTED);
                StopAudio(Audio.WELCOME);
                StopAudio(Audio.INCOMING_CALL);
                StopAudio(Audio.OUTGOING_CALL);
                CallTerminatedAudio.GetComponent<AudioSource>().Play();
                break;
            case Audio.INCOMING_CALL:
                StopAudio(Audio.WELCOME_FIRST_ACCESS);
                StopAudio(Audio.CONNECTED);
                StopAudio(Audio.CALL_TERMINATED);
                StopAudio(Audio.WELCOME);
                StopAudio(Audio.OUTGOING_CALL);
                IncomingCallAudio.GetComponent<AudioSource>().Play();
                break;
            case Audio.OUTGOING_CALL:
                StopAudio(Audio.WELCOME_FIRST_ACCESS);
                StopAudio(Audio.CONNECTED);
                StopAudio(Audio.CALL_TERMINATED);
                StopAudio(Audio.INCOMING_CALL);
                StopAudio(Audio.WELCOME);
                OutgoingCallAudio.GetComponent<AudioSource>().Play();
                break;
        }
    }

    public void StopAudio(Audio audio)
    {
        switch (audio)
        {
            case Audio.WELCOME:
                WelcomeAudio.GetComponent<AudioSource>().Stop();
                break;
            case Audio.WELCOME_FIRST_ACCESS:
                WelcomeFirstAccessAudio.GetComponent<AudioSource>().Stop();
                break;
            case Audio.CONNECTED:
                ConnectedAudio.GetComponent<AudioSource>().Stop();
                break;
            case Audio.CALL_TERMINATED:
                CallTerminatedAudio.GetComponent<AudioSource>().Stop();
                break;
            case Audio.INCOMING_CALL:
                IncomingCallAudio.GetComponent<AudioSource>().Stop();
                break;
            case Audio.OUTGOING_CALL:
                OutgoingCallAudio.GetComponent<AudioSource>().Stop();
                break;
        }
    }


}
