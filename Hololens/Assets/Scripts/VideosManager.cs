﻿using HoloToolkit.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Persistence;

public class VideosManager : IVideosManager
{

    private Action<string> _onVideoLoaded;
    public Action<string> OnVideoLoaded
    {
        set { _onVideoLoaded = value; }
    }

    private Action<Guid, string> _onVideoCreated;
    public Action<Guid, string> OnVideoCreated
    {
        set { _onVideoCreated = value; }
    }

    private Action<string> _onVideoDestroyed;
    public Action<string> OnVideoDestroyed
    {
        set { _onVideoDestroyed = value; }
    }

    private readonly GameObject _videosContainer;
    private GameObject VideosContainer
    {
        get
        {
            return _videosContainer;
        }
    }

    private GameObject _video;
    private GameObject Video
    {
        get { return _video ?? (_video = VideosContainer.transform.Find("Video").gameObject); }
    }

    private readonly ISpatialManager _spatialManager;
    private readonly ISettings _settings;
    private readonly Dictionary<Guid, GameObject> _videos;
    private int _videosCounter;
    private GameObject _selectedVideo;
    private GameObject _catchedVideo;

    private VideosStore _videosStore;
    private readonly WorldAnchorStoreContainer _anchorStoreContainer;
    private WorldAnchorStore AnchorStore
    {
        get
        {
            return _anchorStoreContainer.AnchorStore;
        }
    }

    private float ArFovWidth = 915f;
    private float ArFovHeight = 515f;
    private float ArFovOffsetX = 210f;
    private float ArFovOffsetY = 85f;

    public VideosManager(GameObject videosContainer, ISpatialManager spatialManager, ISettings settings, WorldAnchorStoreContainer anchorStoreContainer)
    {
        _videosContainer = videosContainer;
        _spatialManager = spatialManager;
        _settings = settings;
        _anchorStoreContainer = anchorStoreContainer;
        _videos = new Dictionary<Guid, GameObject>();
        _videosCounter = 0;
        _videosStore = new VideosStore();
    }

    [Serializable]
    private class VideoIncomingCommand
    {
        public HoloCommand command;
        public int step;
        public string videoId;
        public string url;
        public string title;
        public ReferenceSystem referenceSystem;
    }

    [Serializable]
    private class VideoCallBackCommand
    {
        public HoloCommand command;
        public int index;
        public string id;
    }

    public void ReadCommand(string command)
    {
        bool needToSave = false;
        var videoMessage = JsonUtility.FromJson<VideoIncomingCommand>(command);
        var videoId = new Guid(videoMessage.videoId);
        switch (videoMessage.command)
        {
            case HoloCommand.CLEAR:
                ClearVideos();
                needToSave = true;
                break;
            case HoloCommand.COORDINATES:
                CreateVideo(videoMessage.url, videoMessage.title);
                needToSave = true;
                break;
            case HoloCommand.ACTIVE_DEACTIVE:
                RemoveVideo(videoId);
                needToSave = true;
                break;
            case HoloCommand.UP:
                SetVideoCoordinatesW(HoloCommand.UP, new MovementStep(videoMessage.step).GetEffectiveStep(), videoId);
                break;
            case HoloCommand.DOWN:
                SetVideoCoordinatesW(HoloCommand.DOWN, new MovementStep(videoMessage.step).GetEffectiveStep(), videoId);
                break;
            case HoloCommand.LEFT:
                SetVideoCoordinatesWR(HoloCommand.LEFT, new MovementStep(videoMessage.step).GetEffectiveStep(), videoId, videoMessage.referenceSystem);
                break;
            case HoloCommand.RIGHT:
                SetVideoCoordinatesWR(HoloCommand.RIGHT, new MovementStep(videoMessage.step).GetEffectiveStep(), videoId, videoMessage.referenceSystem);
                break;
            case HoloCommand.ZOOM_IN:
                SetVideoCoordinatesW(HoloCommand.ZOOM_IN, 0.05f, videoId);
                break;
            case HoloCommand.ZOOM_OUT:
                SetVideoCoordinatesW(HoloCommand.ZOOM_OUT, 0.05f, videoId);
                break;
            case HoloCommand.INCREASE_Z:
                SetVideoCoordinatesWR(HoloCommand.INCREASE_Z, new MovementStep(videoMessage.step).GetEffectiveStep(), videoId, videoMessage.referenceSystem);
                break;
            case HoloCommand.DECREASE_Z:
                SetVideoCoordinatesWR(HoloCommand.DECREASE_Z, new MovementStep(videoMessage.step).GetEffectiveStep(), videoId, videoMessage.referenceSystem);
                break;
            case HoloCommand.ROTATE_XUP:
                SetVideoCoordinatesW(HoloCommand.ROTATE_XUP, new RotationStep(videoMessage.step).GetEffectiveStep(), videoId);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                SetVideoCoordinatesW(HoloCommand.ROTATE_YRIGHT, new RotationStep(videoMessage.step).GetEffectiveStep(), videoId);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                SetVideoCoordinatesW(HoloCommand.ROTATE_ZRIGHT, new RotationStep(videoMessage.step).GetEffectiveStep(), videoId);
                break;
            case HoloCommand.ROTATE_XDOWN:
                SetVideoCoordinatesW(HoloCommand.ROTATE_XDOWN, new RotationStep(videoMessage.step).GetEffectiveStep(), videoId);
                break;
            case HoloCommand.ROTATE_YLEFT:
                SetVideoCoordinatesW(HoloCommand.ROTATE_YLEFT, new RotationStep(videoMessage.step).GetEffectiveStep(), videoId);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                SetVideoCoordinatesW(HoloCommand.ROTATE_ZLEFT, new RotationStep(videoMessage.step).GetEffectiveStep(), videoId);
                break;
            case HoloCommand.SELECTION:
                SelectVideo(videoId);
                break;
            case HoloCommand.UNSELECTION:
                UnselectVideo();
                break;
            case HoloCommand.EDIT:
                EditVideoTitle(videoId, videoMessage.title);
                needToSave = true;
                break;
        }
        if(needToSave) SaveVideos();
    }

    public void ClearVideosContainer()
    {
        ClearVideos();
    }

    public void LoadVideosContainer()
    {
        if (string.IsNullOrEmpty(_settings.VideosBackup)) return;
        _videosStore = JsonUtility.FromJson<VideosStore>(_settings.VideosBackup);
        _videosStore.videoRecords.ForEach(v =>
        {
            GameObject video;
            InstantiateVideo(out video, new Guid(v.id), v.url, v.title, vi => 
            {
                AnchorStore.Load(v.id.ToString(), vi);
                Component.Destroy(vi.GetComponent<WorldAnchor>());
            });
            _onVideoLoaded.Invoke(JsonUtility.ToJson(new VideoCallBackCommand()
            {
                command = HoloCommand.COORDINATES,
                index = _videosCounter,
                id = v.id,
            }));
            video.GetComponent<Billboard>().enabled = false;
            video.GetComponent<Tagalong>().enabled = false;
            video.transform.Find("Canvas/FooterPanel").gameObject.SetActive(false);
        });
    }

    public void HideVideosContainer()
    {
        VideosContainer.SetActive(false);
        ClearVideos();
    }

    public void ShowVideosContainer()
    {
        VideosContainer.SetActive(true);
    }

    public void CatchVideo(GameObject video)
    {
        _catchedVideo = video;
        _catchedVideo.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 2));
        _catchedVideo.GetComponent<Billboard>().enabled = true;
        _catchedVideo.GetComponent<Tagalong>().enabled = true;
        var fixedAngularSize = _catchedVideo.GetComponent<FixedAngularSize>();
        if (fixedAngularSize != null)
            fixedAngularSize.enabled = true;
        _catchedVideo.transform.Find("Canvas/FooterPanel").gameObject.SetActive(true);
    }

    public Guid PlaceCatchedVideo()
    {
        var videoId = _videos.FirstOrDefault(i => i.Value.Equals(_catchedVideo)).Key;
        _catchedVideo.GetComponent<Billboard>().enabled = false;
        _catchedVideo.GetComponent<Tagalong>().enabled = false;
        _catchedVideo.GetComponent<FixedAngularSize>().enabled = false;
        _catchedVideo.transform.Find("Canvas/FooterPanel").gameObject.SetActive(false);
        _catchedVideo = null;
        return videoId;
    }

    public void FixVideoPosition(Guid videoId)
    {
        RaycastHit hitInfo;
        var ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        if (!Physics.Raycast(ray, out hitInfo, 100, _spatialManager.GetLayerMask())) return;
        var video = _videos.FirstOrDefault(i => i.Key.Equals(videoId)).Value;
        if (video.transform.position.z > hitInfo.point.z)
        {
            video.transform.position = new Vector3(hitInfo.point.x, hitInfo.point.y, hitInfo.point.z - 0.1f);
            video.transform.rotation = Quaternion.identity;
        }
        UpdateVideo(videoId, video, null, null);
    }

    public void RemoveCatchedVideo()
    {
        if (_catchedVideo == null) return;
        var videoId = _videos.FirstOrDefault(i => i.Value.Equals(_catchedVideo)).Key;
        RemoveVideo(videoId);
        _catchedVideo = null;
        SaveVideos();
    }

    private void SelectVideo(Guid id)
    {
        if (_selectedVideo != null)
            ((Behaviour)_selectedVideo.transform.Find("Canvas/RawImage").GetComponent("Halo")).enabled = false;
        GameObject video;
        if (!_videos.TryGetValue(id, out video)) return;
        ((Behaviour)video.transform.Find("Canvas/RawImage").GetComponent("Halo")).enabled = true;
        _selectedVideo = video;
    }

    private void UnselectVideo()
    {
        if (_selectedVideo != null)
            ((Behaviour)_selectedVideo.transform.Find("Canvas/RawImage").GetComponent("Halo")).enabled = false;
        _selectedVideo = null;
    }

    private void EditVideoTitle(Guid id, string title)
    {
        GameObject video;
        if (!_videos.TryGetValue(id, out video)) return;
        video.transform.Find("Canvas/RawImage/Title").GetComponent<Text>().text = title;
        UpdateVideo(id, video, null, title);
    }

    private void UpdateVideo(Guid id, GameObject video, string url, string title)
    {
        if (video == null)
        {
            AnchorStore.Delete(id.ToString());
            _videosStore.videoRecords.Remove(_videosStore.videoRecords.FirstOrDefault(v => v.id == id.ToString()));
            return;
        }
        video.AddComponent<WorldAnchor>();
        AnchorStore.Delete(id.ToString());
        AnchorStore.Save(id.ToString(), video.GetComponent<WorldAnchor>());
        Component.Destroy(video.GetComponent<WorldAnchor>());
        var videoStored = _videosStore.videoRecords.FirstOrDefault(v => v.id == id.ToString());
        if (videoStored == null)
        {
            _videosStore.videoRecords.Add(new VideoRecord
            {
                id = id.ToString(),
                url = url,
                title = title
            });
            return;
        }
        if (title != null) videoStored.title = title;
    }

    private void SaveVideos()
    {
        _settings.VideosBackup = JsonUtility.ToJson(_videosStore);
        _settings.CallToResume = true;
        _settings.Save();
    }

    private void ClearVideos()
    {
        _videos.Keys.ToList().ForEach(RemoveVideo);
        _videosCounter = 0;
        _settings.VideosBackup = string.Empty;
        _settings.Save();
    }

    private void RemoveVideo(Guid id)
    {
        GameObject video;
        if (!_videos.TryGetValue(id, out video)) return;
        UpdateVideo(id, null, null, null);
        if (_selectedVideo!=null && _selectedVideo.Equals(video)) _selectedVideo = null;
        GameObject.Destroy(video);
        _videos.Remove(id);
        if(_onVideoDestroyed!=null) _onVideoDestroyed.Invoke(JsonUtility.ToJson(new VideoCallBackCommand()
        {
            command = HoloCommand.ACTIVE_DEACTIVE,
            id = id.ToString(),
        }));
    }

    private void CreateVideo(string url, string title)
    {
        RemoveCatchedVideo();
        GameObject video;
        var id = Guid.NewGuid();
        InstantiateVideo(out video, id, url, title,
            i => {
                i.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 2));
            });
        _catchedVideo = video;
        UpdateVideo(id, video, url, title);
        if(_onVideoCreated!=null) _onVideoCreated.Invoke(id, JsonUtility.ToJson(new VideoCallBackCommand()
        {
            command = HoloCommand.COORDINATES,
            index = _videosCounter,
            id = id.ToString(),
        }));
    }

    private void InstantiateVideo(out GameObject video, Guid id, string url, string title, Action<GameObject> setPositionAndRotation)
    {
        Debug.Log("NEW VIDEO:" + id);
        video = (GameObject)GameObject.Instantiate(Video, _videosContainer.transform);
        if (setPositionAndRotation != null) setPositionAndRotation.Invoke(video);
        _videosCounter++;
        video.transform.Find("Canvas/HeaderPanel/HeaderPanelContainer/HeaderText").gameObject.GetComponent<TextMesh>().text = _videosCounter.ToString();
        var rawImage = video.transform.Find("Canvas/RawImage");
        rawImage.transform.Find("Title").GetComponent<Text>().text = title;
        var videoPlayerAdapter = rawImage.GetComponent<VideoPlayerAdapterScript>();
        videoPlayerAdapter.VideoUrl = url;
        video.SetActive(true);
        _videos.Add(id, video);
    }

    private void SetVideoCoordinatesW(HoloCommand command, float step, Guid videoId)
    {
        float x, y, z;
        GameObject video;
        if (!_videos.TryGetValue(videoId, out video)) return;
        x = video.transform.localPosition.x;
        y = video.transform.localPosition.y;
        z = video.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.UP:
                video.transform.localPosition = new Vector3(x, y + step, z);
                break;
            case HoloCommand.DOWN:
                video.transform.localPosition = new Vector3(x, y - step, z);
                break;
            case HoloCommand.LEFT:
                video.transform.localPosition = new Vector3(x - step, y, z);
                break;
            case HoloCommand.RIGHT:
                video.transform.localPosition = new Vector3(x + step, y, z);
                break;
            case HoloCommand.ZOOM_IN:
                video.transform.localScale += new Vector3(step, step, step);
                break;
            case HoloCommand.ZOOM_OUT:
                video.transform.localScale -= new Vector3(step, step, step);
                break;
            case HoloCommand.INCREASE_Z:
                video.transform.localPosition = new Vector3(x, y, z + step);
                break;
            case HoloCommand.DECREASE_Z:
                video.transform.localPosition = new Vector3(x, y, z - step);
                break;
            case HoloCommand.ROTATE_XUP:
                video.transform.Rotate(step, 0, 0);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                video.transform.Rotate(0, step, 0);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                video.transform.Rotate(0, 0, step);
                break;
            case HoloCommand.ROTATE_XDOWN:
                video.transform.Rotate(-step, 0, 0);
                break;
            case HoloCommand.ROTATE_YLEFT:
                video.transform.Rotate(0, -step, 0);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                video.transform.Rotate(0, 0, -step);
                break;
        }
        UpdateVideo(videoId, video, null, null);
    }

    private void SetVideoCoordinatesWR(HoloCommand command, float step, Guid videoId, ReferenceSystem referenceSystem)
    {
        float x, y, z;
        GameObject video;
        if (!_videos.TryGetValue(videoId, out video)) return;
        x = video.transform.localPosition.x;
        y = video.transform.localPosition.y;
        z = video.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.LEFT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    video.transform.localPosition = new Vector3(x - step, y, z);
                else
                {
                    step = step * 10;
                    var dir = -Camera.main.transform.right;
                    video.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.RIGHT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    video.transform.localPosition = new Vector3(x + step, y, z);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.right;
                    video.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.INCREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    video.transform.localPosition = new Vector3(x, y, z + step);
                else
                {
                    step = step * 10;
                    var dir = -Camera.main.transform.forward;
                    video.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.DECREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    video.transform.localPosition = new Vector3(x, y, z - step);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.forward;
                    video.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
        }
        UpdateVideo(videoId, video, null, null);
    }

    public IEnumerator SetBlinkVideo(Guid videoId)
    {
        GameObject video;
        if (!_videos.TryGetValue(videoId, out video)) yield break;
        for (int i = 0; i < 3; i++)
        {
            if(video!=null) video.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            if (video!=null) video.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
        if (video!=null) video.SetActive(true);
    }

}
