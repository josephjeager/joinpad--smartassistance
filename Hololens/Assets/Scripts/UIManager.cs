﻿using HoloToolkit.UI.Keyboard;
using HoloToolkit.Unity;
using HoloToolkit.Unity.Buttons;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public enum Menu
{
    MAIN,
    SETTINGS,
    INCOMING_CALL,
    CALL,
    CONTACTS
}

public enum Box
{
    RESUME_CALL,
    LOOK_AROUND,
    CONTEXT_MENU
}

public enum Status
{
    CONNECTED,
    NOT_CONNECTED
}

public class UIManager : IUIManager {

    private Action _onOKDialogBoxClick;
    public Action OnOkDialogBoxClick
    {
        get
        {
            return _onOKDialogBoxClick;
        }
    }

    private Action _onCancelDialogBoxClick;
    public Action OnCancelDialogBoxClick
    {
        get
        {
            return _onCancelDialogBoxClick;
        }
    }

    /* Private Properties */

    private readonly GameObject _uiContainer;
    private GameObject UIContainer
    {
        get
        {
            return _uiContainer;
        }
    }

    private readonly ISettings _settings;

    #region CHILDREN GAME OBJECTS

    private GameObject _mainMenu;
    private GameObject MainMenu
    {
        get
        {
            return _mainMenu ?? (_mainMenu = UIContainer.transform.Find("MainMenu").gameObject);
        }
    }

    private GameObject _incomingCallMenu;
    private GameObject IncomingCallMenu
    {
        get
        {
            return _incomingCallMenu ?? (_incomingCallMenu = UIContainer.transform.Find("IncomingCallMenu").gameObject);
        }
    }

    private GameObject _incomingCallMenuCaller;
    private GameObject IncomingCallMenuCaller
    {
        get
        {
            return _incomingCallMenuCaller ?? (_incomingCallMenuCaller = IncomingCallMenu.transform.Find("Canvas/BodyPanel/Caller").gameObject);
        }
    }

    private GameObject _settingsMenu;
    private GameObject SettingsMenu
    {
        get
        {
            return _settingsMenu ?? (_settingsMenu = UIContainer.transform.Find("SettingsMenu").gameObject);
        }
    }

    private GameObject _contacsMenu;
    private GameObject ContactsMenu
    {
        get
        {
            return _contacsMenu ?? (_contacsMenu = UIContainer.transform.Find("ContactsMenu").gameObject);
        }
    }

    private GameObject _callMenu;
    private GameObject CallMenu
    {
        get
        {
            return _callMenu ?? (_callMenu = UIContainer.transform.Find("CallMenu").gameObject);
        }
    }

    private GameObject _dialogBox;
    private GameObject DialogBox
    {
        get
        {
            return _dialogBox ?? (_dialogBox = UIContainer.transform.Find("DialogBox").gameObject);
        }
    }

    private GameObject _resumeCallBox;
    private GameObject ResumeCallBox
    {
        get
        {
            return _resumeCallBox ?? (_resumeCallBox = UIContainer.transform.Find("ResumeCallBox").gameObject);
        }
    }

    private GameObject _lookAroundBox;
    private GameObject LookAroundBox
    {
        get
        {
            return _lookAroundBox ?? (_lookAroundBox = UIContainer.transform.Find("LookAroundBox").gameObject);
        }
    }

    private GameObject _contextMenuBox;
    private GameObject ContextMenuBox
    {
        get
        {
            return _contextMenuBox ?? (_contextMenuBox = UIContainer.transform.Find("ContextMenuBox").gameObject);
        }
    }

    private GameObject _clientNameInputField;
    private GameObject ClientNameInputField
    {
        get
        {
            return _clientNameInputField ?? (_clientNameInputField =
                       SettingsMenu.transform.Find("Canvas/BodyPanel/ClientNameInputField").gameObject);
        }
    }

    private GameObject _serverAddressInputField;
    private GameObject ServerAddressInputField
    {
        get
        {
            return _serverAddressInputField ?? (_serverAddressInputField =
                       SettingsMenu.transform.Find("Canvas/BodyPanel/ServerAddressInputField").gameObject);
        }
    }

    private GameObject _portInputField;
    private GameObject PortInputField
    {
        get
        {
            return _portInputField ?? (_portInputField =
                       SettingsMenu.transform.Find("Canvas/BodyPanel/PortInputField").gameObject);
        }
    }

    private GameObject _dialogBoxMessagesContainer;
    private GameObject DialogBoxMessagesContainer
    {
        get
        {
            return _dialogBoxMessagesContainer ?? (_dialogBoxMessagesContainer =
                       DialogBox.transform.Find("Canvas/BodyPanel/Messages").gameObject);
        }
    }

    private GameObject _callContactButton;
    private GameObject CallContactButton
    {
        get
        {
            return _callContactButton ?? (_callContactButton =
                       ContactsMenu.transform.Find("Canvas/BodyPanel/CallContactButton").gameObject);
        }
    }

    private GameObject _muteutton;
    private GameObject MuteButton
    {
        get
        {
            return _muteutton ?? (_muteutton = CallMenu.transform.Find("Canvas/BodyPanel/MuteButton").gameObject);
        }
    }

    private GameObject _mainMenuFooterText;
    private GameObject MainMenuFooterText
    {
        get
        {
            return _mainMenuFooterText ?? (_mainMenuFooterText =
                       MainMenu.transform.Find("Canvas/FooterPanel/FooterPanelContainer/FooterText").gameObject);
        }
    }

    private GameObject _mainMenuFooterIcon;
    private GameObject MainMenuFooterIcon
    {
        get
        {
            return _mainMenuFooterIcon ?? (_mainMenuFooterIcon =
                       MainMenu.transform.Find("Canvas/FooterPanel/FooterPanelContainer/FooterIcon").gameObject);
        }
    }

    private GameObject _incomingCallMenuHeaderPanelContainer;
    private GameObject IncomingCallMenuHeaderPanelContainer
    {
        get
        {
            return _incomingCallMenuHeaderPanelContainer ?? (_incomingCallMenuHeaderPanelContainer = IncomingCallMenu
                       .transform.Find("Canvas/HeaderPanel/HeaderPanelContainer").gameObject);
        }
    }

    private GameObject _resumeCallBoxHeaderText;
    private GameObject ResumeCallBoxHeaderText
    {
        get
        {
            return _resumeCallBoxHeaderText ?? (_resumeCallBoxHeaderText =
                       ResumeCallBox.transform.Find("Canvas/HeaderPanel/HeaderPanelContainer/HeaderText").gameObject);
        }
    }

    private GameObject _contactsMenuContent;
    private GameObject ContactsMenuContent
    {
        get
        {
            return _contactsMenuContent ?? (_contactsMenuContent =
                       ContactsMenu.transform.Find("Canvas/BodyPanel/ScrollView/Viewport/Content").gameObject);
        }
    }

    private GameObject _contact;
    private GameObject Contact
    {
        get
        {
            return _contact ?? (_contact = ContactsMenuContent.transform.Find("Contact").gameObject);
        }
    }

    private Dictionary<int, GameObject> _contacts;
    private GameObject _selectedContact;
    private int _contactsCounter;

    #endregion

    /* Constructor */

    public UIManager(GameObject uiContainer, ISettings settings)
    {
        _uiContainer = uiContainer;
        _settings = settings;
        _contacts = new Dictionary<int, GameObject>();
        _selectedContact = null;
        _contactsCounter = 0;
    }

    /* Public Methods */

    public void ShowMenu(Menu menu)
    {
        var position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 2));
        switch (menu)
        {
            case Menu.MAIN:
                MainMenu.transform.position = position;
                MainMenu.SetActive(true);
                SettingsMenu.SetActive(false);
                IncomingCallMenu.SetActive(false);
                CallMenu.SetActive(false);
                DialogBox.SetActive(false);
                ResumeCallBox.SetActive(false);
                LookAroundBox.SetActive(false);
                ContextMenuBox.SetActive(false);
                ContactsMenu.SetActive(false);
                break;
            case Menu.SETTINGS:
                SettingsMenu.transform.position = position;
                MainMenu.SetActive(false);
                SettingsMenu.SetActive(true);
                IncomingCallMenu.SetActive(false);
                CallMenu.SetActive(false);
                DialogBox.SetActive(false);
                ResumeCallBox.SetActive(false);
                LookAroundBox.SetActive(false);
                ContextMenuBox.SetActive(false);
                ContactsMenu.SetActive(false);
                break;
            case Menu.INCOMING_CALL:
                IncomingCallMenu.transform.position = position;
                MainMenu.SetActive(false);
                SettingsMenu.SetActive(false);
                IncomingCallMenu.SetActive(true);
                CallMenu.SetActive(false);
                DialogBox.SetActive(false);
                ResumeCallBox.SetActive(false);
                LookAroundBox.SetActive(false);
                ContextMenuBox.SetActive(false);
                ContactsMenu.SetActive(false);
                break;
            case Menu.CALL:
                CallMenu.transform.position = position;
                MainMenu.SetActive(false);
                SettingsMenu.SetActive(false);
                IncomingCallMenu.SetActive(false);
                CallMenu.SetActive(true);
                DialogBox.SetActive(false);
                ResumeCallBox.SetActive(false);
                LookAroundBox.SetActive(false);
                ContextMenuBox.SetActive(false);
                ContactsMenu.SetActive(false);
                break;
            case Menu.CONTACTS:
                ContactsMenu.transform.position = position;
                MainMenu.SetActive(false);
                SettingsMenu.SetActive(false);
                IncomingCallMenu.SetActive(false);
                CallMenu.SetActive(false);
                DialogBox.SetActive(false);
                ResumeCallBox.SetActive(false);
                LookAroundBox.SetActive(false);
                ContextMenuBox.SetActive(false);
                ContactsMenu.SetActive(true);
                break;
        }
    }

    public void HideMenu(Menu menu)
    {
        switch (menu)
        {
            case Menu.MAIN:
                MainMenu.SetActive(false);
                break;
            case Menu.SETTINGS:
                SettingsMenu.SetActive(false);
                break;
            case Menu.INCOMING_CALL:
                IncomingCallMenu.SetActive(false);
                break;
            case Menu.CALL:
                CallMenu.SetActive(false);
                break;
            case Menu.CONTACTS:
                ContactsMenu.SetActive(false);
                break;
        }
    }

    public void ShowBox(Box box)
    {
        var position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 2));
        switch (box)
        {
            case Box.LOOK_AROUND:
                LookAroundBox.transform.position = position;
                MainMenu.SetActive(false);
                SettingsMenu.SetActive(false);
                IncomingCallMenu.SetActive(false);
                CallMenu.SetActive(false);
                DialogBox.SetActive(false);
                ResumeCallBox.SetActive(false);
                LookAroundBox.SetActive(true);
                ContextMenuBox.SetActive(false);
                ContactsMenu.SetActive(false);
                break;
            case Box.RESUME_CALL:
                ResumeCallBox.transform.position = position;
                MainMenu.SetActive(false);
                SettingsMenu.SetActive(false);
                IncomingCallMenu.SetActive(false);
                CallMenu.SetActive(false);
                DialogBox.SetActive(false);
                LookAroundBox.SetActive(false);
                ResumeCallBox.SetActive(true);
                ContextMenuBox.SetActive(false);
                ContactsMenu.SetActive(false);
                break;
            case Box.CONTEXT_MENU:
                ContextMenuBox.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height, 2));
                MainMenu.SetActive(false);
                SettingsMenu.SetActive(false);
                IncomingCallMenu.SetActive(false);
                CallMenu.SetActive(false);
                DialogBox.SetActive(false);
                LookAroundBox.SetActive(false);
                ResumeCallBox.SetActive(false);
                ContextMenuBox.SetActive(true);
                ContactsMenu.SetActive(false);
                break;
        }
    }

    public void HideBox(Box box)
    {
        switch (box)
        {
            case Box.LOOK_AROUND:
                LookAroundBox.SetActive(false);
                break;
            case Box.RESUME_CALL:
                ResumeCallBox.SetActive(false);
                break;
            case Box.CONTEXT_MENU:
                ContextMenuBox.SetActive(false);
                break;
        }
    }

    public void ShowDialogBox(string firstMessage, string secondMessage, string thirdMessage, Action onOKDialogBoxClick, Action onCancelDialogBoxClick)
    {
        DialogBoxMessagesContainer.transform.Find("FirstMessageText").gameObject.GetComponent<TextMesh>().text = firstMessage;
        DialogBoxMessagesContainer.transform.Find("SecondMessageText").gameObject.GetComponent<TextMesh>().text = secondMessage;
        DialogBoxMessagesContainer.transform.Find("ThirdMessageText").gameObject.GetComponent<TextMesh>().text = thirdMessage;
        DialogBox.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 2));
        _onOKDialogBoxClick = onOKDialogBoxClick;
        _onCancelDialogBoxClick = onCancelDialogBoxClick;
        MainMenu.SetActive(false);
        SettingsMenu.SetActive(false);
        CallMenu.SetActive(false);
        IncomingCallMenu.SetActive(false);
        LookAroundBox.SetActive(false);
        ResumeCallBox.SetActive(false);
        ContextMenuBox.SetActive(false);
        ContactsMenu.SetActive(false);
        DialogBox.SetActive(true);
    }

    public void LockMenu(Menu menu)
    {
        switch (menu)
        {
            case Menu.MAIN:
                MainMenu.GetComponent<Billboard>().enabled = false;
                MainMenu.GetComponent<Tagalong>().enabled = false;
                MainMenu.GetComponent<FixedAngularSize>().enabled = false;
                break;
            case Menu.SETTINGS:
                SettingsMenu.GetComponent<Billboard>().enabled = false;
                SettingsMenu.GetComponent<Tagalong>().enabled = false;
                SettingsMenu.GetComponent<FixedAngularSize>().enabled = false;
                break;
            case Menu.INCOMING_CALL:
                IncomingCallMenu.GetComponent<Billboard>().enabled = false;
                IncomingCallMenu.GetComponent<Tagalong>().enabled = false;
                IncomingCallMenu.GetComponent<FixedAngularSize>().enabled = false;
                break;
            case Menu.CALL:
                CallMenu.GetComponent<Billboard>().enabled = false;
                CallMenu.GetComponent<Tagalong>().enabled = false;
                CallMenu.GetComponent<FixedAngularSize>().enabled = false;
                break;
            case Menu.CONTACTS:
                ContactsMenu.GetComponent<Billboard>().enabled = false;
                ContactsMenu.GetComponent<Tagalong>().enabled = false;
                ContactsMenu.GetComponent<FixedAngularSize>().enabled = false;
                break;
        }
    }

    public void UnlockMenu(Menu menu)
    {
        switch (menu)
        {
            case Menu.MAIN:
                MainMenu.GetComponent<Billboard>().enabled = true;
                MainMenu.GetComponent<Tagalong>().enabled = true;
                MainMenu.GetComponent<FixedAngularSize>().enabled = true;
                break;
            case Menu.SETTINGS:
                SettingsMenu.GetComponent<Billboard>().enabled = true;
                SettingsMenu.GetComponent<Tagalong>().enabled = true;
                SettingsMenu.GetComponent<FixedAngularSize>().enabled = true;
                break;
            case Menu.INCOMING_CALL:
                IncomingCallMenu.GetComponent<Billboard>().enabled = true;
                IncomingCallMenu.GetComponent<Tagalong>().enabled = true;
                IncomingCallMenu.GetComponent<FixedAngularSize>().enabled = true;
                break;
            case Menu.CALL:
                CallMenu.GetComponent<Billboard>().enabled = true;
                CallMenu.GetComponent<Tagalong>().enabled = true;
                CallMenu.GetComponent<FixedAngularSize>().enabled = true;
                break;
            case Menu.CONTACTS:
                ContactsMenu.GetComponent<Billboard>().enabled = true;
                ContactsMenu.GetComponent<Tagalong>().enabled = true;
                ContactsMenu.GetComponent<FixedAngularSize>().enabled = true;
                break;
        }
    }

    public bool CanStoreSettings()
    {
        var clientName = ClientNameInputField.GetComponent<KeyboardInputField>().text;
        var serverAddress = ServerAddressInputField.GetComponent<KeyboardInputField>().text;
        var serverPort = PortInputField.GetComponent<KeyboardInputField>().text;
        return !string.IsNullOrEmpty(clientName) && !string.IsNullOrEmpty(serverAddress) && !string.IsNullOrEmpty(serverPort);
    }

    public void ReadAndSaveSettings()
    {
        var clientName = ClientNameInputField.GetComponent<KeyboardInputField>().text;
        var serverAddress = ServerAddressInputField.GetComponent<KeyboardInputField>().text;
        var serverPort = PortInputField.GetComponent<KeyboardInputField>().text;
        _settings.ClientName = clientName;
        _settings.ServerAddress = serverAddress;
        _settings.ServerPort = serverPort;
        _settings.Initialized = true;
        _settings.Save();
    }

    public void LoadAndFillSettings()
    {
        ClientNameInputField.GetComponent<KeyboardInputField>().text = _settings.ClientName;
        ServerAddressInputField.GetComponent<KeyboardInputField>().text = _settings.ServerAddress;
        PortInputField.GetComponent<KeyboardInputField>().text = _settings.ServerPort;
    }

    public void RestoreCallContactButton()
    {
        CallContactButton.GetComponentInChildren<TextMesh>().text = "NEW CALL";
    }

    public void UpdateMenuFooterStatus(Status status)
    {
        switch (status)
        {
            case Status.CONNECTED:
                MainMenuFooterText.GetComponent<TextMesh>().color = Color.green;
                MainMenuFooterIcon.GetComponent<RawImage>().color = Color.green;
                MainMenuFooterText.GetComponent<TextMesh>().text = "CONNECTED";
                MainMenuFooterIcon.GetComponent<RawImage>().texture = Resources.Load("UI_Icon_Wifi") as Texture;
                break;
            case Status.NOT_CONNECTED:
                MainMenuFooterText.GetComponent<TextMesh>().color = Color.red;
                MainMenuFooterIcon.GetComponent<RawImage>().color = Color.red;
                MainMenuFooterText.GetComponent<TextMesh>().text = "NOT CONNECTED";
                MainMenuFooterIcon.GetComponent<RawImage>().texture = Resources.Load("UI_Icon_WifiProblem") as Texture;
                break;
        }
    }

    public void ShowMuteButtonState()
    {
        MuteButton.GetComponentInChildren<TextMesh>().text = "MUTE";
        MuteButton.GetComponentInChildren<CompoundButtonIcon>().IconOverride = Resources.Load("UI_Icon_MicOn") as Texture2D;
        MuteButton.GetComponentInChildren<CompoundButtonIcon>().IconName = "UI_Icon_MicOn";
    }

    public void ShowUnmuteButtonState()
    {
        MuteButton.GetComponentInChildren<TextMesh>().text = "UNMUTE";
        MuteButton.GetComponentInChildren<CompoundButtonIcon>().IconOverride = Resources.Load("UI_Icon_MicOff") as Texture2D;
        MuteButton.GetComponentInChildren<CompoundButtonIcon>().IconName = "UI_Icon_MicOff";
    }

    public IEnumerator RemoveContact(int id)
    {
        if (!_contacts.ContainsKey(id)) yield break;
        var contactObj = _contacts[id];
        if (_selectedContact!=null && _selectedContact.Equals(contactObj))
            UnSelectContact();
        //_contactsCounter--; 
        GameObject.Destroy(contactObj);
        _contacts.Remove(id);
        yield return new WaitForSeconds(0.5f);
    }

    public IEnumerator AddContacts(List<Contact> contacts)
    {
        yield return ClearContacts();
        foreach (var contact in contacts)
            yield return AddContact(contact);
    }

    private IEnumerator ClearContacts()
    {
        foreach(var contact in _contacts.ToList())
            yield return RemoveContact(contact.Key);
        var contentRectTransform = ContactsMenuContent.GetComponent<RectTransform>();
        contentRectTransform.sizeDelta = new Vector2(contentRectTransform.sizeDelta.x, 0);
        _contactsCounter = 0;
    }

    public IEnumerator AddContact(Contact contact)
    {
        if (_contacts.ContainsKey(contact.Id)) yield break;
        var contentRectTransform = ContactsMenuContent.GetComponent<RectTransform>();
        contentRectTransform.sizeDelta = new Vector2(contentRectTransform.sizeDelta.x, contentRectTransform.sizeDelta.y + 10f);
        var contactObj = (GameObject)GameObject.Instantiate(Contact, ContactsMenuContent.transform);
        var contactRect = contactObj.GetComponent<RectTransform>();
        contactRect.localPosition = new Vector3(contactRect.localPosition.x, contactRect.localPosition.y + (-9.7f * _contactsCounter), contactRect.localPosition.z);
        _contactsCounter++;
        contactObj.GetComponentInChildren<Text>().text = contact.Username.ToUpper();
        contactObj.GetComponent<ContactClickScript>().ContactId = contact.Id;
        contactObj.SetActive(true);
        _contacts.Add(contact.Id, contactObj);
        yield return new WaitForSeconds(0.5f);
    }

    public void SelectContact(int id)
    {
        if(_selectedContact!=null)
        {
            _selectedContact.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0.392f);
            if (_selectedContact.Equals(_contacts[id]))
            {
                _selectedContact = null;
                return;
            }
        }      
        _selectedContact = _contacts[id];
        _selectedContact.GetComponent<Image>().color = Color.blue;
    }

    public void UnSelectContact()
    {
        if (_selectedContact != null)
            _selectedContact.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0.392f);
        _selectedContact = null;
    }

    public void SetCaller(string caller)
    {
        IncomingCallMenuCaller.GetComponent<TextMesh>().text = caller;
    }

    public IEnumerator MainMenuConnectCoroutine()
    {
        MainMenuFooterText.GetComponent<TextMesh>().color = Color.white;
        MainMenuFooterIcon.GetComponent<RawImage>().color = Color.white;
        MainMenuFooterIcon.GetComponent<RawImage>().texture = Resources.Load("UI_Icon_Swap") as Texture;
        return SetDynamicTextMesh(MainMenuFooterText.GetComponent<TextMesh>(), "CONNECTING", 0.5F);
    }

    public IEnumerator ResumeCallCoroutine()
    {
        return SetDynamicTextMesh(ResumeCallBoxHeaderText.GetComponent<TextMesh>(), "RESUMING CALL", 0.5F);
    }

    public IEnumerator OutgoingCallCoroutine()
    {
        return SetDynamicTextMesh(CallContactButton.GetComponentInChildren<TextMesh>(), "CALLING", 0.5F);
    }

    public IEnumerator IncomingCallCoroutine()
    {
        return SetBlinkGameObject(IncomingCallMenuHeaderPanelContainer, 0.5F, 0.5F);
    }

    public IEnumerator HideLookAroundBoxCoroutine(float seconds, Action endCallback)
    {
        yield return new WaitForSeconds(seconds);
        LookAroundBox.SetActive(false);
        endCallback.Invoke();
    }

    public IEnumerator WaitCallMenuCoroutine(float seconds, Action callBack)
    {
        ShowBox(Box.CONTEXT_MENU);
        yield return new WaitForSeconds(seconds);
        ShowMenu(Menu.CALL);
        callBack();
    }

    /* Private Methods */

    private IEnumerator SetDynamicTextMesh(TextMesh textMesh, string text, float sleep)
    {
        while (true)
        {
            textMesh.text = text;
            yield return new WaitForSeconds(sleep);
            textMesh.text = text + ".";
            yield return new WaitForSeconds(sleep);
            textMesh.text = text + "..";
            yield return new WaitForSeconds(sleep);
            textMesh.text = text + "...";
            yield return new WaitForSeconds(sleep);
        }
    }

    private IEnumerator SetBlinkGameObject(GameObject gameObject, float activeSleep, float inactiveSleep)
    {
        while (true)
        {
            gameObject.SetActive(true);
            yield return new WaitForSeconds(activeSleep);
            gameObject.SetActive(false);
            yield return new WaitForSeconds(inactiveSleep);
        }
    }
}
