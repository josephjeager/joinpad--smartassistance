﻿using HoloToolkit.Unity.SpatialMapping;
using System.Collections;
using UnityEngine;

public class SpatialManager : ISpatialManager {

    /* Private Properties */

    private readonly GameObject _spatialMapping;
    private GameObject SpatialMapping
    {
        get
        {
            return _spatialMapping;
        }
    }
  
    /* Constructor */

    public SpatialManager(GameObject spatialMapping)
    {
        _spatialMapping = spatialMapping;
    }

    /* Public Methods */

    public void EnableSpatialMapping()
    {
        SpatialMapping.SetActive(true);
    }

    public void DisableSpatialMapping()
    {
        SpatialMapping.SetActive(false);
    }

    public int GetLayerMask()
    {
        return SpatialMapping.GetComponent<SpatialMappingManager>().LayerMask;
    }

    public void ShowVisualMeshes()
    {
        if (!SpatialMapping.activeSelf) return;
        var spm = SpatialMapping.GetComponent<SpatialMappingManager>();
        spm.SetSurfaceMaterial(spm.VisualMaterial);
    }

    public IEnumerator HideVisualMeshesCoroutine(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        var spm = SpatialMapping.GetComponent<SpatialMappingManager>();
        spm.SetSurfaceMaterial(spm.OcclusionMaterial);
    }

}
