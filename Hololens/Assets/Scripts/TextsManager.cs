﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Persistence;

public class TextsManager : ITextsManager
{

    private Action<string> _onTextLoaded;
    public Action<string> OnTextLoaded
    {
        set { _onTextLoaded = value; }
    }

    private Action<Guid, string> _onTextCreated;
    public Action<Guid, string> OnTextCreated
    {
        set { _onTextCreated = value; }
    }

    private Action<string> _onTextDestroyed;
    public Action<string> OnTextDestroyed
    {
        set { _onTextDestroyed = value; }
    }

    private readonly GameObject _textsContainer;
    private GameObject TextsContainer
    {
        get
        {
            return _textsContainer;
        }
    }

    private GameObject _text;
    private GameObject Text
    {
        get
        {
            return _text ?? (_text = TextsContainer.transform.Find("Text").gameObject);
        }
    }


    private readonly ISpatialManager _spatialManager;
    private readonly ISettings _settings;
    private readonly Dictionary<Guid, GameObject> _texts;
    private int _textsCounter;
    private GameObject _selectedText;

    private TextsStore _textsStore;
    private readonly WorldAnchorStoreContainer _anchorStoreContainer;
    private WorldAnchorStore AnchorStore
    {
        get
        {
            return _anchorStoreContainer.AnchorStore;
        }
    }

    private float ArFovWidth = 915f;
    private float ArFovHeight = 515f;
    private float ArFovOffsetX = 210f;
    private float ArFovOffsetY = 85f;

    public TextsManager(GameObject textsContainer, ISpatialManager spatialManager, ISettings settings, WorldAnchorStoreContainer anchorStoreContainer)
    {
        _textsContainer = textsContainer;
        _spatialManager = spatialManager;
        _settings = settings;
        _anchorStoreContainer = anchorStoreContainer;
        _texts = new Dictionary<Guid, GameObject>();
        _textsCounter = 0;
        _textsStore = new TextsStore();
    }

    [Serializable]
    private class TextIncomingCommand
    {
        public HoloCommand command;
        public float x;
        public float y;
        public int step;
        public string textId;
        public string text;
        public Colors.Name colorName;
        public ReferenceSystem referenceSystem;
    }

    [Serializable]
    private class TextCallBackCommand
    {
        public HoloCommand command;
        public int index;
        public string id;
        public string text;
        public Colors.Name colorName;
    }

    public void ReadCommand(string command)
    {
        bool needToSave = false;
        var textMessage = JsonUtility.FromJson<TextIncomingCommand>(command);
        var textId = new Guid(textMessage.textId);
        switch (textMessage.command)
        {
            case HoloCommand.CLEAR:
                ClearTexts();
                needToSave = true;
                break;
            case HoloCommand.COORDINATES:
                CreateText(textMessage.x, textMessage.y, textMessage.text, textMessage.colorName);
                needToSave = true;
                break;
            case HoloCommand.ACTIVE_DEACTIVE:
                RemoveText(textId);
                needToSave = true;
                break;
            case HoloCommand.EDIT:
                EditText(textId, textMessage.text);
                needToSave = true;
                break;
            case HoloCommand.SET_WHITE:
                SetTextColor(textId, Colors.Name.WHITE);
                needToSave = true;
                break;
            case HoloCommand.SET_RED:
                SetTextColor(textId, Colors.Name.RED);
                needToSave = true;
                break;
            case HoloCommand.SET_YELLOW:
                SetTextColor(textId, Colors.Name.YELLOW);
                needToSave = true;
                break;
            case HoloCommand.SET_GREEN:
                SetTextColor(textId, Colors.Name.GREEN);
                needToSave = true;
                break;
            case HoloCommand.SET_BLUE:
                SetTextColor(textId, Colors.Name.BLUE);
                needToSave = true;
                break;
            case HoloCommand.UP:
                SetTextCoordinatesW(HoloCommand.UP, new MovementStep(textMessage.step).GetEffectiveStep(), textId);
                break;
            case HoloCommand.DOWN:
                SetTextCoordinatesW(HoloCommand.DOWN, new MovementStep(textMessage.step).GetEffectiveStep(), textId);
                break;
            case HoloCommand.LEFT:
                SetTextCoordinatesWR(HoloCommand.LEFT, new MovementStep(textMessage.step).GetEffectiveStep(), textId, textMessage.referenceSystem);
                break;
            case HoloCommand.RIGHT:
                SetTextCoordinatesWR(HoloCommand.RIGHT, new MovementStep(textMessage.step).GetEffectiveStep(), textId, textMessage.referenceSystem);
                break;
            case HoloCommand.ZOOM_IN:
                SetTextCoordinatesW(HoloCommand.ZOOM_IN, 0.05f, textId);
                break;
            case HoloCommand.ZOOM_OUT:
                SetTextCoordinatesW(HoloCommand.ZOOM_OUT, 0.05f, textId);
                break;
            case HoloCommand.INCREASE_Z:
                SetTextCoordinatesWR(HoloCommand.INCREASE_Z, new MovementStep(textMessage.step).GetEffectiveStep(), textId, textMessage.referenceSystem);
                break;
            case HoloCommand.DECREASE_Z:
                SetTextCoordinatesWR(HoloCommand.DECREASE_Z, new MovementStep(textMessage.step).GetEffectiveStep(), textId, textMessage.referenceSystem);
                break;
            case HoloCommand.ROTATE_XUP:
                SetTextCoordinatesW(HoloCommand.ROTATE_XUP, new RotationStep(textMessage.step).GetEffectiveStep(), textId);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                SetTextCoordinatesW(HoloCommand.ROTATE_YRIGHT, new RotationStep(textMessage.step).GetEffectiveStep(), textId);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                SetTextCoordinatesW(HoloCommand.ROTATE_ZRIGHT, new RotationStep(textMessage.step).GetEffectiveStep(), textId);
                break;
            case HoloCommand.ROTATE_XDOWN:
                SetTextCoordinatesW(HoloCommand.ROTATE_XDOWN, new RotationStep(textMessage.step).GetEffectiveStep(), textId);
                break;
            case HoloCommand.ROTATE_YLEFT:
                SetTextCoordinatesW(HoloCommand.ROTATE_YLEFT, new RotationStep(textMessage.step).GetEffectiveStep(), textId);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                SetTextCoordinatesW(HoloCommand.ROTATE_ZLEFT, new RotationStep(textMessage.step).GetEffectiveStep(), textId);
                break;
            case HoloCommand.SELECTION:
                SelectText(textId);
                break;
            case HoloCommand.UNSELECTION:
                UnselectText();
                break;
        }
        if(needToSave) SaveTexts();
    }

    public void ClearTextsContainer()
    {
        ClearTexts();
    }

    public void LoadTextsContainer()
    {
        Debug.Log(_settings.TextsBackup);
        if (string.IsNullOrEmpty(_settings.TextsBackup)) return;
        _textsStore = JsonUtility.FromJson<TextsStore>(_settings.TextsBackup);
        _textsStore.textRecords.ForEach(t =>
        {
            GameObject text;
            InstantiateText(out text, new Guid(t.id), t.text, t.colorName, te =>
            {
                AnchorStore.Load(t.id.ToString(), te);
                Component.Destroy(te.GetComponent<WorldAnchor>());
            });
            _onTextLoaded.Invoke(JsonUtility.ToJson(new TextCallBackCommand()
            {
                command = HoloCommand.COORDINATES,
                index = _textsCounter,
                id = t.id,
                text = t.text,
                colorName = t.colorName
            }));
        });
    }

    public void HideTextsContainer()
    {
        TextsContainer.SetActive(false);
        ClearTexts();
    }

    public void ShowTextsContainer()
    {
        TextsContainer.SetActive(true);
    }

    public void RemoveText(GameObject textObj)
    {
        var id = _texts.FirstOrDefault(t => t.Value.Equals(textObj)).Key;
        RemoveText(id);
        SaveTexts();
    }

    private void EditText(Guid id, string text)
    {
        GameObject textObj;
        if (!_texts.TryGetValue(id, out textObj)) return;
        textObj.transform.Find("Text").gameObject.GetComponent<TextMesh>().text = text;
        UpdateText(id, textObj, text, null);
    }

    private void SelectText(Guid id)
    {
        if (_selectedText != null)
            ((Behaviour)_selectedText.transform.Find("Text").GetComponent("Halo")).enabled = false;
        GameObject textObj;
        if (!_texts.TryGetValue(id, out textObj)) return;
        ((Behaviour)textObj.transform.Find("Text").GetComponent("Halo")).enabled = true;
        _selectedText = textObj;
    }

    private void UnselectText()
    {
        if (_selectedText != null)
            ((Behaviour)_selectedText.transform.Find("Text").GetComponent("Halo")).enabled = false;
        _selectedText = null;
    }

    private void UpdateText(Guid id, GameObject textObj, string text, Colors.Name? colorName)
    {
        if (textObj == null)
        {
            AnchorStore.Delete(id.ToString());
            _textsStore.textRecords.Remove(_textsStore.textRecords.FirstOrDefault(t => t.id == id.ToString()));
            return;
        }
        textObj.AddComponent<WorldAnchor>();
        AnchorStore.Delete(id.ToString());
        AnchorStore.Save(id.ToString(), textObj.GetComponent<WorldAnchor>());
        Component.Destroy(textObj.GetComponent<WorldAnchor>());
        var textStored = _textsStore.textRecords.FirstOrDefault(w => w.id == id.ToString());
        if (textStored == null)
        {
            _textsStore.textRecords.Add(new TextRecord
            {
                id = id.ToString(),
                text = text,
                colorName = colorName.Value
            });
            return;
        }
        if (text != null) textStored.text = text;
        if (colorName != null) textStored.colorName = colorName.Value;
    }

    private void SaveTexts()
    {
        _settings.TextsBackup = JsonUtility.ToJson(_textsStore);
        _settings.CallToResume = true;
        _settings.Save();
    }

    private void ClearTexts()
    {
        _texts.Keys.ToList().ForEach(RemoveText);
        _textsCounter = 0;
        _settings.TextsBackup = string.Empty;
        _settings.Save();
    }

    private void CreateText(float x, float y, string text, Colors.Name colorName)
    {
        x = NormalizeCoordX(x);
        y = NormalizeCoordY(y);
        RaycastHit hitInfo;
        var ray = Camera.main.ScreenPointToRay(new Vector3(x, y, 0));
        if (!Physics.Raycast(ray, out hitInfo, 100, _spatialManager.GetLayerMask())) return;
        GameObject textObj;
        var id = Guid.NewGuid();
        InstantiateText(out textObj, id, text, colorName,
            w => {
                w.transform.position = hitInfo.point;
                w.transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward, Camera.main.transform.up);
            });
        UpdateText(id, textObj, text, colorName);
        if(_onTextCreated!=null) _onTextCreated.Invoke(id, JsonUtility.ToJson(new TextCallBackCommand()
        {
            command = HoloCommand.COORDINATES,
            index = _textsCounter,
            id = id.ToString(),
            text = text,
            colorName = colorName
        }));
    }

    private void InstantiateText(out GameObject textObj, Guid id, string text, Colors.Name colorName, Action<GameObject> setPositionAndRotation)
    {
        Debug.Log("NEW TEXT:" + id);
        textObj = (GameObject)GameObject.Instantiate(Text, _textsContainer.transform);
        if (setPositionAndRotation != null) setPositionAndRotation.Invoke(textObj);
        _textsCounter++;
        textObj.transform.Find("Label").gameObject.GetComponent<TextMesh>().text = _textsCounter.ToString();
        textObj.transform.Find("Text").gameObject.GetComponent<TextMesh>().text = text;
        var color = Colors.GetInstance().GetColor(colorName);
        textObj.transform.Find("Label").gameObject.GetComponent<TextMesh>().color = color;
        textObj.transform.Find("Text").gameObject.GetComponent<TextMesh>().color = color;
        textObj.SetActive(true);
        _texts.Add(id, textObj);
    }

    private void SetTextColor(Guid id, Colors.Name colorName)
    {
        var color = Colors.GetInstance().GetColor(colorName);
        GameObject textObj;
        if (!_texts.TryGetValue(id, out textObj)) return;
        textObj.transform.Find("Label").gameObject.GetComponent<TextMesh>().color = color;
        textObj.transform.Find("Text").gameObject.GetComponent<TextMesh>().color = color;
        UpdateText(id, textObj, null, colorName);
    }

    private void SetTextCoordinatesW(HoloCommand command, float step, Guid id)
    {
        float x, y, z;
        GameObject textObj;
        if (!_texts.TryGetValue(id, out textObj)) return;
        x = textObj.transform.localPosition.x;
        y = textObj.transform.localPosition.y;
        z = textObj.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.UP:
                textObj.transform.localPosition = new Vector3(x, y + step, z);
                break;
            case HoloCommand.DOWN:
                textObj.transform.localPosition = new Vector3(x, y - step, z);
                break;
            case HoloCommand.LEFT:
                textObj.transform.localPosition = new Vector3(x - step, y, z);
                break;
            case HoloCommand.RIGHT:
                textObj.transform.localPosition = new Vector3(x + step, y, z);
                break;
            case HoloCommand.ZOOM_IN:
                textObj.transform.localScale += new Vector3(step, step, step);
                break;
            case HoloCommand.ZOOM_OUT:
                textObj.transform.localScale -= new Vector3(step, step, step);
                break;
            case HoloCommand.INCREASE_Z:
                textObj.transform.localPosition = new Vector3(x, y, z + step);
                break;
            case HoloCommand.DECREASE_Z:
                textObj.transform.localPosition = new Vector3(x, y, z - step);
                break;
            case HoloCommand.ROTATE_XUP:
                textObj.transform.Rotate(step, 0, 0);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                textObj.transform.Rotate(0, step, 0);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                textObj.transform.Rotate(0, 0, step);
                break;
            case HoloCommand.ROTATE_XDOWN:
                textObj.transform.Rotate(-step, 0, 0);
                break;
            case HoloCommand.ROTATE_YLEFT:
                textObj.transform.Rotate(0, -step, 0);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                textObj.transform.Rotate(0, 0, -step);
                break;
        }
        UpdateText(id, textObj, null, null);
    }

    private void SetTextCoordinatesWR(HoloCommand command, float step, Guid id, ReferenceSystem referenceSystem)
    {
        float x, y, z;
        GameObject textObj;
        if (!_texts.TryGetValue(id, out textObj)) return;
        x = textObj.transform.localPosition.x;
        y = textObj.transform.localPosition.y;
        z = textObj.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.LEFT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    textObj.transform.localPosition = new Vector3(x - step, y, z);
                else
                {
                    step = step * 10;
                    var dir = -Camera.main.transform.right;
                    textObj.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.RIGHT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    textObj.transform.localPosition = new Vector3(x + step, y, z);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.right;
                    textObj.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.INCREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    textObj.transform.localPosition = new Vector3(x, y, z + step);
                else
                {
                    step = step * 10;
                    var dir = -Camera.main.transform.forward;
                    textObj.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.DECREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    textObj.transform.localPosition = new Vector3(x, y, z - step);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.forward;
                    textObj.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
        }
        UpdateText(id, textObj, null, null);
    }

    private void RemoveText(Guid id)
    {
        GameObject textObj;
        if (!_texts.TryGetValue(id, out textObj)) return;
        UpdateText(id, null, null, null);
        if (_selectedText!=null && _selectedText.Equals(textObj)) _selectedText = null;
        GameObject.Destroy(textObj);
        _texts.Remove(id);
        if(_onTextDestroyed!=null) _onTextDestroyed.Invoke(JsonUtility.ToJson(new TextCallBackCommand()
        {
            command = HoloCommand.ACTIVE_DEACTIVE,
            id = id.ToString(),
        }));
    }

    private float NormalizeCoordX(float x)
    {
        return ArFovWidth * (x / 100) + ArFovOffsetX;
    }

    private float NormalizeCoordY(float y)
    {
        return ArFovHeight * (y / 100) + ArFovOffsetY;
    }

    public IEnumerator SetBlinkText(Guid id)
    {
        GameObject textObj;
        if (!_texts.TryGetValue(id, out textObj)) yield break;
        for (int i = 0; i < 3; i++)
        {
            if(textObj!=null) textObj.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            if (textObj != null) textObj.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
        if (textObj != null) textObj.SetActive(true);
    }

}
