﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoPlayerAdapterScript : MonoBehaviour
{

    public string VideoUrl;
    private bool _init;

    private VideoPlayer _videoPlayer;
    private AudioSource _audioSource;

    void Start()
    {
        Application.runInBackground = true;
    }

    public void Play()
    {
        if(!_init)
        {
            ClearVideo();
            StartCoroutine(PrepareAndPlayVideo());
            _init = true;
            return;
        }
        if (_videoPlayer.isPlaying) return;
        StartCoroutine(PlayVideo());
    }

    public void Pause()
    {
        if (!_videoPlayer.isPlaying) return;
        _videoPlayer.Pause();
        _audioSource.Pause();
    }

    public void Stop()
    {
        _videoPlayer.Stop();
        _audioSource.Stop();
        ClearVideo();
        StartCoroutine(PrepareVideo());
    }

    public void Reset()
    {
        _videoPlayer.Stop();
        _audioSource.Stop();
        ClearVideo();
        StartCoroutine(PrepareAndPlayVideo());
    }

    public void AudioOn()
    {
        _audioSource.enabled = true;
    }

    public void AudioOff()
    {
        _audioSource.enabled = false;
    }

    private void ClearVideo()
    {
        GetComponent<RawImage>().texture = null;
        GetComponent<RawImage>().color = new Color32(190, 190, 190, 255);
        gameObject.transform.Find("Title").gameObject.SetActive(true);
        Component.Destroy(GetComponent<VideoPlayer>());
        Component.Destroy(GetComponent<AudioSource>());
    }

    private IEnumerator PrepareVideo()
    {
        _videoPlayer = gameObject.AddComponent<VideoPlayer>();
        _audioSource = gameObject.AddComponent<AudioSource>();
        _videoPlayer.playOnAwake = false;
        _audioSource.playOnAwake = false;
        _audioSource.spatialize = true;
        _audioSource.spread = 0;
        _audioSource.spatialBlend = 1.0f;
         _audioSource.rolloffMode = AudioRolloffMode.Linear;
         _audioSource.minDistance = 2f;
         _audioSource.maxDistance = 4f;
        _videoPlayer.source = VideoSource.Url;
        _videoPlayer.url = VideoUrl;
        _videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        _videoPlayer.controlledAudioTrackCount = 1;
        _videoPlayer.EnableAudioTrack(0, true);
        _videoPlayer.SetTargetAudioSource(0, _audioSource);
        _videoPlayer.Prepare();
        while (!_videoPlayer.isPrepared)
            yield return null;
    }

    private IEnumerator PrepareAndPlayVideo()
    {
        yield return PrepareVideo();
        yield return PlayVideo();
    }

    private IEnumerator PlayVideo()
    {
        gameObject.transform.Find("Title").gameObject.SetActive(false);
        GetComponent<RawImage>().texture = _videoPlayer.texture;
        GetComponent<RawImage>().color = Color.white;
        _videoPlayer.Play();
        _audioSource.Play();
        while (_videoPlayer.isPlaying)
            yield return null;
    }

}
