﻿using System;

[Serializable]
public class MessageData
{
    public string type;
    public CommandMessage content;
}