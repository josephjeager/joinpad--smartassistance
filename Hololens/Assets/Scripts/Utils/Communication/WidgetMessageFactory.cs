﻿
public class WidgetMessageFactory : CommandMessageFactory {

    public WidgetMessageFactory()
    {
        this.dispatcher = Dispatcher.WIDGETS;
    }

    public CommandMessage CallBack(string command)
    {
        return new CommandMessage()
        {
            dispatcher = this.dispatcher,
            command = command
        };
    }

}   
