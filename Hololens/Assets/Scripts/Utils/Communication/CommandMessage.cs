﻿using System;

[Serializable]
public class CommandMessage
{
    public Dispatcher dispatcher;
    public string command;
}