﻿using System;
using UnityEngine;

public class Settings : ISettings {

    private const string INITIALIZED = "Initialized";
    private const string CALL_TO_REUSME = "CallToResume";
    private const string CLIENT_NAME = "ClientName";
    private const string SERVER_ADDRESS = "ServerAddress";
    private const string SERVER_PORT = "ServerPort";
    private const string WIDGETS_BACKUP = "WidgetsBackup";
    private const string TEXTS_BACKUP = "TextsBackup";
    private const string IMAGES_BACKUP = "ImagesBackup";
    private const string DOCUMENTS_BACKUP = "DocumentsBackup";
    private const string VIDEOS_BACKUP = "VideosBackup";
    private const string LAST_PEER_NAME_BACKUP = "LastPeerNameBackup";
    private const string LAST_PEER_SESSION_ID = "LastPeerSessionId";

    private bool _initialized;
    public bool Initialized
    {
        set { _initialized = value; }
        get { return _initialized; }
    }

    private bool _callToResume;
    public bool CallToResume
    {
        set { _callToResume = value; }
        get { return _callToResume; }
    }   

    private string _clientName;
    public string ClientName
    {
        set { _clientName = value; }
        get { return _clientName; }
    }

    private string _serverAddress;
    public string ServerAddress
    {
        set { _serverAddress = value; }
        get { return _serverAddress; }
    }

    private string _serverPort;
    public string ServerPort
    {
        set { _serverPort = value; }
        get { return _serverPort; }
    }

    private string _widgetsBackup;
    public string WidgetsBackup
    {
        set { _widgetsBackup = value; }
        get { return _widgetsBackup; }
    }

    private string _textsBackup;
    public string TextsBackup
    {
        set { _textsBackup = value; }
        get { return _textsBackup; }
    }

    private string _imagesBackup;
    public string ImagesBackup
    {
        set { _imagesBackup = value; }
        get { return _imagesBackup; }
    }

    private string _documentsBackup;
    public string DocumentsBackup
    {
        set { _documentsBackup = value; }
        get { return _documentsBackup; }
    }

    private string _videosBackup;
    public string VideosBackup
    {
        set { _videosBackup = value; }
        get { return _videosBackup; }
    }

    private string _lastPeerNameBackup;
    public string LastPeerNameBackup
    {
        set { _lastPeerNameBackup = value; }
        get { return _lastPeerNameBackup; }
    }

    private string _lastPeerSessionIdBackup;
    public string LastPeerSessionIdBackup
    {
        set { _lastPeerSessionIdBackup = value; }
        get { return _lastPeerSessionIdBackup; }
    }

    public Settings()
    {
        _initialized = Convert.ToBoolean(PlayerPrefs.GetInt(INITIALIZED));
        _callToResume = Convert.ToBoolean(PlayerPrefs.GetInt(CALL_TO_REUSME));
        _clientName = PlayerPrefs.GetString(CLIENT_NAME);
        _serverAddress = PlayerPrefs.GetString(SERVER_ADDRESS);
        _serverPort = PlayerPrefs.GetString(SERVER_PORT);
        _widgetsBackup = PlayerPrefs.GetString(WIDGETS_BACKUP);
        _textsBackup = PlayerPrefs.GetString(TEXTS_BACKUP);
        _imagesBackup = PlayerPrefs.GetString(IMAGES_BACKUP);
        _documentsBackup = PlayerPrefs.GetString(DOCUMENTS_BACKUP);
        _videosBackup = PlayerPrefs.GetString(VIDEOS_BACKUP);
        _lastPeerNameBackup = PlayerPrefs.GetString(LAST_PEER_NAME_BACKUP);
        _lastPeerSessionIdBackup = PlayerPrefs.GetString(LAST_PEER_SESSION_ID);
    }

    public void Save()
    {
        PlayerPrefs.SetInt(INITIALIZED, Convert.ToInt32(_initialized));
        PlayerPrefs.SetInt(CALL_TO_REUSME, Convert.ToInt32(_callToResume));
        PlayerPrefs.SetString(CLIENT_NAME, _clientName);
        PlayerPrefs.SetString(SERVER_ADDRESS, _serverAddress);
        PlayerPrefs.SetString(SERVER_PORT, _serverPort);
        PlayerPrefs.SetString(WIDGETS_BACKUP, _widgetsBackup);
        PlayerPrefs.SetString(TEXTS_BACKUP, _textsBackup);
        PlayerPrefs.SetString(IMAGES_BACKUP, _imagesBackup);
        PlayerPrefs.SetString(DOCUMENTS_BACKUP, _documentsBackup);
        PlayerPrefs.SetString(VIDEOS_BACKUP, _videosBackup);
        PlayerPrefs.SetString(LAST_PEER_NAME_BACKUP, _lastPeerNameBackup);
        PlayerPrefs.SetString(LAST_PEER_SESSION_ID, _lastPeerSessionIdBackup);
        PlayerPrefs.Save();
    }
}
