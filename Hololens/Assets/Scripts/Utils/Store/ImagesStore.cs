﻿using System;
using System.Collections.Generic;

[Serializable]
public class ImagesStore
{
    public List<ImageRecord> imageRecords;

    public ImagesStore()
    {
        imageRecords = new List<ImageRecord>();
    }
}

[Serializable]
public class ImageRecord
{
    public string id;
    public Direction direction;
    public string url;
}