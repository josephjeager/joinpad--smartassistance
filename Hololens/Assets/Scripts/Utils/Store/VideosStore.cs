﻿using System;
using System.Collections.Generic;

[Serializable]
public class VideosStore
{
    public List<VideoRecord> videoRecords;

    public VideosStore()
    {
        videoRecords = new List<VideoRecord>();
    }
}

[Serializable]
public class VideoRecord
{
    public string id;
    public string url;
    public string title;
}