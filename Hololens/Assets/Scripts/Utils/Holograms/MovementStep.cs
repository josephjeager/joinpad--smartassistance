﻿
public class MovementStep : IStep {

    private readonly int _step;

    public MovementStep(int step)
    {
        _step = step;
    }

    public float GetEffectiveStep()
    {
        return (float)(((double) _step)/100);
    }

}
