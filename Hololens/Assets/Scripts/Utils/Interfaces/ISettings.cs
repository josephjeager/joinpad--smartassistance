﻿
public interface ISettings {

    bool Initialized { get; set; }
    bool CallToResume { get; set; }
    string ClientName { get; set; }
    string ServerAddress { get; set; }
    string ServerPort { get; set; }
    string WidgetsBackup { get; set; }
    string TextsBackup { get; set; }
    string ImagesBackup { get; set; }
    string DocumentsBackup { get; set; }
    string VideosBackup { get; set; }
    string LastPeerNameBackup { get; set; }
    string LastPeerSessionIdBackup { get; set; }
    void Save();

}
