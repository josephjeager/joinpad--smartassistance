﻿
public enum VideoCommand
{
    PLAY = 0,
    PAUSE = 1,
    STOP = 2,
    RESET = 3,
    AUDIO_ON = 4,
    AUDIO_OFF = 5
}