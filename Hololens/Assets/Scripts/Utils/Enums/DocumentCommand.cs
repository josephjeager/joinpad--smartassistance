﻿
public enum DocumentCommand
{
    ZOOM_IN = 0,
    ZOOM_OUT = 1,
    SCROLL_UP = 2,
    SCROLL_DOWN = 3
}