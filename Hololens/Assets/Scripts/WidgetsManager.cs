﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.XR.WSA.Persistence;
using UnityEngine.XR.WSA;

public class WidgetsManager : IWidgetsManager {

    private Action<string> _onWidgetLoaded;
    public Action<string> OnWidgetLoaded
    {
        set { _onWidgetLoaded = value; }
    }

    private Action<Guid, string> _onWidgetCreated;
    public Action<Guid, string> OnWidgetCreated
    {
        set { _onWidgetCreated = value; }
    }

    private Action<string> _onWidgetDestroyed;
    public Action<string> OnWidgetDestroyed
    {
        set { _onWidgetDestroyed = value; }
    }

    private readonly GameObject _widgetsContainer;
    private GameObject WidgetsContainer
    {
        get
        {
            return _widgetsContainer;
        }
    }

    private GameObject _square;
    private GameObject Square
    {
        get
        {
            return _square ?? (_square = WidgetsContainer.transform.Find("Square").gameObject);
        }
    }

    private GameObject _arrow;
    private GameObject Arrow
    {
        get
        {
            return _arrow ?? (_arrow = WidgetsContainer.transform.Find("Arrow").gameObject);
        }
    }

    private GameObject _circle;
    private GameObject Circle
    {
        get
        {
            return _circle ?? (_circle = WidgetsContainer.transform.Find("Circle").gameObject);
        }
    }

    private readonly ISpatialManager _spatialManager;
    private readonly ISettings _settings;
    private readonly Dictionary<Guid, GameObject> _widgets;
    private int _widgetsCounter;
    private GameObject _selectedWidget;

    private WidgetsStore _widgetsStore;
    private readonly WorldAnchorStoreContainer _anchorStoreContainer;
    private WorldAnchorStore AnchorStore
    {
        get
        {
            return _anchorStoreContainer.AnchorStore;
        }
    }

    private float ArFovWidth = 915f;
    private float ArFovHeight = 515f;
    private float ArFovOffsetX = 210f;
    private float ArFovOffsetY = 85f;

    private const string ARROW_GUID  = "00000000-0000-0000-0000-000000000000";
    private const string CIRCLE_GUID = "00000000-0000-0000-0000-000000000001";
    private const string SQUARE_GUID = "00000000-0000-0000-0000-000000000002";

    public WidgetsManager(GameObject widgetsContainer, ISpatialManager spatialManager, ISettings settings, WorldAnchorStoreContainer anchorStoreContainer)
    {
        _widgetsContainer = widgetsContainer;
        _spatialManager = spatialManager;
        _settings = settings;
        _anchorStoreContainer = anchorStoreContainer;
        _widgets = new Dictionary<Guid, GameObject>();
        _widgetsCounter = 0;
        _widgetsStore = new WidgetsStore();
    }

    [Serializable]
    private class WidgetIncomingCommand
    {
        public HoloCommand command;
        public float x;
        public float y;
        public int step;
        public string widgetId;
        public string text;
        public Colors.Name colorName;
        public ReferenceSystem referenceSystem;
    }

    [Serializable]
    private class WidgetCallBackCommand
    {
        public HoloCommand command;
        public int index;
        public string id;
        public string text;
        public Colors.Name colorName;
    }

    public void ReadCommand(string command)
    {
        bool needToSave = false;
        var widgetMessage = JsonUtility.FromJson<WidgetIncomingCommand>(command);
        var widgetId = new Guid(widgetMessage.widgetId);
        switch (widgetMessage.command)
        {
            case HoloCommand.CLEAR:
                ClearWidgets();
                needToSave = true;
                break;
            case HoloCommand.COORDINATES:
                CreateWidget(widgetMessage.x, widgetMessage.y, widgetId, widgetMessage.text, widgetMessage.colorName);
                needToSave = true;
                break;
            case HoloCommand.ACTIVE_DEACTIVE:
                RemoveWidget(widgetId);
                needToSave = true;
                break;
            case HoloCommand.SET_WHITE:
                SetWidgetColor(widgetId, Colors.Name.WHITE);
                needToSave = true;
                break;
            case HoloCommand.SET_RED:
                SetWidgetColor(widgetId, Colors.Name.RED);
                needToSave = true;
                break;
            case HoloCommand.SET_YELLOW:
                SetWidgetColor(widgetId, Colors.Name.YELLOW);
                needToSave = true;
                break;
            case HoloCommand.SET_GREEN:
                SetWidgetColor(widgetId, Colors.Name.GREEN);
                needToSave = true;
                break;
            case HoloCommand.SET_BLUE:
                SetWidgetColor(widgetId, Colors.Name.BLUE);
                needToSave = true;
                break;
            case HoloCommand.UP:
                SetWidgetCoordinatesW(HoloCommand.UP, new MovementStep(widgetMessage.step).GetEffectiveStep(), widgetId);
                break;
            case HoloCommand.DOWN:
                SetWidgetCoordinatesW(HoloCommand.DOWN, new MovementStep(widgetMessage.step).GetEffectiveStep(), widgetId);
                break;
            case HoloCommand.LEFT:
                SetWidgetCoordinatesWR(HoloCommand.LEFT, new MovementStep(widgetMessage.step).GetEffectiveStep(), widgetId, widgetMessage.referenceSystem);
                break;
            case HoloCommand.RIGHT:
                SetWidgetCoordinatesWR(HoloCommand.RIGHT, new MovementStep(widgetMessage.step).GetEffectiveStep(), widgetId, widgetMessage.referenceSystem);
                break;
            case HoloCommand.ZOOM_IN:
                SetWidgetCoordinatesW(HoloCommand.ZOOM_IN, 0.05f, widgetId);
                break;
            case HoloCommand.ZOOM_OUT:
                SetWidgetCoordinatesW(HoloCommand.ZOOM_OUT, 0.05f, widgetId);
                break;
            case HoloCommand.INCREASE_Z:
                SetWidgetCoordinatesWR(HoloCommand.INCREASE_Z, new MovementStep(widgetMessage.step).GetEffectiveStep(), widgetId, widgetMessage.referenceSystem);
                break;
            case HoloCommand.DECREASE_Z:
                SetWidgetCoordinatesWR(HoloCommand.DECREASE_Z, new MovementStep(widgetMessage.step).GetEffectiveStep(), widgetId, widgetMessage.referenceSystem);
                break;
            case HoloCommand.ROTATE_XUP:
                SetWidgetCoordinatesW(HoloCommand.ROTATE_XUP, new RotationStep(widgetMessage.step).GetEffectiveStep(), widgetId);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                SetWidgetCoordinatesW(HoloCommand.ROTATE_YRIGHT, new RotationStep(widgetMessage.step).GetEffectiveStep(), widgetId);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                SetWidgetCoordinatesW(HoloCommand.ROTATE_ZRIGHT, new RotationStep(widgetMessage.step).GetEffectiveStep(), widgetId);
                break;
            case HoloCommand.ROTATE_XDOWN:
                SetWidgetCoordinatesW(HoloCommand.ROTATE_XDOWN, new RotationStep(widgetMessage.step).GetEffectiveStep(), widgetId);
                break;
            case HoloCommand.ROTATE_YLEFT:
                SetWidgetCoordinatesW(HoloCommand.ROTATE_YLEFT, new RotationStep(widgetMessage.step).GetEffectiveStep(), widgetId);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                SetWidgetCoordinatesW(HoloCommand.ROTATE_ZLEFT, new RotationStep(widgetMessage.step).GetEffectiveStep(), widgetId);
                break;
            case HoloCommand.EDIT:
                EditWidgetText(widgetId, widgetMessage.text);
                needToSave = true;
                break;
            case HoloCommand.SELECTION:
                SelectWidget(widgetId);
                break;
            case HoloCommand.UNSELECTION:
                UnselectWidget();
                break;
        }
        if (needToSave) SaveWidgets();
    }

    public void ClearWidgetsContainer()
    {
        ClearWidgets();
    }

    public void LoadWidgetsContainer()
    {
        if (string.IsNullOrEmpty(_settings.WidgetsBackup)) return;
        _widgetsStore = JsonUtility.FromJson<WidgetsStore>(_settings.WidgetsBackup);
        _widgetsStore.widgetRecords.ForEach(w => 
        {
            GameObject widget;
            var id = new Guid(w.id);
            InstantiateWidget(out widget, new Guid(w.type), id, w.text, w.colorName, wi =>
            {
                AnchorStore.Load(w.id.ToString(), wi);
                Component.Destroy(wi.GetComponent<WorldAnchor>());
            });
            _onWidgetLoaded.Invoke(JsonUtility.ToJson(new WidgetCallBackCommand()
            {
                command = HoloCommand.COORDINATES,
                index = _widgetsCounter,
                id = w.id,
                text = w.text,
                colorName = w.colorName
            }));
        });
    }

    public void HideWidgetsContainer()
    {
        WidgetsContainer.SetActive(false);
        ClearWidgets();
    }

    public void ShowWidgetsContainer()
    {
        WidgetsContainer.SetActive(true);
    }

    public void RemoveWidget(GameObject widget)
    {
        var id = _widgets.FirstOrDefault(t => t.Value.Equals(widget)).Key;
        RemoveWidget(id);
        SaveWidgets();
    }

    private void UpdateWidget(Guid id, Guid? type, GameObject widget, string text, Colors.Name? colorName)
    {
        if(widget == null)
        {
            AnchorStore.Delete(id.ToString());
            _widgetsStore.widgetRecords.Remove(_widgetsStore.widgetRecords.FirstOrDefault(w => w.id == id.ToString()));
            return;
        }
        widget.AddComponent<WorldAnchor>();
        AnchorStore.Delete(id.ToString());
        AnchorStore.Save(id.ToString(), widget.GetComponent<WorldAnchor>());
        Component.Destroy(widget.GetComponent<WorldAnchor>());
        var widgetStored = _widgetsStore.widgetRecords.FirstOrDefault(w => w.id == id.ToString());
        if (widgetStored == null)
        {
            _widgetsStore.widgetRecords.Add(new WidgetRecord
            {
                id = id.ToString(),
                type = type.Value.ToString(),
                text = text,
                colorName = colorName.Value
            });
            return;
        }
        if (text!=null) widgetStored.text = text;
        if(colorName!=null) widgetStored.colorName = colorName.Value;
    }

    private void SaveWidgets()
    {
        _settings.WidgetsBackup = JsonUtility.ToJson(_widgetsStore);
        _settings.CallToResume = true;
        _settings.Save();
    }

    private void ClearWidgets()
    {
        _widgets.Keys.ToList().ForEach(RemoveWidget);
        _widgetsCounter = 0;
        _settings.WidgetsBackup = string.Empty;
        _settings.Save();
    }

    private void CreateWidget(float x, float y, Guid type, string text, Colors.Name colorName)
    {
        x = NormalizeCoordX(x);
        y = NormalizeCoordY(y);
        RaycastHit hitInfo;
        var ray = Camera.main.ScreenPointToRay(new Vector3(x, y, 0));
        if (!Physics.Raycast(ray, out hitInfo, 100, _spatialManager.GetLayerMask())) return;
        GameObject widget;
        var id = Guid.NewGuid();
        InstantiateWidget(out widget, type, id, text, colorName,
            (w) => {
                w.transform.position = hitInfo.point;
                w.transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward, Camera.main.transform.up);
            });
        UpdateWidget(id, type, widget, text, colorName);
        if(_onWidgetCreated!=null) _onWidgetCreated.Invoke(id, JsonUtility.ToJson(new WidgetCallBackCommand()
        {
            command = HoloCommand.COORDINATES,
            index = _widgetsCounter,
            id = id.ToString(),
            text = text,
            colorName = colorName
        }));
    }

    private void InstantiateWidget(out GameObject widget, Guid type, Guid id, string text, Colors.Name colorName, Action<GameObject> setPositionAndRotation)
    {
        widget = null;
        switch (type.ToString())
        {
            case ARROW_GUID:
                Debug.Log("NEW ARROW:" + id);
                widget = (GameObject)GameObject.Instantiate(Arrow, _widgetsContainer.transform);
                break;
            case CIRCLE_GUID:
                Debug.Log("NEW CIRCLE:" + id);
                widget = (GameObject)GameObject.Instantiate(Circle, _widgetsContainer.transform);
                break;
            case SQUARE_GUID:
                Debug.Log("NEW SQUARE:" + id);
                widget = (GameObject)GameObject.Instantiate(Square, _widgetsContainer.transform);
                break;
        }
        _widgetsCounter++;
        if (setPositionAndRotation != null) setPositionAndRotation.Invoke(widget);
        if (widget == null) return;
        widget.transform.Find("Label").gameObject.GetComponent<TextMesh>().text = _widgetsCounter.ToString();
        widget.transform.Find("Text").gameObject.GetComponent<TextMesh>().text = text;
        var color = Colors.GetInstance().GetColor(colorName);
        widget.transform.Find("Object").gameObject.GetComponent<Renderer>().material.color = color;
        widget.transform.Find("Label").gameObject.GetComponent<TextMesh>().color = color;
        widget.transform.Find("Text").gameObject.GetComponent<TextMesh>().color = color;
        widget.SetActive(true);
        _widgets.Add(id, widget);
    }

    private void SetWidgetColor(Guid id, Colors.Name colorName)
    {
        var color = Colors.GetInstance().GetColor(colorName);
        GameObject widget;
        if (!_widgets.TryGetValue(id, out widget)) return;
        var widgetObject = widget.transform.Find("Object").gameObject;
        var widgetLabel = widget.transform.Find("Label").gameObject;
        var widgetText = widget.transform.Find("Text").gameObject;
        widgetObject.GetComponent<Renderer>().material.color = color;
        widgetLabel.GetComponent<TextMesh>().color = color;
        widgetText.GetComponent<TextMesh>().color = color;
        UpdateWidget(id, null, widget, null, colorName);
    }

    private void SetWidgetCoordinatesW(HoloCommand command, float step, Guid id)
    {
        float x, y, z;
        GameObject widget;
        if (!_widgets.TryGetValue(id, out widget)) return;
        x = widget.transform.localPosition.x;
        y = widget.transform.localPosition.y;
        z = widget.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.UP:
                widget.transform.localPosition = new Vector3(x, y + step, z);
                break;
            case HoloCommand.DOWN:
                widget.transform.localPosition = new Vector3(x, y - step, z);
                break;
            case HoloCommand.LEFT:
                widget.transform.localPosition = new Vector3(x - step, y, z);
                break;
            case HoloCommand.RIGHT:
                widget.transform.localPosition = new Vector3(x + step, y, z);
                break;
            case HoloCommand.ZOOM_IN:
                widget.transform.localScale += new Vector3(step, step, step);
                break;
            case HoloCommand.ZOOM_OUT:
                widget.transform.localScale -= new Vector3(step, step, step);
                break;
            case HoloCommand.INCREASE_Z:
                widget.transform.localPosition = new Vector3(x, y, z + step);
                break;
            case HoloCommand.DECREASE_Z:
                widget.transform.localPosition = new Vector3(x, y, z - step);
                break;
            case HoloCommand.ROTATE_XUP:
                widget.transform.Rotate(step, 0, 0);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                widget.transform.Rotate(0, step, 0);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                widget.transform.Rotate(0, 0, step);
                break;
            case HoloCommand.ROTATE_XDOWN:
                widget.transform.Rotate(-step, 0, 0);
                break;
            case HoloCommand.ROTATE_YLEFT:
                widget.transform.Rotate(0, -step, 0);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                widget.transform.Rotate(0, 0, -step);
                break;
        }
        UpdateWidget(id, null, widget, null, null);
    }

    private void SetWidgetCoordinatesWR(HoloCommand command, float step, Guid id, ReferenceSystem referenceSystem)
    {
        GameObject widget;
        if (!_widgets.TryGetValue(id, out widget)) return;
        if (widget == null || !widget.activeInHierarchy) return;
        var x = widget.transform.localPosition.x;
        var y = widget.transform.localPosition.y;
        var z = widget.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.LEFT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    widget.transform.localPosition = new Vector3(x - step, y, z);
                else
                {
                    step = step * 10;
                    Vector3 dir = -Camera.main.transform.right;
                    widget.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.RIGHT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    widget.transform.localPosition = new Vector3(x + step, y, z);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.right;
                    widget.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.INCREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    widget.transform.localPosition = new Vector3(x, y, z + step);
                else
                {
                    step = step * 10;
                    var dir = -Camera.main.transform.forward;
                    widget.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.DECREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    widget.transform.localPosition = new Vector3(x, y, z - step);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.forward;
                    widget.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
        }
        UpdateWidget(id, null, widget, null, null);
    }

    private void RemoveWidget(Guid id)
    {
        GameObject widget;
        if (!_widgets.TryGetValue(id, out widget)) return;
        UpdateWidget(id, null, null, null, null);
        if (_selectedWidget!=null && _selectedWidget.Equals(widget)) _selectedWidget = null;
        GameObject.Destroy(widget);
        _widgets.Remove(id);
        if(_onWidgetDestroyed!=null) _onWidgetDestroyed.Invoke(JsonUtility.ToJson(new WidgetCallBackCommand()
        {
            command = HoloCommand.ACTIVE_DEACTIVE,
            id = id.ToString(),
        }));
    }

    private void EditWidgetText(Guid id, string text)
    {
        GameObject widget;
        if (!_widgets.TryGetValue(id, out widget)) return;
        widget.transform.Find("Text").gameObject.GetComponent<TextMesh>().text = text;
        UpdateWidget(id, null, widget, text, null);
    }

    private void SelectWidget(Guid id)
    {
        if(_selectedWidget!=null)
            ((Behaviour)_selectedWidget.transform.Find("Object").GetComponent("Halo")).enabled = false;
        GameObject widget;
        if (!_widgets.TryGetValue(id, out widget)) return;
        ((Behaviour)widget.transform.Find("Object").GetComponent("Halo")).enabled = true;
        _selectedWidget = widget;
    }

    private void UnselectWidget()
    {
        if (_selectedWidget != null)
            ((Behaviour)_selectedWidget.transform.Find("Object").GetComponent("Halo")).enabled = false;
        _selectedWidget = null;
    }

    private float NormalizeCoordX(float x)
    {
        return ArFovWidth * (x / 100) + ArFovOffsetX;
    }

    private float NormalizeCoordY(float y)
    {
        return ArFovHeight * (y / 100) + ArFovOffsetY;
    }
    
    public IEnumerator SetBlinkWidget(Guid id)
    {
        GameObject widget;
        if (!_widgets.TryGetValue(id, out widget)) yield break;
        for (int i = 0; i < 3; i++)
        {
            if (widget != null) widget.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            if (widget != null) widget.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
        if (widget != null) widget.SetActive(true);
    }

}