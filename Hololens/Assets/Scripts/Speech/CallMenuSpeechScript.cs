﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CallMenuSpeechScript : MonoBehaviour, ISpeechHandler
{
    public ControlBusScript ControlBus;

    public void OnSpeechKeywordRecognized(SpeechEventData eventData)
    {
        var command = eventData.RecognizedText.ToUpper();
        Debug.Log("VOCAL COMMAND: " + command);
    }
}
