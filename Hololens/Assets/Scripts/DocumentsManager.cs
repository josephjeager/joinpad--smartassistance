﻿using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Persistence;
using UnityEngine.Networking;

public class DocumentsManager : IDocumentsManager {

    private Action<string> _onDocumentLoaded;
    public Action<string> OnDocumentLoaded
    {
        set { _onDocumentLoaded = value; }
    }

    private Action<string,float,GameObject> _onDocumentInstantiated;
    public Action<string,float,GameObject> OnDocumentInstantiated
    {
        set { _onDocumentInstantiated = value; }
    }

    private Action<Guid, string> _onDocumentCreated;
    public Action<Guid, string> OnDocumentCreated
    {
        set { _onDocumentCreated = value; }
    }

    private Action<string> _onDocumentDestroyed;
    public Action<string> OnDocumentDestroyed
    {
        set { _onDocumentDestroyed = value; }
    }

    private readonly GameObject _documentsContainer;
    private GameObject DocumentsContainer
    {
        get
        {
            return _documentsContainer;
        }
    }

    private GameObject _document;
    private GameObject Document
    {
        get
        {
            return _document ?? (_document = DocumentsContainer.transform.Find("Document").gameObject);
        }
    }

    private readonly ISpatialManager _spatialManager;
    private readonly ISettings _settings;
    private readonly Dictionary<Guid, GameObject> _documents;
    private int _documentsCounter;
    private GameObject _selectedDocument;
    private GameObject _catchedDocument;

    private DocumentsStore _documentsStore;
    private readonly WorldAnchorStoreContainer _anchorStoreContainer;
    private WorldAnchorStore AnchorStore
    {
        get
        {
            return _anchorStoreContainer.AnchorStore;
        }
    }

    public DocumentsManager(GameObject documentsContainer, ISpatialManager spatialManager, ISettings settings, WorldAnchorStoreContainer anchorStoreContainer)
    {
        _documentsContainer = documentsContainer;
        _spatialManager = spatialManager;
        _settings = settings;
        _anchorStoreContainer = anchorStoreContainer;
        _documents = new Dictionary<Guid, GameObject>();
        _documentsCounter = 0;
        _documentsStore = new DocumentsStore();
    }

    [Serializable]
    private class DocumentIncomingCommand
    {
        public HoloCommand command;
        public int step;
        public string documentId;
        public string url;
        public int pagesNumber;
        public ReferenceSystem referenceSystem;
    }

    [Serializable]
    private class DocumentCallBackCommand
    {
        public HoloCommand command;
        public int index;
        public string id;
    }

    public void ReadCommand(string command)
    {
        bool needToSave = false;
        var documentMessage = JsonUtility.FromJson<DocumentIncomingCommand>(command);
        var documentId = new Guid(documentMessage.documentId);
        switch (documentMessage.command)
        {
            case HoloCommand.CLEAR:
                ClearDocuments();
                needToSave = true;
                break;
            case HoloCommand.COORDINATES:
                CreateDocument(documentMessage.pagesNumber, documentMessage.url);
                needToSave = true;
                break;
            case HoloCommand.ACTIVE_DEACTIVE:
                RemoveDocument(documentId);
                needToSave = true;
                break;
            case HoloCommand.UP:
                SetDocumentCoordinatesW(HoloCommand.UP, new MovementStep(documentMessage.step).GetEffectiveStep(), documentId);
                break;
            case HoloCommand.DOWN:
                SetDocumentCoordinatesW(HoloCommand.DOWN, new MovementStep(documentMessage.step).GetEffectiveStep(), documentId);
                break;
            case HoloCommand.LEFT:
                SetDocumentCoordinatesWR(HoloCommand.LEFT, new MovementStep(documentMessage.step).GetEffectiveStep(), documentId, documentMessage.referenceSystem);
                break;
            case HoloCommand.RIGHT:
                SetDocumentCoordinatesWR(HoloCommand.RIGHT, new MovementStep(documentMessage.step).GetEffectiveStep(), documentId, documentMessage.referenceSystem);
                break;
            case HoloCommand.ZOOM_IN:
                SetDocumentCoordinatesW(HoloCommand.ZOOM_IN, 0.05f, documentId);
                break;
            case HoloCommand.ZOOM_OUT:
                SetDocumentCoordinatesW(HoloCommand.ZOOM_OUT, 0.05f, documentId);
                break;
            case HoloCommand.INCREASE_Z:
                SetDocumentCoordinatesWR(HoloCommand.INCREASE_Z, new MovementStep(documentMessage.step).GetEffectiveStep(), documentId, documentMessage.referenceSystem);
                break;
            case HoloCommand.DECREASE_Z:
                SetDocumentCoordinatesWR(HoloCommand.DECREASE_Z, new MovementStep(documentMessage.step).GetEffectiveStep(), documentId, documentMessage.referenceSystem);
                break;
            case HoloCommand.ROTATE_XUP:
                SetDocumentCoordinatesW(HoloCommand.ROTATE_XUP, new RotationStep(documentMessage.step).GetEffectiveStep(), documentId);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                SetDocumentCoordinatesW(HoloCommand.ROTATE_YRIGHT, new RotationStep(documentMessage.step).GetEffectiveStep(), documentId);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                SetDocumentCoordinatesW(HoloCommand.ROTATE_ZRIGHT, new RotationStep(documentMessage.step).GetEffectiveStep(), documentId);
                break;
            case HoloCommand.ROTATE_XDOWN:
                SetDocumentCoordinatesW(HoloCommand.ROTATE_XDOWN, new RotationStep(documentMessage.step).GetEffectiveStep(), documentId);
                break;
            case HoloCommand.ROTATE_YLEFT:
                SetDocumentCoordinatesW(HoloCommand.ROTATE_YLEFT, new RotationStep(documentMessage.step).GetEffectiveStep(), documentId);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                SetDocumentCoordinatesW(HoloCommand.ROTATE_ZLEFT, new RotationStep(documentMessage.step).GetEffectiveStep(), documentId);
                break;
            case HoloCommand.SELECTION:
                SelectDocument(documentId);
                break;
            case HoloCommand.UNSELECTION:
                UnselectDocument();
                break;
        }
        if (needToSave) SaveDocuments();
    }

    public void ClearDocumentsContainer()
    {
        ClearDocuments();
    }

    public void LoadDocumentsContainer()
    {
        if (string.IsNullOrEmpty(_settings.DocumentsBackup)) return;
        _documentsStore = JsonUtility.FromJson<DocumentsStore>(_settings.DocumentsBackup);
        _documentsStore.documentRecords.ForEach(d =>
        {
            GameObject document;
            InstantiateDocument(out document, new Guid(d.id), d.pagesNumber, d.bookmark, d.url, doc =>
            {
                AnchorStore.Load(d.id.ToString(), doc);
                Component.Destroy(doc.GetComponent<WorldAnchor>());
            });
            _onDocumentLoaded.Invoke(JsonUtility.ToJson(new DocumentCallBackCommand()
            {
                command = HoloCommand.COORDINATES,
                index = _documentsCounter,
                id = d.id,
            }));
            document.GetComponent<Billboard>().enabled = false;
            document.GetComponent<Tagalong>().enabled = false;
            document.transform.Find("Canvas/FooterPanel").gameObject.SetActive(false);
        });
    }

    public void HideDocumentsContainer()
    {
        DocumentsContainer.SetActive(false);
        ClearDocuments();
    }

    public void ShowDocumentsContainer()
    {
        DocumentsContainer.SetActive(true);
    }

    public void CatchDocument(GameObject document)
    {
        _catchedDocument = document;
        _catchedDocument.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 2));
        _catchedDocument.GetComponent<BoxCollider>().enabled = false;
        _catchedDocument.transform.Find("Canvas/ScrollView").GetComponent<BoxCollider>().enabled = true;
        _catchedDocument.GetComponent<Billboard>().enabled = true;
        _catchedDocument.GetComponent<Tagalong>().enabled = true;
        var fixedAngularSize = _catchedDocument.GetComponent<FixedAngularSize>();
        if (fixedAngularSize != null)
            fixedAngularSize.enabled = true;
        _catchedDocument.transform.Find("Canvas/FooterPanel").gameObject.SetActive(true);
    }

    public Guid PlaceCatchedDocument()
    {
        var documentId = _documents.FirstOrDefault(d => d.Value.Equals(_catchedDocument)).Key;
        _catchedDocument.GetComponent<BoxCollider>().enabled = true;
        _catchedDocument.transform.Find("Canvas/ScrollView").GetComponent<BoxCollider>().enabled = false;
        _catchedDocument.GetComponent<Billboard>().enabled = false;
        _catchedDocument.GetComponent<Tagalong>().enabled = false;
        _catchedDocument.GetComponent<FixedAngularSize>().enabled = false;
        _catchedDocument.transform.Find("Canvas/FooterPanel").gameObject.SetActive(false);
        _catchedDocument = null;
        return documentId;
    }

    public void FixDocumentPosition(Guid documentId)
    {
        RaycastHit hitInfo;
        var ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        if (!Physics.Raycast(ray, out hitInfo, 100, _spatialManager.GetLayerMask())) return;
        var document = _documents.FirstOrDefault(i => i.Key.Equals(documentId)).Value;
        if (document.transform.position.z > hitInfo.point.z)
        {
            document.transform.position = new Vector3(hitInfo.point.x, hitInfo.point.y, hitInfo.point.z - 0.1f);
            document.transform.rotation = Quaternion.identity;
        }
        var bookmark = document.transform.Find("Canvas/ScrollView").gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition;
        UpdateDocument(documentId, document, null, bookmark, null);
        SaveDocuments(); // Need this for bookmark
    }

    public void RemoveCatchedDocument()
    {
        if (_catchedDocument == null) return;
        var documentId = _documents.FirstOrDefault(d => d.Value.Equals(_catchedDocument)).Key;
        RemoveDocument(documentId);
        _catchedDocument = null;
        SaveDocuments();
    }

    public void ZoomIn(Guid documentId)
    {
        if (_catchedDocument != null) return;
        GameObject document;
        if (!_documents.TryGetValue(documentId, out document)) return;
        document.transform.localScale += new Vector3(0.05f, 0.05f, 0.05f);
        UpdateDocument(documentId, document, null, null, null);
    }

    public void ZoomOut(Guid documentId)
    {
        if (_catchedDocument != null) return;
        GameObject document;
        if (!_documents.TryGetValue(documentId, out document)) return;
        document.transform.localScale -= new Vector3(0.05f, 0.05f, 0.05f);
        UpdateDocument(documentId, document, null, null, null);
    }

    public void ScrollUp(Guid documentId)
    {
        if (_catchedDocument != null) return;
        GameObject document;
        if (!_documents.TryGetValue(documentId, out document)) return;
        var normalizedPosition = document.transform.Find("Canvas/ScrollView").gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition;
        if (1 - normalizedPosition <= 0.2f)
            document.transform.Find("Canvas/ScrollView").gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = 1;
        else document.transform.Find("Canvas/ScrollView").gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition += 0.2f;
        SaveDocuments(); // Need this for bookmark
    }

    public void ScrollDown(Guid documentId)
    {
        if (_catchedDocument != null) return;
        GameObject document;
        if (!_documents.TryGetValue(documentId, out document)) return;
        var normalizedPosition = document.transform.Find("Canvas/ScrollView").gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition;
        if (normalizedPosition <= 0.2f)
            document.transform.Find("Canvas/ScrollView").gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = 0f;
        else document.transform.Find("Canvas/ScrollView").gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition -= 0.2f;
        SaveDocuments(); // Need this for bookmark
    }

    private void SelectDocument(Guid id)
    {
        if (_selectedDocument != null)
            ((Behaviour)_selectedDocument.transform.Find("Canvas/ScrollView").GetComponent("Halo")).enabled = false;
        GameObject document;
        if (!_documents.TryGetValue(id, out document)) return;
        ((Behaviour)document.transform.Find("Canvas/ScrollView").GetComponent("Halo")).enabled = true;
        _selectedDocument = document;
    }

    private void UnselectDocument()
    {
        if (_selectedDocument != null)
            ((Behaviour)_selectedDocument.transform.Find("Canvas/ScrollView").GetComponent("Halo")).enabled = false;
        _selectedDocument = null;
    }

    private void UpdateDocument(Guid id, GameObject document, int? pagesNumber, float? bookmark, string url)
    {
        if (document == null)
        {
            AnchorStore.Delete(id.ToString());
            _documentsStore.documentRecords.Remove(_documentsStore.documentRecords.FirstOrDefault(d => d.id == id.ToString()));
            return;
        }
        document.AddComponent<WorldAnchor>();
        AnchorStore.Delete(id.ToString());
        AnchorStore.Save(id.ToString(), document.GetComponent<WorldAnchor>());
        Component.Destroy(document.GetComponent<WorldAnchor>());
        var documentStored = _documentsStore.documentRecords.FirstOrDefault(d => d.id == id.ToString());
        if(documentStored == null)
        {
            _documentsStore.documentRecords.Add(new DocumentRecord
            {
                id = id.ToString(),
                pagesNumber = pagesNumber.Value,
                bookmark = bookmark.Value,
                url = url
            });
            return;
        }
        if (bookmark != null) documentStored.bookmark = bookmark.Value;
    }

    private void SaveDocuments()
    {
        _settings.DocumentsBackup = JsonUtility.ToJson(_documentsStore);
        _settings.CallToResume = true;
        _settings.Save();
    }

    private void ClearDocuments()
    {
        _documents.Keys.ToList().ForEach(RemoveDocument);
        _documentsCounter = 0;
        _settings.DocumentsBackup = string.Empty;
        _settings.Save();
    }

    private void RemoveDocument(Guid id)
    {
        GameObject document;
        if (!_documents.TryGetValue(id, out document)) return;
        UpdateDocument(id, null, null, null, null);
        if (_selectedDocument!=null && _selectedDocument.Equals(document)) _selectedDocument = null;
        GameObject.Destroy(document);
        _documents.Remove(id);
        if(_onDocumentDestroyed!=null) _onDocumentDestroyed.Invoke(JsonUtility.ToJson(new DocumentCallBackCommand()
        {
            command = HoloCommand.ACTIVE_DEACTIVE,
            id = id.ToString(),
        }));
    }

    private void CreateDocument(int pagesNumber, string url)
    {
        RemoveCatchedDocument();
        GameObject document;
        var id = Guid.NewGuid();
        InstantiateDocument(out document, id, pagesNumber, 1, url,
            d => {
                d.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 2));
            });
        _catchedDocument = document;
        _catchedDocument.GetComponent<BoxCollider>().enabled = false;
        _catchedDocument.transform.Find("Canvas/ScrollView").GetComponent<BoxCollider>().enabled = true;
        UpdateDocument(id, document, pagesNumber, 1, url);
        if(_onDocumentCreated!=null) _onDocumentCreated.Invoke(id, JsonUtility.ToJson(new DocumentCallBackCommand()
        {
            command = HoloCommand.COORDINATES,
            index = _documentsCounter,
            id = id.ToString(),
        }));
    }

    private void InstantiateDocument(out GameObject document, Guid id, int pagesNumber, float bookmark, string url, Action<GameObject> setPositionAndRotation)
    {
        Debug.Log("NEW DOCUMENT:" + id);
        document = (GameObject)GameObject.Instantiate(Document, _documentsContainer.transform);
        if (setPositionAndRotation != null) setPositionAndRotation.Invoke(document);
        _documentsCounter++;
        document.transform.Find("Canvas/HeaderPanel/HeaderPanelContainer/HeaderText").gameObject.GetComponent<TextMesh>().text = _documentsCounter.ToString();
        var scrollView = document.transform.Find("Canvas/ScrollView");
        var content = scrollView.transform.Find("Viewport/Content");
        var contentRectTransform = content.GetComponent<RectTransform>();
        contentRectTransform.sizeDelta = new Vector2(contentRectTransform.sizeDelta.x, contentRectTransform.sizeDelta.y * pagesNumber);
        var rawImage = content.transform.Find("RawImage");
        var rawImageRectTransform = rawImage.GetComponent<RectTransform>();
        rawImageRectTransform.sizeDelta = new Vector2(rawImageRectTransform.sizeDelta.x, rawImageRectTransform.sizeDelta.y * pagesNumber);
        document.transform.Find("Canvas/FloatPanel/ZoomInButton").gameObject.GetComponent<ControlDocumentButtonScript>().DocumentId = id;
        document.transform.Find("Canvas/FloatPanel/ZoomOutButton").gameObject.GetComponent<ControlDocumentButtonScript>().DocumentId = id;
        document.transform.Find("Canvas/FloatPanel/ScrollUpButton").gameObject.GetComponent<ControlDocumentButtonScript>().DocumentId = id;
        document.transform.Find("Canvas/FloatPanel/ScrollDownButton").gameObject.GetComponent<ControlDocumentButtonScript>().DocumentId = id;
        if (_onDocumentInstantiated!=null) _onDocumentInstantiated.Invoke(url, bookmark, scrollView.gameObject);
        document.SetActive(true);
        _documents.Add(id, document);
    }

    private void SetDocumentCoordinatesW(HoloCommand command, float step, Guid documentId)
    {
        float x, y, z;
        GameObject document;
        if(!_documents.TryGetValue(documentId, out document)) return;
        x = document.transform.localPosition.x;
        y = document.transform.localPosition.y;
        z = document.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.UP:
                document.transform.localPosition = new Vector3(x, y + step, z);
                break;
            case HoloCommand.DOWN:
                document.transform.localPosition = new Vector3(x, y - step, z);
                break;
            case HoloCommand.LEFT:
                document.transform.localPosition = new Vector3(x - step, y, z);
                break;
            case HoloCommand.RIGHT:
                document.transform.localPosition = new Vector3(x + step, y, z);
                break;
            case HoloCommand.ZOOM_IN:
                document.transform.localScale += new Vector3(step, step, step);
                break;
            case HoloCommand.ZOOM_OUT:
                document.transform.localScale -= new Vector3(step, step, step);
                break;
            case HoloCommand.INCREASE_Z:
                document.transform.localPosition = new Vector3(x, y, z + step);
                break;
            case HoloCommand.DECREASE_Z:
                document.transform.localPosition = new Vector3(x, y, z - step);
                break;
            case HoloCommand.ROTATE_XUP:
                document.transform.Rotate(step, 0, 0);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                document.transform.Rotate(0, step, 0);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                document.transform.Rotate(0, 0, step);
                break;
            case HoloCommand.ROTATE_XDOWN:
                document.transform.Rotate(-step, 0, 0);
                break;
            case HoloCommand.ROTATE_YLEFT:
                document.transform.Rotate(0, -step, 0);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                document.transform.Rotate(0, 0, -step);
                break;
        }
        UpdateDocument(documentId, document, null, null, null);
    }

    private void SetDocumentCoordinatesWR(HoloCommand command, float step, Guid documentId, ReferenceSystem referenceSystem)
    {
        float x, y, z;
        GameObject document;
        if (!_documents.TryGetValue(documentId, out document)) return;
        x = document.transform.localPosition.x;
        y = document.transform.localPosition.y;
        z = document.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.LEFT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    document.transform.localPosition = new Vector3(x - step, y, z);
                else
                {
                    step = step * 10;
                    var dir = -Camera.main.transform.right;
                    document.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.RIGHT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    document.transform.localPosition = new Vector3(x + step, y, z);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.right;
                    document.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.INCREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    document.transform.localPosition = new Vector3(x, y, z + step);
                else
                {
                    step = step * 10;
                    var dir = -Camera.main.transform.forward;
                    document.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.DECREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    document.transform.localPosition = new Vector3(x, y, z - step);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.forward;
                    document.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
        }
        UpdateDocument(documentId, document, null, null, null);
    }

    public IEnumerator SetBlinkDocument(Guid documentId)
    {
        GameObject document;
        if (!_documents.TryGetValue(documentId, out document)) yield break;
        for (var i = 0; i < 3; i++)
        {
            if(document!=null) document.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            if (document != null) document.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
        if (document != null) document.SetActive(true);
    }

    public IEnumerator LoadDocumentFromUrl(string url, float bookmark, GameObject scrollView)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();
            var rawImage = scrollView.transform.Find("Viewport/Content/RawImage").GetComponent<RawImage>();
            rawImage.transform.Find("LoadPlaceHolder").gameObject.SetActive(false);
            if (!string.IsNullOrEmpty(www.error))
            {
                rawImage.transform.Find("ErrorPlaceHolder").gameObject.SetActive(true);
                yield break;
            }
            scrollView.gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = bookmark;
            var texture = DownloadHandlerTexture.GetContent(www);
            rawImage.color = Color.white;
            rawImage.texture = texture;
        }
    }

}
