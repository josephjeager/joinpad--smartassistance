﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CursorsManager : ICursorsManager {

    /* Private Properties */

    private GameObject _cursor;

    private readonly GameObject _cursorContainer;
    private GameObject CursorContainer
    {
        get
        {
            return _cursorContainer;
        }
    }

    private GameObject _defaultCursor;
    private GameObject DefaultCursor
    {
        get
        {
            return _defaultCursor ?? (_defaultCursor = CursorContainer.transform.Find("DefaultCursor").gameObject);
        }
    }

    private GameObject _basicCursor;
    private GameObject BasicCursor
    {
        get
        {
            return _basicCursor ?? (_basicCursor = CursorContainer.transform.Find("BasicCursor").gameObject);
        }
    }

    public CursorsManager(GameObject cursorsContainer)
    {
        _cursorContainer = cursorsContainer;
        _cursor = DefaultCursor;
    }

    public void SwitchCursor(CustomCursor cursor)
    {
        HideCursor();
        HoloToolkit.Unity.InputModule.Cursor cursorComponent = null;
        switch (cursor)
        {
            case CustomCursor.BASIC:
                _cursor = BasicCursor;
                cursorComponent = _cursor.GetComponent<ObjectCursor>();
                break;
            case CustomCursor.DEFAULT:
                _cursor = DefaultCursor;
                cursorComponent = _cursor.GetComponent<AnimatedCursor>();
                break;
        }
        InputManager.Instance.GetComponent<SimpleSinglePointerSelector>().Cursor = cursorComponent;
        ShowCursor();
    }

    public void ShowCursor()
    {
        _cursor.SetActive(true);
    }

    public void HideCursor()
    {
        _cursor.SetActive(false);
    }

}
