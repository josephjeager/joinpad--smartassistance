﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRequireDocumentsManager {

    IDocumentsManager DocumentsManager { set; }
	
}
