﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRequireTextsManager {

    ITextsManager TextsManager { set; }

}
