﻿using System;
using System.Collections;
using UnityEngine;

public interface IVideosManager : IMessageDispatcher
{
    Action<string> OnVideoLoaded { set; }
    Action<Guid, string> OnVideoCreated { set; }
    Action<string> OnVideoDestroyed { set; }
    IEnumerator SetBlinkVideo(Guid id);
    void ShowVideosContainer();
    void HideVideosContainer();
    void ClearVideosContainer();
    void LoadVideosContainer();
    void CatchVideo(GameObject video);
    Guid PlaceCatchedVideo();
    void RemoveCatchedVideo();
    void FixVideoPosition(Guid videoId);
}
