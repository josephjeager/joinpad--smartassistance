﻿using System;
using System.Collections;
using UnityEngine;

public interface ITextsManager : IMessageDispatcher {

    Action<string> OnTextLoaded { set; }
    Action<Guid, string> OnTextCreated { set; }
    Action<string> OnTextDestroyed { set; }
    IEnumerator SetBlinkText(Guid id);
    void ShowTextsContainer();
    void HideTextsContainer();
    void ClearTextsContainer();
    void LoadTextsContainer();
    void RemoveText(GameObject text);

}
