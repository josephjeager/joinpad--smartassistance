﻿public interface ICursorsManager {

    void SwitchCursor(CustomCursor cursor);
    void ShowCursor();
    void HideCursor();

}
