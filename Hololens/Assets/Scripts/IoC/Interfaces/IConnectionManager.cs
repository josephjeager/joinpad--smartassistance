﻿using System;
using System.Collections.Generic;

public interface IConnectionManager {

  int SelectedPeerId { set; get; }
  Func<string, bool> OnIncomingCallFunction { set; }
  Action OnOutgoingCallAcceptedAction { set; }
  Action OnCallEndAction { set; }
  Action<int, string> OnContactAdded { set; }
  Action<int> OnContactRemoved { set; }
  Action<CommandMessage> OnCommandMessageReceivedAction { set; }
  bool IsConnected { get; }
  bool IsConnectedToPeer { get; }
  void ConnectToServer();
  void DisconnectFromPeer();
  void DisconnectFromServer();
  void ResumeCall();
  void StartCall();
  void DisableMicrophoneStream();
  void EnableMicrophoneStream();
  void GetContacts(Action<List<Contact>> callBack);
  void SendMessageToConnectedPeer(CommandMessage content);

}
