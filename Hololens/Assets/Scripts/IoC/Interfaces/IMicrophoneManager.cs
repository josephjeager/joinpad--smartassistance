﻿using System;

public interface IMicrophoneManager {

    void SwitchState(Action onMute, Action onUnmute);

}
