﻿using System;
using System.Collections;
using UnityEngine;

public interface IWidgetsManager : IMessageDispatcher {

    Action<string> OnWidgetLoaded { set; }
    Action<Guid, string> OnWidgetCreated { set; }
    Action<string> OnWidgetDestroyed { set; }
    IEnumerator SetBlinkWidget(Guid id);
    void ShowWidgetsContainer();
    void HideWidgetsContainer();
    void ClearWidgetsContainer();
    void LoadWidgetsContainer();
    void RemoveWidget(GameObject widget);

}
