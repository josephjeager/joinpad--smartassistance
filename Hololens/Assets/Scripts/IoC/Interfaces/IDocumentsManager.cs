﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public interface IDocumentsManager : IMessageDispatcher
{
    Action<string> OnDocumentLoaded { set; }
    Action<Guid, string> OnDocumentCreated { set; }
    Action<string> OnDocumentDestroyed { set; }
    Action<string, float, GameObject> OnDocumentInstantiated { set; }
    IEnumerator SetBlinkDocument(Guid id);
    IEnumerator LoadDocumentFromUrl(string url, float bookmark, GameObject scrollView);
    void ShowDocumentsContainer();
    void HideDocumentsContainer();
    void ClearDocumentsContainer();
    void LoadDocumentsContainer();
    void CatchDocument(GameObject document);
    Guid PlaceCatchedDocument();
    void RemoveCatchedDocument();
    void FixDocumentPosition(Guid documentId);
    void ZoomIn(Guid documentId);
    void ZoomOut(Guid documentId);
    void ScrollUp(Guid documentId);
    void ScrollDown(Guid documentId);
}
