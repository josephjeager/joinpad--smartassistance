﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRequireWidgetsManager {

    IWidgetsManager WidgetsManager { set; }

}
