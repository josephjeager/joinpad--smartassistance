﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRequireMicrophoneManager
{

    IMicrophoneManager MicrophoneManager { set; }

}
