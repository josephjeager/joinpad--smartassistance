﻿using System.Linq;
using UnityEngine;
using UnityEngine.XR.WSA.Persistence;
#if !UNITY_EDITOR
using WebRtcWrapper;
#endif

public class StartupScript : MonoBehaviour {

    public GameObject AudioContainer;
    public GameObject UIContainer;
    public GameObject SpatialMapping;
    public GameObject Keyboard;
    public GameObject WidgetsContainer;
    public GameObject TextsContainer;
    public GameObject ImagesContainer;
    public GameObject DocumentsContainer;
    public GameObject VideosContainer;
    public GameObject CursorsContainer;

    private readonly WorldAnchorStoreContainer _storeContainer;

    public StartupScript()
    {
        _storeContainer = new WorldAnchorStoreContainer();
    }

    void Start()
    {
        WorldAnchorStore.GetAsync(StoreLoaded);
        SetupContainer(Resources.FindObjectsOfTypeAll<MonoBehaviour>());
    }

    private void SetupContainer(MonoBehaviour[] components)
    {
        var settings = new Settings();
        components.Where(c => c is IRequireSettings).Cast<IRequireSettings>().ToList().ForEach(c => c.Settings = settings);

        var uiManager = new UIManager(UIContainer, settings);
        components.Where(c => c is IRequireUIManager).Cast<IRequireUIManager>().ToList().ForEach(c => c.UIManager = uiManager);

        var audioManager = new AudioManager(AudioContainer);
        components.Where(c => c is IRequireAudioManager).Cast<IRequireAudioManager>().ToList().ForEach(c => c.AudioManager = audioManager);

        var spatialManager = new SpatialManager(SpatialMapping);
        components.Where(c => c is IRequireSpatialManager).Cast<IRequireSpatialManager>().ToList().ForEach(c => c.SpatialManager = spatialManager);

        var keyboardManager = new KeyboardManager(Keyboard);
        components.Where(c => c is IRequireKeyboardManager).Cast<IRequireKeyboardManager>().ToList().ForEach(c => c.KeyboardManager = keyboardManager);

        var widgetsManager = new WidgetsManager(WidgetsContainer, spatialManager, settings, _storeContainer);
        components.Where(c => c is IRequireWidgetsManager).Cast<IRequireWidgetsManager>().ToList().ForEach(c => c.WidgetsManager = widgetsManager);

        var textsManager = new TextsManager(TextsContainer, spatialManager, settings, _storeContainer);
        components.Where(c => c is IRequireTextsManager).Cast<IRequireTextsManager>().ToList().ForEach(c => c.TextsManager = textsManager);

        var imagesManager = new ImagesManager(ImagesContainer, spatialManager, settings, _storeContainer);
        components.Where(c => c is IRequireImagesManager).Cast<IRequireImagesManager>().ToList().ForEach(c => c.ImagesManager = imagesManager);

        var documentsManager = new DocumentsManager(DocumentsContainer, spatialManager, settings, _storeContainer);
        components.Where(c => c is IRequireDocumentsManager).Cast<IRequireDocumentsManager>().ToList().ForEach(c => c.DocumentsManager = documentsManager);

        var videosManager = new VideosManager(VideosContainer, spatialManager, settings, _storeContainer);
        components.Where(c => c is IRequireVideosManager).Cast<IRequireVideosManager>().ToList().ForEach(c => c.VideosManager = videosManager);
            
        var microphoneManager = new MicrophoneManager();
        components.Where(c => c is IRequireMicrophoneManager).Cast<IRequireMicrophoneManager>().ToList().ForEach(c => c.MicrophoneManager = microphoneManager);

        var cursorsManager = new CursorsManager(CursorsContainer);
        components.Where(c => c is IRequireCursorsManager).Cast<IRequireCursorsManager>().ToList().ForEach(c => c.CursorsManager = cursorsManager);

        var sessionManager = new SessionManager(settings);
        components.Where(c => c is IRequireSessionManager).Cast<IRequireSessionManager>().ToList().ForEach(c => c.SessionManager = sessionManager);

//        #if !UNITY_EDITOR
        var webRtcControl = new WebRtcControl();
        var connectionManger = new ConnectionManager(webRtcControl, settings);
        components.Where(c => c is IRequireConnectionManager).Cast<IRequireConnectionManager>().ToList().ForEach(c => c.ConnectionManager = connectionManger);
//        #endif
    }

    private void StoreLoaded(WorldAnchorStore anchorStore)
    {
        _storeContainer.AnchorStore = anchorStore;
    }

}
