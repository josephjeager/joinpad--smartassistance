﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class RemoveImageButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.RemoveCatchedImage();
    }
}