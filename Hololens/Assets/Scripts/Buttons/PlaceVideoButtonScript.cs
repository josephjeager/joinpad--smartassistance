﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class PlaceVideoButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.PlaceCatchedVideo();
    }
}