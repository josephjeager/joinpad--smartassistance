﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class RemoveVideoButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.RemoveCatchedVideo();
    }
}