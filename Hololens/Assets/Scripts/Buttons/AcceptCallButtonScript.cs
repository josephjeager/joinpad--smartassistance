﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class AcceptCallButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.AcceptIncomingCall();
    }
}
