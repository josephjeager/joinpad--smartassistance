﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class IgnoreCallButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.IgnoreIncomingCall();
    }
}
