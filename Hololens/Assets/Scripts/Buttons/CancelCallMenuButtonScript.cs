﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CancelCallMenuButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.CancelCallMenu();
    }
}
