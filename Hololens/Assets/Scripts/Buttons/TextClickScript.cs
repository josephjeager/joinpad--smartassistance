﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class TextClickScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.RemoveText(this.gameObject);
    }
}