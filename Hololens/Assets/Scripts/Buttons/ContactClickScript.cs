﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class ContactClickScript : MonoBehaviour, IInputClickHandler {

    public ControlBusScript ControlBus;

    private int _contactId;
    public int ContactId
    {
        set { _contactId = value;  }
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.SelectContactToCall(_contactId);
    }
}
