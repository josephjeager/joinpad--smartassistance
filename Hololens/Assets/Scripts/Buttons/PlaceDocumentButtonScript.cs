﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class PlaceDocumentButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.PlaceCatchedDocument();
    }
}
