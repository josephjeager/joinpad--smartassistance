﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CancelDialogButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.OnDialogBoxCancelButtonClick();
    }
}
