﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CancelSettingsButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.UndoSettings();
    }
}
