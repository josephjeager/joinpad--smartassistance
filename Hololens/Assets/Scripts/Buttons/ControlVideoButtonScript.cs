﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class ControlVideoButtonScript : MonoBehaviour, IInputClickHandler
{
    public VideoPlayerAdapterScript VideoPlayerAdapter;
    public VideoCommand Command;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        switch (Command)
        {
            case VideoCommand.PLAY:
                VideoPlayerAdapter.Play();
                break;
            case VideoCommand.PAUSE:
                VideoPlayerAdapter.Pause();
                break;
            case VideoCommand.STOP:
                VideoPlayerAdapter.Stop();
                break;
            case VideoCommand.RESET:
                VideoPlayerAdapter.Reset();
                break;
            case VideoCommand.AUDIO_ON:
                VideoPlayerAdapter.AudioOn();
                break;
            case VideoCommand.AUDIO_OFF:
                VideoPlayerAdapter.AudioOff();
                break;
        }
    }
}
