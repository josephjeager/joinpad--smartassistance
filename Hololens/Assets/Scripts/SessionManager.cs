﻿using System;
using UnityEngine;

public class SessionManager : ISessionManager
{

    private ISettings _settings;

    private Action<string> _onSessionChecked;
    public Action<string> OnSessionChecked
    {
        set { _onSessionChecked = value; }
    }

    [Serializable]
    private class SessionIncomingCommand
    {
        public SessionCommand command;
        public string userName;
        public string sessionId;
    }

    [Serializable]
    private class SessionCallBackCommand
    {
        public SessionCommand command;
        public string sessionId;
    }

    public SessionManager(ISettings settings)
    {
        _settings = settings;
    }

    public void ReadCommand(string command)
    {
        var sessionMessage = JsonUtility.FromJson<SessionIncomingCommand>(command);
        switch (sessionMessage.command)
        {
            case SessionCommand.HANDSHAKE:
                CheckSession(sessionMessage.userName, sessionMessage.sessionId);
                break;
        }
    }

    private void CheckSession(string userName, string sessionId)
    {
        if(string.IsNullOrEmpty(_settings.LastPeerSessionIdBackup))
        {
            _settings.LastPeerSessionIdBackup = sessionId;
            _settings.Save();
        } else if(_settings.LastPeerNameBackup.Equals(userName) &&
                 !_settings.LastPeerSessionIdBackup.Equals(sessionId))
        {
            _onSessionChecked(JsonUtility.ToJson(new SessionCallBackCommand()
            {
                command = SessionCommand.HANDSHAKE,
                sessionId = _settings.LastPeerSessionIdBackup
            }));
        }
    }

}
